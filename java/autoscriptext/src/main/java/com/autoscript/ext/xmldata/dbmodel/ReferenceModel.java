/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.ArrayList;
import java.util.List;

import com.autoscript.ui.helper.XMLHelper;

/**
 * 参考模型实现
 * 作者:龙色波
 * 日期:2014-5-11
 */
public class ReferenceModel implements IReferenceModel {
	/**
	 * 基数
	 */
	private String cardinality;
	/**
	 * 子表名
	 */
	private String childTable;
	/**
	 * 编码
	 */
	private String code;
	/**
	 * 唯一标识
	 */
	private String id;
	/**
	 * 父表名
	 */
	private String parentTable;
	/**
	 * 参考连接模型列表(即关联列列表)
	 */
	private List<IReferenceJoinModel> referenceJoinModels = new ArrayList<IReferenceJoinModel>();
	/**
	 * 名称
	 */
	private String name;
	private String childTableId;
	private String parentTableId;
	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#getCardinality()
	 */
	@Override
	public String getCardinality() {
		return cardinality;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#getChildTable()
	 */
	@Override
	public String getChildTable() {
		return childTable;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#getCode()
	 */
	@Override
	public String getCode() {
		return code;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#getParentTable()
	 */
	@Override
	public String getParentTable() {
		return parentTable;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#getReferenceJoinModels()
	 */
	@Override
	public List<IReferenceJoinModel> getReferenceJoinModels() {
		return referenceJoinModels;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#setCardinality(java.lang.String)
	 */
	@Override
	public void setCardinality(String cardinality) {
		this.cardinality = cardinality;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#setChildTable(java.lang.String)
	 */
	@Override
	public void setChildTable(String childTableName) {
		this.childTable = childTableName;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#setParentTable(java.lang.String)
	 */
	@Override
	public void setParentTable(String parentTableName) {
		this.parentTable = parentTableName;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceModel#setReferenceJoinModels(java.util.List)
	 */
	@Override
	public void setReferenceJoinModels(
			List<IReferenceJoinModel> referenceJoinModels) {
		this.referenceJoinModels = referenceJoinModels;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.IToXml#toXml()
	 */
	@Override
	public String toXml() {
		StringBuilder builder = new StringBuilder();
		builder.append("<Reference ");
		builder.append(" code=\"").append(XMLHelper.transferred(code))
		.append("\" ");
		builder.append(" name=\"").append(XMLHelper.transferred(name))
		.append("\" "); 
		builder.append(" cardinality=\"").append(XMLHelper.transferred(cardinality))
		.append("\" ");
		builder.append(" childtable=\"").append(XMLHelper.transferred(childTable))
		.append("\" ");
		builder.append(" parenttable=\"").append(XMLHelper.transferred(parentTable))
		.append("\">\r\n");
		//连接模型
		for(IReferenceJoinModel joinModel:referenceJoinModels){
			builder.append(joinModel.toXml()).append("\r\n");
		}
		builder.append("</Reference>\r\n");
		return builder.toString();
	}

	@Override
	public String getChildTableId() {
		return childTableId;
	}

	@Override
	public String getParentTableId() {
		return parentTableId;
	}

	@Override
	public void setChildTableId(String id) {
		this.childTableId = id;
	}

	@Override
	public void setParentTableId(String id) {
		this.parentTableId = id;
	}

}
