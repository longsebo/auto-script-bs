/**
 * 
 */
package com.autoscript.ext.xmldata.pdm;

import javax.swing.JDialog;
import javax.swing.JFrame;

import com.autoscript.extinterface.xmldata.IMakeXmlData;

/**
 * 根据pdm构造xml
 * 作者:龙色波
 * 日期:2013-10-18
 */
public class MakeXmlDataFromPdm implements IMakeXmlData {

	/* (non-Javadoc)
	 * @see com.autoscript.extinterface.xmldata.IMakeXmlData#makeXml(javax.swing.JFrame)
	 */
	@Override
	public String makeXml(JFrame frame) throws Exception {
		PdmDialog dialog = new PdmDialog(frame,"选择PDM文件",true);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
		if(dialog.getRightPdmModel()!=null && dialog.getRightPdmModel().getTableModels()!=null 
				&& dialog.getRightPdmModel().getTableModels().size()>0){
			return dialog.getRightPdmModel().toXml();
		}else{
			return null;
		}
	}

}
