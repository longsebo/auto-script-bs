/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.List;

import com.autoscript.ui.helper.XMLHelper;

/**
 * 索引模型
 * 作者:龙色波
 * 日期:2013-10-20
 */
public class IndexModel implements IIndexModel {
	private String name;
	private String code;
	private List<String> columnNames;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<String> getColumnNames() {
		return columnNames;
	}
	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}
	@Override
	public String toXml() {
		StringBuilder builder = new StringBuilder();
		builder.append("<Index ");
		builder.append(" code=\"").append(XMLHelper.transferred(code)).append("\" ")
		.append("name=\"").append(XMLHelper.transferred(name)).append("\" ");
		builder.append(">\r\n");
		for(String colunmName:columnNames){
			builder.append("<Column>").append(XMLHelper.transferred(colunmName)).append("</Column>\r\n");
		}
		builder.append("</Index>");
		return builder.toString();
	}
}
