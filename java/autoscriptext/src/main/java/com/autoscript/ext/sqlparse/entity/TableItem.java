package com.autoscript.ext.sqlparse.entity;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 每个表定义解析
 */
@Data
public class TableItem {
    /**
     * 数据库类型
     */
    private String dbtype;
    /**
     * 字段列表
     */
    private List<Column> column;
    /**
     * 主键字段列表
     */
    private List<String> pkColumns;
    /**
     * 表英文名
     */
    private String table;
    /**
     * 表中文名
     */
    private String comment;
    /**
     * 外键列表
     */
    private List<ForeignItem> foreignItems;
}
