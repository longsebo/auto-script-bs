/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import com.autoscript.ui.helper.XMLHelper;

/**
 * 参考连接模型
 * 作者:龙色波
 * 日期:2014-5-11
 */
public class ReferenceJoinModel implements IReferenceJoinModel {
   /**
    * 子表列名
    */
   private String childColumnName;
   /**
    * 父表列名
    */
   private String parentColumnName;
	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceJoinModel#getChildColumnName()
	 */
	@Override
	public String getChildColumnName() {
		return childColumnName;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceJoinModel#getParentColumnName()
	 */
	@Override
	public String getParentColumnName() {
		// TODO Auto-generated method stub
		return parentColumnName;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceJoinModel#setChildColumnName(java.lang.String)
	 */
	@Override
	public void setChildColumnName(String childColumnName) {
		this.childColumnName = childColumnName;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.dbmodel.IReferenceJoinModel#setParentColumnName(java.lang.String)
	 */
	@Override
	public void setParentColumnName(String parentColumnName) {
		this.parentColumnName = parentColumnName;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.IToXml#toXml()
	 */
	@Override
	public String toXml() {
		StringBuilder builder = new StringBuilder();
		builder.append("<ReferenceJoin ");
		builder.append(" parentcolumnname=\"").append(XMLHelper.transferred(parentColumnName))
		.append("\" ");
		builder.append(" childcolumnname=\"").append(XMLHelper.transferred(childColumnName))
		.append("\" /> "); 
		return builder.toString();
	}

}
