/**
 * 
 */
package com.autoscript.ext.xmldata.database.dbmodelext;

import com.autoscript.ext.xmldata.dbmodel.IndexModel;
import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.XMLHelper;

/**
 * 索引模型扩展
 * 作者:龙色波
 * 日期:2013-10-29
 */
public class IndexModelExt extends IndexModel implements IIndexModelExt {

	private String ascOrDesc;
	private String indexQualifier;
	private String type;
	private boolean nonUnique;

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IIndexModelExt#getAscOrDesc()
	 */
	@Override
	public String getAscOrDesc() {
		return ascOrDesc;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IIndexModelExt#getIndexQualifier()
	 */
	@Override
	public String getIndexQualifier() {
		return indexQualifier;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IIndexModelExt#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IIndexModelExt#isNonUnique()
	 */
	@Override
	public boolean isNonUnique() {
		return nonUnique;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IIndexModelExt#setAscOrDesc(java.lang.String)
	 */
	@Override
	public void setAscOrDesc(String ascOrDesc) {
		this.ascOrDesc = ascOrDesc;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IIndexModelExt#setIndexQualifier(java.lang.String)
	 */
	@Override
	public void setIndexQualifier(String indexQualifier) {
		this.indexQualifier = indexQualifier;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IIndexModelExt#setNonUnique(boolean)
	 */
	@Override
	public void setNonUnique(boolean nonUnique) {
		this.nonUnique = nonUnique;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IIndexModelExt#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toXml(){
		//先调用父类的转换 
		String superxml = super.toXml();
		StringBuilder buff = new StringBuilder();
		//自身转换 
		if(!StringHelper.isEmpty(ascOrDesc)){
			buff.append(" ascordesc=\"").append(XMLHelper.transferred(ascOrDesc))
			.append("\"");
		}
		if(!StringHelper.isEmpty(indexQualifier)){
			buff.append(" indexqualifier=\"").append(XMLHelper.transferred(indexQualifier))
			.append("\"");
		}
		buff.append(" nonunique=\"").append(nonUnique).append("\"");
		if(!StringHelper.isEmpty(type)){
			buff.append(" type=\"").append(XMLHelper.transferred(type))
			.append("\"");
		}
		//连接到父xml
		if(!StringHelper.isEmpty(buff.toString())){
			return StringHelper.replaceAll(superxml, "/>", buff.toString()+"/>").toString();
		}else{
			return superxml;
		}
	}
}
