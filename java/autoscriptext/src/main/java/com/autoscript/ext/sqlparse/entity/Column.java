package com.autoscript.ext.sqlparse.entity;

import lombok.Data;

/**
 * sql 列信息
 */
@Data
public class Column {
    /**
     * 字段名
     */
    private String fieldName;
    /**
     * 字段类型
     */
    private String dataType;
    /**
     * 字段注释
     */
    private String comment;
    /**
     * 是否允许为空
     */
    private boolean allownull;
}
