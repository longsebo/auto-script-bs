package com.autoscript.ext.xmldata.sql.parse;

import com.autoscript.ext.xmldata.dbmodel.IDBModel;

/**
 * sql语句解析接口
 */
public interface ISqlParse {
    /**
     * 左括号
     */
    public static final String LEFT_BRACKET="(";
    /**
     * 右括号
     */
    public static final String RIGHT_BRACKET=")";
    /**
     * 逗号
     */
    public static final String COMMA=",";
    /**
     * 空字符串
     */
    public static final String NULL="null";
    /**
     * 单引号
     */
    public static final String SINGLE_QUOTES="'";
    /**
     * 解析建表语句
     * @param sqlContent  sql内容
     * @param dbType 数据库类型
     * @return 解析结果
     */
    IDBModel parseCreateSql(String sqlContent, String dbType) throws Exception;
}
