/**
 * 
 */
package com.autoscript.ext.xmldata.database;

import javax.swing.JDialog;
import javax.swing.JFrame;

import com.autoscript.extinterface.xmldata.IMakeXmlData;

/**
 * 根据数据库构造xml源数据
 * 作者:龙色波
 * 日期:2013-10-28
 */
public class MakeXmlDataFromDB implements IMakeXmlData {

	/* (non-Javadoc)
	 * @see com.autoscript.extinterface.xmldata.IMakeXmlData#makeXml(javax.swing.JFrame)
	 */ 
	@Override
	public String makeXml(JFrame frame) throws Exception {
		DataBaseDialog dialog = new DataBaseDialog(frame,"选择数据源",true);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
		if(dialog.getRightPdmModel()!=null && dialog.getRightPdmModel().getTableModels()!=null 
				&& dialog.getRightPdmModel().getTableModels().size()>0){
			String xml = dialog.getRightPdmModel().toXml();
			return xml;
		}else{
			return null;
		}
	}

}
