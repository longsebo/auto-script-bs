/**
 * 
 */
package com.autoscript.ext.xmldata.database.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Oracle数据源处理器
 * 作者:龙色波
 * 日期:2013-11-1
 */
public class OracleDataSourceProcess extends AbstractDataSourceProcess {

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.process.DataSourceProcess#getColumnComments(java.lang.String, java.lang.String)
	 */
	@Override
	public Map<String, String> getColumnComments(String owner, String tableName) throws SQLException {
		String sql;
		sql = "select COLUMN_NAME,COMMENTS from all_col_comments where OWNER=? and table_name=?";
		PreparedStatement st =null;
		ResultSet rs = null;
		Map<String,String> map = new HashMap<String,String>();
		try{
			st= con.prepareStatement(sql);
			st.setString(1, owner);
			st.setString(2, tableName);
			rs = st.executeQuery();
			while(rs.next()){
				map.put(rs.getString(1),rs.getString(2));
			}
			return map;
		}finally{
			if(rs!=null){
				try{
					rs.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(st!=null){
				try{
					st.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.process.DataSourceProcess#getTableComments(java.lang.String, java.lang.String)
	 */
	@Override
	public String getTableComments(String owner, String tableName) throws SQLException {
		String sql;
		sql = "select COMMENTS from all_tab_comments where OWNER=? and table_name=?";
		PreparedStatement st =null;
		ResultSet rs = null;
		try{
			st= con.prepareStatement(sql);
			st.setString(1, owner);
			st.setString(2, tableName);
			rs = st.executeQuery();
			while(rs.next()){
				return rs.getString(1);
			}
			return "";
		}finally{
			if(rs!=null){
				try{
					rs.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(st!=null){
				try{
					st.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}

}
