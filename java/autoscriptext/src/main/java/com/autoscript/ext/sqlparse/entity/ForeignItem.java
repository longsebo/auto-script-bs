package com.autoscript.ext.sqlparse.entity;

import lombok.Data;

import java.util.List;

/**
 * 外键
 */
@Data
public class ForeignItem {
    /**
     * 父表名称
     */
    private String parentTableName;
    /**
     * 父字段列表
     */
    private List<String> parentFieldNameList;
    /**
     * 子字段列表
     */
    private List<String> childFieldNameList;
    /**
     * 外键名称
     */
    private String name;

}
