/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.ArrayList;
import java.util.List;

import com.autoscript.ui.helper.XMLHelper;
import com.autoscript.ui.model.xml.IXmlNode;

/**
 * 主键模型实现
 * 作者:龙色波
 * 日期:2013-10-20
 */
public class KeyModel implements IKeyModel {
	/**
	 * 主键英文名
	 */
	private String code;
	/**
	 * 构成主键列名
	 */
	private List<String> columns;
	/**
	 * 主键中文名
	 */
	private String name;

	public KeyModel() {
		
	}
	public KeyModel(IXmlNode childNode) {
		code = childNode.getAttributeVal("code");
		name = childNode.getAttributeVal("name");
		columns = new ArrayList<String>();
		for(IXmlNode node1:childNode.getChildNodes()) {
			columns.add(node1.getValue());
		}
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public List<String> getColumns() {
		return columns;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}

	@Override
	public void setName(String name) {
		this.name =name;
	}

	@Override
	public String toXml() {
		StringBuilder builder = new StringBuilder();
		builder.append("<PrimaryKey ");
		if(code!=null){
		builder.append(" code=\"").append(XMLHelper.transferred(code)).append("\" ");
		}

		if(name!=null){
			builder.append("name=\"").append(XMLHelper.transferred(name)).append("\" ");
		}
		builder.append(">\r\n");
		for(String colunmName:columns){
			builder.append("<Column>").append(XMLHelper.transferred(colunmName)).append("</Column>\r\n");
		}
		builder.append("</PrimaryKey>");

		return builder.toString();
	}

}
