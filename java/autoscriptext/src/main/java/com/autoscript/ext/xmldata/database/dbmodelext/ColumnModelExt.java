/**
 * 
 */
package com.autoscript.ext.xmldata.database.dbmodelext;

import com.autoscript.ext.xmldata.dbmodel.ColumnModel;
import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.XMLHelper;

/**
 * 列扩展模型
 * 作者:龙色波
 * 日期:2013-10-29
 */
public class ColumnModelExt extends ColumnModel implements IColumnModelExt {
	private int charOctetLength;
	private String columnDef;
	private String isAutoincrement;
	private String isNullable;
	private int javaDataType;
	private int numPrecRadix;
	private int ordinalPosition;

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#getCharOctetLength()
	 */
	@Override
	public int getCharOctetLength() {
		return charOctetLength;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#getColumnDef()
	 */
	@Override
	public String getColumnDef() {
		return columnDef;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#getIsAutoincrement()
	 */
	@Override
	public String getIsAutoincrement() {
		return isAutoincrement;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#getIsNullable()
	 */
	@Override
	public String getIsNullable() {
		return isNullable;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#getJavaDataType()
	 */
	@Override
	public int getJavaDataType() {
		return javaDataType;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#getNumPrecRadix()
	 */
	@Override
	public int getNumPrecRadix() {
		return numPrecRadix;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#getOrdinalPosition()
	 */
	@Override
	public int getOrdinalPosition() {
		return ordinalPosition;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#setCharOctetLength(int)
	 */
	@Override
	public void setCharOctetLength(int charOctetLength) {
		this.charOctetLength = charOctetLength;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#setColumnDef(java.lang.String)
	 */
	@Override
	public void setColumnDef(String columnDef) {
		this.columnDef = columnDef;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#setIsAutoincrement(java.lang.String)
	 */
	@Override
	public void setIsAutoincrement(String isAutoincrement) {
		this.isAutoincrement = isAutoincrement;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#setIsNullable(java.lang.String)
	 */
	@Override
	public void setIsNullable(String isNullable) {
		this.isNullable = isNullable;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#setJavaDataType(int)
	 */
	@Override
	public void setJavaDataType(int dataType) {
		this.javaDataType = dataType;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#setNumPrecRadix(int)
	 */
	@Override
	public void setNumPrecRadix(int numPrecRadix) {
		this.numPrecRadix = numPrecRadix;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.IColumnModelExt#setOrdinalPosition(int)
	 */
	@Override
	public void setOrdinalPosition(int ordinalPosition) {
		this.ordinalPosition = ordinalPosition;

	}
	@Override
	public String toXml(){
		//先调用父类的转换 
		String superxml = super.toXml();
		StringBuilder buff = new StringBuilder();
		//自身转换 
		if(charOctetLength>0){
			buff.append(" charoctetlength=\"").append(charOctetLength)
			.append("\"");
		}
		if(!StringHelper.isEmpty(columnDef)){
			buff.append(" columndef=\"").append(XMLHelper.transferred(columnDef))
			.append("\"");
		}
		if(!StringHelper.isEmpty(isAutoincrement)){
			buff.append(" isautoincrement=\"").append(XMLHelper.transferred(isAutoincrement))
			.append("\"");
		}
		if(!StringHelper.isEmpty(isNullable)){
			buff.append(" isnullable=\"").append(XMLHelper.transferred(isNullable))
			.append("\"");
		}
		buff.append(" javadatatype=\"").append(javaDataType).append("\"");
		buff.append(" numprecradix=\"").append(numPrecRadix).append("\"");
		buff.append(" ordinalposition=\"").append(ordinalPosition)
			.append("\"");
		//连接到父xml
		if(!StringHelper.isEmpty(buff.toString())){
			return StringHelper.replaceAll(superxml, "/>", buff.toString()+"/>").toString();
		}else{
			return superxml;
		}
	}
}
