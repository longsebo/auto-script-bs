package com.autoscript.ext.xmldata.pdm;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Insets;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Component;

public class TestDlg extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			TestDlg dialog = new TestDlg();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public TestDlg() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(1, 1, 0, 0));
		{
			JSplitPane splitPane = new JSplitPane();
			contentPanel.add(splitPane);
			{
				JPanel panel = new JPanel();
				splitPane.setLeftComponent(panel);
				GridBagLayout gridbag = new GridBagLayout();
				gridbag.rowWeights = new double[]{0.0, 0.0, 1.0};
				gridbag.columnWeights = new double[]{1.0, 1.0};
		        GridBagConstraints c = new GridBagConstraints();


//				panel.setLayout(new GridLayout(0, 2, 0, 0));
		        panel.setLayout(gridbag);
		        {
		        	JLabel lblNewLabel = new JLabel("New label");
		        	GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		        	gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		        	gbc_lblNewLabel.gridwidth = 2;
		        	gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		        	gbc_lblNewLabel.gridx = 0;
		        	gbc_lblNewLabel.gridy = 0;
		        	panel.add(lblNewLabel, gbc_lblNewLabel);
		        }
		        {
		        	JLabel lblNewLabel_1 = new JLabel("New label");
		        	lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		        	GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		        	gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		        	gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		        	gbc_lblNewLabel_1.gridx = 0;
		        	gbc_lblNewLabel_1.gridy = 1;
//		        	gbc_lblNewLabel_1.gridwidth = GridBagConstraints.RELATIVE;
		        	panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		        }
		        {
		        	textField = new JTextField();
		        	GridBagConstraints gbc_textField = new GridBagConstraints();
		        	gbc_textField.anchor = GridBagConstraints.WEST;
		        	gbc_textField.insets = new Insets(0, 0, 5, 0);
		        	gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		        	gbc_textField.gridx = 1;
		        	gbc_textField.gridy = 1;
//		        	gbc_textField.gridwidth = GridBagConstraints.REMAINDER;
		        	panel.add(textField, gbc_textField);
//		        	textField.setColumns(10);
		        }
		        {
		        	JList list = new JList();
		        	GridBagConstraints gbc_list = new GridBagConstraints();
		        	gbc_list.gridwidth = 2;
		        	gbc_list.insets = new Insets(0, 0, 0, 5);
		        	gbc_list.fill = GridBagConstraints.BOTH;
		        	gbc_list.gridx = 0;
		        	gbc_list.gridy = 2;
		        	panel.add(list, gbc_list);
		        }
				{
					c.fill = GridBagConstraints.BOTH;
					c.weightx =0.0;
				}
				
				{
					c.fill = GridBagConstraints.BOTH;
					c.weightx =0.0;
				}
				{
					c.fill = GridBagConstraints.HORIZONTAL;
					c.weightx =1.0;
					c.gridwidth = GridBagConstraints.REMAINDER;
				}
				{
					c.fill = GridBagConstraints.BOTH;
					c.weightx =2.0;
					c.gridwidth = GridBagConstraints.REMAINDER;
				}
			}
			{
				JPanel panel = new JPanel();
				splitPane.setRightComponent(panel);
				panel.setLayout(new BorderLayout(0, 0));
				{
					JPanel panel_1 = new JPanel();
					panel.add(panel_1, BorderLayout.WEST);
					panel_1.setLayout(new GridLayout(2, 1, 0, 10));
					{
						JButton btnNewButton_1 = new JButton("New button");
						
						btnNewButton_1.setHorizontalAlignment(SwingConstants.LEFT);
						panel_1.add(btnNewButton_1);
					}
					{
						JButton btnNewButton_2 = new JButton("New button");
						btnNewButton_2.setHorizontalAlignment(SwingConstants.LEFT);
						panel_1.add(btnNewButton_2);
					}
//					{
//						JButton btnNewButton_3 = new JButton("New button");
//						btnNewButton_3.setHorizontalAlignment(SwingConstants.LEFT);
//						GridBagConstraints gbc_btnNewButton_3 = new GridBagConstraints();
//						gbc_btnNewButton_3.fill = GridBagConstraints.HORIZONTAL;
//						gbc_btnNewButton_3.gridwidth = 0;
//						gbc_btnNewButton_3.gridheight = 0;
//						gbc_btnNewButton_3.insets = new Insets(0, 0, 0, 5);
//						gbc_btnNewButton_3.gridx = 2;
//						gbc_btnNewButton_3.gridy = 0;
//						panel_1.add(btnNewButton_3, gbc_btnNewButton_3);
//					}
//					{
//						JButton btnNewButton_4 = new JButton("New button");
//						btnNewButton_4.setHorizontalAlignment(SwingConstants.LEFT);
//						GridBagConstraints gbc_btnNewButton_4 = new GridBagConstraints();
//						gbc_btnNewButton_4.fill = GridBagConstraints.HORIZONTAL;
//						gbc_btnNewButton_4.gridwidth = 0;
//						gbc_btnNewButton_4.gridheight = 0;
//						gbc_btnNewButton_4.anchor = GridBagConstraints.WEST;
//						gbc_btnNewButton_4.gridx = 3;
//						gbc_btnNewButton_4.gridy = 0;
//						panel_1.add(btnNewButton_4, gbc_btnNewButton_4);
//					}
				}
				{
					JScrollPane scrollPane = new JScrollPane();
					panel.add(scrollPane);
					{
						JList list = new JList();
						scrollPane.setViewportView(list);
					}
				}
				{
					JPanel panel_1 = new JPanel();
					panel.add(panel_1, BorderLayout.NORTH);
					{
						JLabel lblNewLabel_North = new JLabel("New label_North");
						panel_1.add(lblNewLabel_North);
					}
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		{
			JPanel panel = new JPanel();
			getContentPane().add(panel, BorderLayout.NORTH);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				JLabel lblNewLabel_2 = new JLabel("New label");
				lblNewLabel_2.setHorizontalAlignment(SwingConstants.LEFT);
				panel.add(lblNewLabel_2);
			}
			{
				textField_1 = new JTextField();
				textField_1.setHorizontalAlignment(SwingConstants.CENTER);
				panel.add(textField_1);
				textField_1.setColumns(10);
			}
			{
				JButton btnNewButton = new JButton("New button");
				panel.add(btnNewButton);
			}
		}
	}

}
