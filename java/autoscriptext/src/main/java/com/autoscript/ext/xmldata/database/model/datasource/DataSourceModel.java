/**
 * 
 */
package com.autoscript.ext.xmldata.database.model.datasource;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据源模型实现
 * 作者:龙色波
 * 日期:2013-10-28
 */
public class DataSourceModel implements IDataSourceModel {

	private List<IDataSourceItemModel> dataSourceItems = new ArrayList<IDataSourceItemModel>();

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceModel#getDataSourceItems()
	 */
	@Override
	public List<IDataSourceItemModel> getDataSourceItems() {
		return dataSourceItems;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceModel#setDataSourceItems(java.util.List)
	 */
	@Override
	public void setDataSourceItems(List<IDataSourceItemModel> items) {
		this.dataSourceItems = items;
	}

	@Override
	public void verify() throws Exception {
		if(dataSourceItems!=null){
			//循环验证每个数据源配置
			List<String> dataSourceNames = new ArrayList<String>();
			for(IDataSourceItemModel itemModel:dataSourceItems){
				if(dataSourceNames.contains(itemModel.getName())){
					throw new Exception("数据源名称："+itemModel.getName()+"已经存在!");
				}else{
					dataSourceNames.add(itemModel.getName());
				}
				itemModel.verify();	
			}
		}
	}

}
