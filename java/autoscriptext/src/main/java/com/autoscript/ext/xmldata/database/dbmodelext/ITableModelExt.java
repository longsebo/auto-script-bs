/**
 * 
 */
package com.autoscript.ext.xmldata.database.dbmodelext;

import com.autoscript.ext.xmldata.dbmodel.ITableModel;

/**
 * 扩展的表模型
 * 作者:龙色波
 * 日期:2013-10-29
 */
public interface ITableModelExt extends ITableModel {
	/**
	 * 设置表分类
	 * @param tableCat
	 */
	public void setTableCat(String tableCat);
	/**
	 * 设置表Schem
	 */
	public void setTableSchem(String tableSchem);
	/**
	 * 设置表类型
	 * @param tableType
	 */
	public void setTableType(String tableType);
	/**
	 * 设置类型分类
	 * @param typeCat
	 */
	public void setTypeCat(String typeCat);
	/**
	 * 设置类型Schema
	 * @param typeSchem
	 */
	public void setTypeSchem(String typeSchem);
	/**
	 * 设置类型名称
	 * @param typeName
	 */
	public void setTypeName(String typeName);
	/**
	 * 设置有类型表的指定 "identifier" 列的名称
	 * @param selfReferencingColName
	 */
	public void setSelfReferencingColName(String selfReferencingColName);
	/**
	 * 指定在 SELF_REFERENCING_COL_NAME 中创建值的方式
	 * @param refGeneration
	 */
	public void setRefGeneration(String refGeneration);
	/**
	 * 获取表分类
	 * @return
	 */
	public String getTableCat();
	/**
	 * 获取表Schema
	 * @return
	 */
	public String getTableSchem();
	/**
	 * 获取表类型
	 * @return
	 */	
	public String getTableType();
	/**
	 * 获取类型分类
	 * @return
	 */
	public String getTypeCat();
	/**
	 * 获取类型Schema
	 * @return
	 */	
	public String getTypeSchem();
	/**
	 * 获取类型名称
	 * @return
	 */	
	public String getTypeName();
	/**
	 * 获取有类型表的指定 "identifier" 列的名称
	 * @return
	 */	
	public String getSelfReferencingColName();
	/**
	 * 获取在 SELF_REFERENCING_COL_NAME 中创建值的方式
	 * @return
	 */	
	public String getRefGeneration();
}
