/**
 * 
 */
package com.autoscript.ext.xmldata;

/**
 * 用于界面和线程传递操作信息的Bean
 * 作者:龙色波
 * 日期:2013-11-7
 */
public class ProcessInfo {
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
