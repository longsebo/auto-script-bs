/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.List;

import com.autoscript.ext.xmldata.IToXml;

/**
 * 引用模型(一般为外键)
 * 作者:龙色波
 * 日期:2014-5-11
 */
public interface IReferenceModel extends IToXml {
   /**
    * 设置id
    * @param id 唯一标识
    */
   public void setId(String id);
   /**
    * 获取id
    * @return 返回id
    */
   public String getId();
   /**
    * 设置名称
    * @param name 名称
    */
   public void setName(String name);
   /**
    * 获取名称
    * @return
    */
   public String getName();
   /**
	 * 设置表名英文名
	 * @param code
	 */
	public void setCode(String code);
	/**
	 * 获取表名英文名
	 * @return
	 */
	public String getCode();
	/**
	 * 设置基数 例如0..n
	 * @param cardinality
	 */
	public void setCardinality(String cardinality);
	/**
	 * 获取基数
	 * @return
	 */
	public String  getCardinality();
	/**
	 * 设置父表名称
	 * @param parentTableName
	 */
	public void setParentTable(String parentTableName);
	/**
	 * 获取父表名称
	 * @return
	 */
	public String getParentTable();
	/**
	 * 设置子表名称
	 * @param childTableName
	 */
	public void setChildTable(String childTableName);
	/**
	 * 获取子表名称
	 * @return
	 */
	public String getChildTable();
	/**
	 * 设置参考连接模型列表
	 * @param referenceJoinModels
	 */
	public void setReferenceJoinModels(List<IReferenceJoinModel> referenceJoinModels);
	/**
	 * 获取参考连接模型列表
	 * @return
	 */
	public List<IReferenceJoinModel> getReferenceJoinModels();
	/**
	 * 获取父表id
	 * @return
	 */
	public String getParentTableId();
	/**
	 * 设置父亲id
	 * @param id
	 */
	public void setParentTableId(String id);
	/**
	 * 获取子表id
	 * @return
	 */
	public String getChildTableId();
	/**
	 * 设置子表id
	 * @param id
	 */
	public void setChildTableId(String id);
}
