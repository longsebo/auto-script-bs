/**
 * 
 */
package com.autoscript.ext;

import java.io.File;

/**
 * 系统常量
 * 作者:龙色波
 * 日期:2013-10-28
 */
public interface ISystemConstant {
	public static final String HOME_PATH = System.getProperty("user.dir");

	public static final String CONF_PATH = HOME_PATH + "/conf";

	/**
	 * 数据源xml文件名
	 */
	public static final String DATASOURCE_CONFFILE = CONF_PATH + File.separator
			+ "datasource.xml";
	//数据库元数据Resetset字段常量
	//表记录集字段
	/**
	 * TABLE_CAT
	 */
	public static final String TABLE_CAT="TABLE_CAT";
	/**
	 * TABLE_SCHEM
	 */
	public static final String TABLE_SCHEM="TABLE_SCHEM";
	/**
	 * TABLE_NAME
	 */
	public static final String TABLE_NAME="TABLE_NAME";
	/**
	 * TABLE_TYPE
	 */
	public static final String TABLE_TYPE="TABLE_TYPE";
	/**
	 * REMARKS
	 */
	public static final String REMARKS="REMARKS";
	/**
	 * TYPE_CAT
	 */
	public static final String TYPE_CAT="TYPE_CAT";
	/**
	 * TYPE_SCHEM 
	 */
	public static final String TYPE_SCHEM ="TYPE_SCHEM";
	/**
	 * TYPE_NAME
	 */
	public static final String TYPE_NAME="TYPE_NAME";
	/**
	 * SELF_REFERENCING_COL_NAME 
	 */
	public static final String SELF_REFERENCING_COL_NAME="SELF_REFERENCING_COL_NAME";
	/**
	 * REF_GENERATION 
	 */
	public static final String REF_GENERATION ="REF_GENERATION";
	//主键记录集字段
	/**
	 * PK_NAME
	 */
	public static final String PK_NAME="PK_NAME";
	/**
	 * COLUMN_NAME
	 */
	public static final String COLUMN_NAME="COLUMN_NAME";
	//索引记录集字段
	/**
	 * NON_UNIQUE
	 */
	public static final String NON_UNIQUE="NON_UNIQUE";
	/**
	 * INDEX_NAME
	 */
	public static final String INDEX_NAME ="INDEX_NAME";
	/**
	 * ORDINAL_POSITION 
	 */
	public static final String ORDINAL_POSITION ="ORDINAL_POSITION";
	/**
	 * ASC_OR_DESC
	 */
	public static final String ASC_OR_DESC="ASC_OR_DESC";
	/**
	 * INDEX_QUALIFIER
	 */
	public static final String INDEX_QUALIFIER="INDEX_QUALIFIER";
	/**
	 * TYPE
	 */
	public static final String TYPE="TYPE";
	//列记录集字段
	/**
	 * CHAR_OCTET_LENGTH 
	 */
	public static final String CHAR_OCTET_LENGTH ="CHAR_OCTET_LENGTH";
	/**
	 * COLUMN_DEF
	 */
	public static final String COLUMN_DEF="COLUMN_DEF";
	/**
	 * IS_AUTOINCREMENT
	 */
	public static final String IS_AUTOINCREMENT="IS_AUTOINCREMENT";
	/**
	 * NULLABLE
	 */
	public static final String NULLABLE="NULLABLE"; 
	/**
	 * DATA_TYPE
	 */
	public static final String DATA_TYPE="DATA_TYPE";
	/**
	 * NUM_PREC_RADIX
	 */
	public static final String NUM_PREC_RADIX="NUM_PREC_RADIX";
	/**
	 * COLUMN_SIZE
	 */
	public static final String COLUMN_SIZE = "COLUMN_SIZE";
	/**
	 * DECIMAL_DIGITS
	 */
	public static final String DECIMAL_DIGITS="DECIMAL_DIGITS";
	/**
	 * IS_NULLABLE 
	 */
	public static final String IS_NULLABLE ="IS_NULLABLE"; 
	//数据库类型
	/**
	 * Oracle
	 */
	public static final String DBTYPE_ORACE="oracle";
	/**
	 * mysql
	 */
	public static final String DBTYPE_MYSQL="mysql";
	/**
	 * UTF-8字符集
	 */
	public static final String ENCODE_UTF_8="UTF-8";
}
