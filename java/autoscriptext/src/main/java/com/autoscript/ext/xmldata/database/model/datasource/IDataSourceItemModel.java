/**
 * 
 */
package com.autoscript.ext.xmldata.database.model.datasource;

import java.util.List;

/**
 * 数据源项配置模型
 * 作者:龙色波
 * 日期:2013-10-28
 */
public interface IDataSourceItemModel {
	  /**
	   * 设置数据源名称
	   * @param name
	   */
      public void setName(String name);
      /**
       * 获取数据源名称
       * @return
       */
      public String getName();
      /**
       * 设置数据源驱动类名
       * @param className
       */
      public void setDriverClass(String className);
      /**
       * 获取数据源驱动类名
       * @return
       */
      public String getDriverClass();
      /**
       * 设置数据库url
       * @param url
       */
      public void setURL(String url);
      /**
       * 获取数据库url
       * @return
       */
      public String getURL();
      /**
       * 设置用户名
       * @param user
       */
      public void setUser(String user);
      /**
       * 获取用户名
       * @return
       */
      public String getUser();
      /**
       * 设置密码
       * @param password
       */
      public void setPassword(String password);
      /**
       * 获取密码
       * @return
       */
      public String getPassword();
      /**
       * 设置驱动jar文件列表
       * @param jarFiles
       */
      public void setJarFiles(List<String> jarFiles);
      /**
       * 获取驱动jar文件列表
       * @return
       */
      public List<String> getJarFiles();
      /**
       * 设置数据库类型
       * @param dbType
       */
      public void setDBType(String dbType);
      /**
       * 获取数据库类型
       * @return
       */
      public String getDBType();
      public String toString();
      /**
       * 验证
       */
      public void verify() throws Exception;
}
