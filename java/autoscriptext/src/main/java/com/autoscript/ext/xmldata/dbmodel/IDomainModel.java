/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import com.autoscript.ext.xmldata.IToXml;

/**
 * @author Administrator
 * 	列domain
 */
public interface IDomainModel extends IToXml {
	/**
	 * 	获取编码 
	 * @return
	 */
	public String getCode();
	/**
	 * 	获取数据类型
	 * @return
	 */
	public String getDataType();
	/**
	 * 	获取id
	 * @return
	 */
	public String getId();
	/**
	 * 	获取数据类型长度
	 * @return
	 */
	public int getLength();
	/**
	 * 	获取名称
	 * @return
	 */
	public String getName();
	/**
	 * 	设置编码
	 * @param code
	 */
	public void setCode(String code);
	/**
	 * 	设置数据类型
	 * @param dataType
	 */
	public void setDataType(String dataType);
	/**
	 * 	设置id
	 * @param id
	 */
	public void setId(String id);
	/**
	 * 	设置长度
	 * @param length
	 */
	public void setLength(int length);
	/**
	 * 	设置名称
	 * @param name
	 */
	public void setName(String name);
}
