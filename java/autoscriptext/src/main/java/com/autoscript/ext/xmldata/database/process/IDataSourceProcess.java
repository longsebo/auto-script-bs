/**
 * 
 */
package com.autoscript.ext.xmldata.database.process;

import java.util.List;

import javax.swing.JLabel;

import com.autoscript.ext.xmldata.ProcessInfo;
import com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel;
import com.autoscript.ext.xmldata.dbmodel.IDBModel;
import com.autoscript.ext.xmldata.dbmodel.ITableModel;

/**
 * 数据源处理器接口
 * 作者:龙色波
 * 日期:2013-10-29
 */
public interface IDataSourceProcess {
	/**
	 * 从数据源构造
	 * @param dsItemModel 数据源模型 
	 * @return 返回db模型
	 * @throws Exception
	 */
	public IDBModel parse(IDataSourceItemModel dsItemModel) throws Exception;
	/**
	 * 填充表模型的其他信息（包括列信息，主键，索引等信息)
	 * @param dsItemModel 数据源模型
	 * @param list 表模型列表
	 * @throws Exception
	 */
	public List<ITableModel> fillTabModelsInfo(IDataSourceItemModel dsItemModel,
			List<ITableModel> list) throws Exception ;
	/**
	 * 设置显示的信息
	 * @param processMessage
	 */
	public void setDispMsg(ProcessInfo processMessage);
}
