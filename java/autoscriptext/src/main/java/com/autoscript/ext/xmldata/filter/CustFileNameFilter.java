/**
 * 
 */
package com.autoscript.ext.xmldata.filter;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 自定义文件名过滤器
 */
public class CustFileNameFilter implements FilenameFilter {
	/**
	 * 文件名后缀
	 */
	private String extFileName;
	public CustFileNameFilter(String extFileName){
		this.extFileName = extFileName.toLowerCase();
	}
	/* (non-Javadoc)
	 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
	 */
	@Override
	public boolean accept(File dir, String name) {
		return name.toLowerCase().equals(extFileName);
	}

}
