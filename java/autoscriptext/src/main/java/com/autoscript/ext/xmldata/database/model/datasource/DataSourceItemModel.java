/**
 * 
 */
package com.autoscript.ext.xmldata.database.model.datasource;

import java.util.List;

import com.autoscript.ui.helper.FileCtrlUtils;
import com.autoscript.ui.helper.StringHelper;

/**
 * 数据源项配置模型
 * 作者:龙色波
 * 日期:2013-10-28
 */
public class DataSourceItemModel implements IDataSourceItemModel {
	/**
	 * 驱动类名
	 */
	private String driverClass;
	/**
	 * jar包文件名列表
	 */
	private List<String> jarFiles;
	/**
	 * 数据源名称
	 */
	private String name;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 数据库url
	 */
	private String url;
	/**
	 * 用户
	 */
	private String user;
	/**
	 * 数据库类型
	 */
	private String dbType;

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#getDriverClass()
	 */
	@Override
	public String getDriverClass() {
		return this.driverClass;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#getJarFiles()
	 */
	@Override
	public List<String> getJarFiles() {
		return jarFiles;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#getPassword()
	 */
	@Override
	public String getPassword() {
		return password;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#getURL()
	 */
	@Override
	public String getURL() {
		return url;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#getUser()
	 */
	@Override
	public String getUser() {
		return user;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#setDriverClass(java.lang.String)
	 */
	@Override
	public void setDriverClass(String className) {
		this.driverClass = className;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#setJarFiles(java.util.List)
	 */
	@Override
	public void setJarFiles(List<String> jarFiles) {
		this.jarFiles = jarFiles;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#setPassword(java.lang.String)
	 */
	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#setURL(java.lang.String)
	 */
	@Override
	public void setURL(String url) {
		this.url = url;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel#setUser(java.lang.String)
	 */
	@Override
	public void setUser(String user) {
		this.user = user;

	}
	public String toString(){
		return name;
	}

	@Override
	public void verify() throws Exception {
		//数据源名称不能为空
		if(StringHelper.isEmpty(name)){
			throw new Exception("数据源名称不能为空!");
		}
		//驱动类名不能为空
		if(StringHelper.isEmpty(driverClass)){
			throw new Exception("驱动类名不能为空!");
		}
		//url 不能为空
		if(StringHelper.isEmpty(url)){
			throw new Exception("url 不能为空!");
		}
		//jar文件列表的文件必须存在
		if(jarFiles==null||jarFiles.size()==0){
			throw new Exception("jar文件列表为空");
		}
		for(String fileName:jarFiles){
			if(!FileCtrlUtils.isExists(fileName)){
				throw new Exception("文件:"+fileName+"不存在!");
			}
		}
		//数据库类型不能为空
		if(StringHelper.isEmpty(dbType)){
			throw new Exception("数据库类型不能为空!");
		}
	}

	@Override
	public String getDBType() {
		return dbType;
	}

	@Override
	public void setDBType(String dbType) {
		this.dbType = dbType;
		
	}
}
