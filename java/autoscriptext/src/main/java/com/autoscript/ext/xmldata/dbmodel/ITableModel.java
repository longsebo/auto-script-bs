/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.List;

import com.autoscript.ext.xmldata.IToXml;

/**
 * 表模型
 * 作者:龙色波
 * 日期:2013-10-19
 */
public interface ITableModel extends IToXml {
	
	/**
	 * 设置表名中文名
	 * @param name
	 */
	public void setName(String name);
	/**
	 * 获取表名中文名
	 * @return
	 */
	public String getName();
	/**
	 * 设置表名英文名
	 * @param code
	 */
	public void setCode(String code);
	/**
	 * 获取表名英文名
	 * @return
	 */
	public String getCode();
	/**
	 * 设置表注释
	 * @param comment
	 */
	public void setComment(String comment);
	/**
	 * 获取表注释
	 * @return
	 */
	public String getComment();
	/**
	 * 设置列模型列表
	 * @param columnModels
	 */
	public void setColumnModels(List<IColumnModel> columnModels);
	/**
	 * 获取列模型列表
	 * @return 
	 */
	public List<IColumnModel> getColumnModels();
	/**
	 * 设置索引模型列表
	 * @param indexModels
	 */
	public void setIndexModels(List<IIndexModel> indexModels);
	/**
	 * 获取索引模型列表
	 * @return
	 */
	public List<IIndexModel> getIndexModels();
	/**
	 * 设置主键模型
	 * @param keyModel 主键模型
	 */
	public void setKey(IKeyModel keyModel);
	/**
	 * 获取主键模型
	 * @return
	 */
	public IKeyModel getKey();
    /**
     * 设置id
     * @param id 唯一标识
     */
    public void setId(String id);
    /**
     * 获取id
     * @return 返回id
     */
    public String getId();

	/**
	 * 转换为xml
	 * @return
	 */
	public String toXml();
	/**
	 * 转换为String 
	 * @return
	 */
	public String toString();
	
}
