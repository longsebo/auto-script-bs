/**
 * 
 */
package com.autoscript.ext.xmldata.database.dbmodelext;

import com.autoscript.ext.xmldata.dbmodel.TableModel;
import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.XMLHelper;
import com.autoscript.ui.model.xml.IXmlNode;

/**
 * 表扩展模型
 * 作者:龙色波
 * 日期:2013-10-29
 */
public class TableModelExt extends TableModel implements ITableModelExt {


	private String refGeneration;
	private String selfReferencingColName;
	private String tableCat;
	private String tableSchem;
	private String tableType;
	private String typeCat;
	private String typeName;
	private String typeSchem;
	
	public TableModelExt() {
		
	}
	public TableModelExt(IXmlNode xmlNode) {
		super(xmlNode);
	}
	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#getRefGeneration()
	 */
	@Override
	public String getRefGeneration() {
		return this.refGeneration;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#getSelfReferencingColName()
	 */
	@Override
	public String getSelfReferencingColName() {
		return this.selfReferencingColName;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#getTableCat()
	 */
	@Override
	public String getTableCat() {
		return this.tableCat;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#getTableSchem()
	 */
	@Override
	public String getTableSchem() {
		return this.tableSchem;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#getTableType()
	 */
	@Override
	public String getTableType() {
		return tableType;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#getTypeCat()
	 */
	@Override
	public String getTypeCat() {
		return typeCat;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#getTypeName()
	 */
	@Override
	public String getTypeName() {
		return typeName;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#getTypeSchem()
	 */
	@Override
	public String getTypeSchem() {
		return typeSchem;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#setRefGeneration(java.lang.String)
	 */
	@Override
	public void setRefGeneration(String refGeneration) {
		this.refGeneration = refGeneration;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#setSelfReferencingColName(java.lang.String)
	 */
	@Override
	public void setSelfReferencingColName(String selfReferencingColName) {
		this.selfReferencingColName = selfReferencingColName;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#setTableCat(java.lang.String)
	 */
	@Override
	public void setTableCat(String tableCat) {
		this.tableCat = tableCat;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#setTableSchem(java.lang.String)
	 */
	@Override
	public void setTableSchem(String tableSchem) {
		this.tableSchem = tableSchem;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#setTableType(java.lang.String)
	 */
	@Override
	public void setTableType(String tableType) {
		this.tableType = tableType;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#setTypeCat(java.lang.String)
	 */
	@Override
	public void setTypeCat(String typeCat) {
		this.typeCat = typeCat;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#setTypeName(java.lang.String)
	 */
	@Override
	public void setTypeName(String typeName) {
		this.typeName = typeName;

	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt#setTypeSchem(java.lang.String)
	 */
	@Override
	public void setTypeSchem(String typeSchem) {
		this.typeSchem = typeSchem;

	}
	@Override
	public String toXml(){
		//先调用父类的转换 
		String superxml = super.toXml();
		StringBuilder buff = new StringBuilder();
		//自身转换 
		if(!StringHelper.isEmpty(refGeneration)){
			buff.append(" refgeneration=\"").append(XMLHelper.transferred(refGeneration))
			.append("\"");
		}
		if(!StringHelper.isEmpty(selfReferencingColName)){
			buff.append(" selfreferencingcolname=\"").append(XMLHelper.transferred(selfReferencingColName))
			.append("\"");
		}
		if(!StringHelper.isEmpty(tableCat)){
			buff.append(" tablecat=\"").append(XMLHelper.transferred(tableCat))
			.append("\"");
		}
		if(!StringHelper.isEmpty(tableSchem)){
			buff.append(" tableschem=\"").append(XMLHelper.transferred(tableSchem))
			.append("\"");
		}
		if(!StringHelper.isEmpty(tableType)){
			buff.append(" tabletype=\"").append(XMLHelper.transferred(tableType))
			.append("\"");
		}
		if(!StringHelper.isEmpty(typeCat)){
			buff.append(" typecat=\"").append(XMLHelper.transferred(typeCat))
			.append("\"");
		}
		if(!StringHelper.isEmpty(typeName)){
			buff.append(" typename=\"").append(XMLHelper.transferred(typeName))
			.append("\"");
		}
		if(!StringHelper.isEmpty(typeSchem)){
			buff.append(" typeschem=\"").append(XMLHelper.transferred(typeSchem))
			.append("\"");
		}
		//连接到父xml
		if(!StringHelper.isEmpty(buff.toString())){
			return StringHelper.replaceAll(superxml, "/>", buff.toString()+"/>").toString();
		}else{
			return superxml;
		}
	}
	public String toString(){
		if(!StringHelper.isEmpty(tableSchem)){
			return tableSchem+"."+super.toString();
		}else{
			return super.toString();
		}
	}
}
