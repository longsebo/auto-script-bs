package com.autoscript.ext.xmldata.dbmodel;

import com.autoscript.ext.xmldata.dbmodel.IColumnModel;
import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.XMLHelper;
import com.autoscript.ui.model.xml.IXmlNode;


public class ColumnModel
        implements IColumnModel {
    private String code;
    private String name;
    private String dateType;
    private int length = 0;


    private int precision = 0;


    private boolean mandatory = false;


    private String comment;


    private String referenceName = "";


    private String domain;


    private String id;


    public ColumnModel() {
    }


    public ColumnModel(IXmlNode childNode) {
        this.code = childNode.getAttributeVal("code");
        this.name = childNode.getAttributeVal("name");
        this.dateType = childNode.getAttributeVal("datatype");
        this.domain = childNode.getAttributeVal("domain");
        if (!StringHelper.isEmpty(childNode.getAttributeVal("length"))) {
            this.length = Integer.valueOf(childNode.getAttributeVal("length")).intValue();
        }
        if (!StringHelper.isEmpty(childNode.getAttributeVal("isnotnull"))) {
            this.mandatory = Boolean.valueOf(childNode.getAttributeVal("isnotnull")).booleanValue();
        }

        if (!StringHelper.isEmpty(childNode.getAttributeVal("precision"))) {
            this.precision = Integer.valueOf(childNode.getAttributeVal("precision")).intValue();
        }

        this.comment = childNode.getAttributeVal("comment");
        this.referenceName = childNode.getAttributeVal("referenceName");
    }


    public String getCode() {
        return this.code;
    }


    public String getDomain() {
        return this.domain;
    }


    public void setDomain(String domain) {
        this.domain = domain;
    }


    public String getDataType() {
        return this.dateType;
    }


    public int getLength() {
        return this.length;
    }


    public String getName() {
        return this.name;
    }


    public int getPrecision() {
        return this.precision;
    }


    public boolean isMandatory() {
        return this.mandatory;
    }


    public void setCode(String code) {
        this.code = code;
    }


    public void setDataType(String dataType) {
        this.dateType = dataType;
    }


    public void setLength(int length) {
        this.length = length;
    }


    public void setMandatory(boolean isMandatory) {
        this.mandatory = isMandatory;
    }


    public void setName(String name) {
        this.name = name;
    }


    public void setPrecision(int precision) {
        this.precision = precision;
    }


    public String toXml() {
        StringBuilder builder = new StringBuilder();
        builder.append("<Column ");
        builder.append(" code=\"").append(XMLHelper.transferred(this.code)).append("\" ");
        if (this.name != null) {
            builder.append("name=\"").append(XMLHelper.transferred(this.name)).append("\" ");
        }
        builder.append("datatype=\"").append(XMLHelper.transferred(this.dateType)).append("\" ");
        if (this.length > 0) {
            builder.append("length=\"").append(this.length).append("\" ");
        }
        if (this.precision > 0) {
            builder.append("precision=\"").append(this.precision).append("\" ");
        }
        if (this.mandatory) {
            builder.append("isnotnull=\"true\" ");
        }
        if (this.comment != null) {
            builder.append("comment=\"").append(XMLHelper.transferred(this.comment)).append("\" ");
        }
        if (!StringHelper.isEmpty(this.referenceName)) {
            builder.append("referenceName=\"").append(XMLHelper.transferred(this.referenceName)).append("\" ");
        }
        if (!StringHelper.isEmpty(this.domain)) {
            builder.append("domain=\"").append(XMLHelper.transferred(this.domain)).append("\" ");
        }
        builder.append("/>");
        return builder.toString();
    }


    public String getComment() {
        return this.comment;
    }


    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getDateType() {
        return this.dateType;
    }


    public void setDateType(String dateType) {
        this.dateType = dateType;
    }


    public String getReferenceName() {
        return this.referenceName;
    }


    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }


    public String getId() {
        return this.id;
    }


    public void setId(String id) {
        this.id = id;
    }
}

