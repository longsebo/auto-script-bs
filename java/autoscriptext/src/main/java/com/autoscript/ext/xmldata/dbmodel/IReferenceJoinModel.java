/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import com.autoscript.ext.xmldata.IToXml;

/**
 * 引用连接模型接口
 * 作者:龙色波
 * 日期:2014-5-11
 */
public interface IReferenceJoinModel extends IToXml {
	/**
	 * 设置父表列名
	 * @param parentColumnName
	 */
	public void setParentColumnName(String parentColumnName);
	/**
	 * 获取父表列名
	 * @return
	 */
	public String getParentColumnName();
	/**
	 * 设置子表列名
	 * @param childColumnName
	 */
	public void setChildColumnName(String childColumnName);
	/**
	 * 获取子表列名
	 * @return
	 */
	public String getChildColumnName();
}
