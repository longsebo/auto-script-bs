/**
 * 
 */
package com.autoscript.ext.xmldata.pdm.parse;

import com.autoscript.ext.xmldata.dbmodel.IDBModel;
import com.autoscript.ext.xmldata.dbmodel.ITableModel;

/**
 * pdm文件解析器接口
 * 作者:龙色波
 * 日期:2013-10-18
 */
public interface IPdmParse {
	//常量定义
	/**
	 * 属性名称
	 */
	public static final String NAME="Name";
	/**
	 * 属性代码
	 */
	public static final String CODE="Code";
	/**
	 * 属性注释
	 */
	public static final String COMMENT="Comment";
	/**
	 * 属性ID
	 */
	public static final String ID="Id";
	/**
	 * 数据类型
	 */
	public static final String DATATYPE="DataType";
	/**
	 * 是否为空
	 */
	public static final String MANDATORY="Column.Mandatory";
	/**
	 * 是否为空
	 */
	public static final String OLD_MANDATORY="Mandatory";
		
	/**
	 * 字段长度
	 */
	public static final String LENGTH="Length";
	/**
	 * 精度
	 */
	public static final String PRECISION="Precision";
	/**
	 * 基数
	 */
	public static final String CARDINALITY="Cardinality";
	/**
	 * 父表名
	 */
	public static final String PARENTTABLE="ParentTable";
	/**
	 * 子表名
	 */
	public static final String CHILDTABLE="ChildTable";
	/**
	 * 表名
	 */
	public static final String TABLE="Table";
	/**
	 * 引用模型父对象
	 */
	public static final String OBJECT1 ="Object1";
	/**
	 * 引用模型子对象
	 */
	public static final String OBJECT2 ="Object2";
	/**
	 * 主键列名列表节点名
	 */
	public static final String KEYCOLUMNS="Key.Columns";
	/**
	 * 列对象节点名，目前用于主键列名列表内部
	 */
	public static final String O_COLUMN="Column";
	/**
	 * 引用节点属性名
	 */
	public static final String REF="Ref";
	/**
	 * Domain节点名
	 */
	public static final String DOMAIN="Domain";
	/**
	 * 左括号
	 */
	public static final String LEFT_BRACKET="(";
	/**
	 * 右括号
	 */
	public static final String RIGHT_BRACKET=")";
	/**
	 * 唯一属性
	 */
	public static final String UNIQUE="Unique";
	/**
	 * 解析pdm文件
	 * @param pdmFileName pdm文件名 
	 * @return 返回pdm模型
	 * @throws Exception
	 */
	public IDBModel parse(String pdmFileName) throws Exception;		
	/**
	 * 解析pdm文件
	 * @param bytes pdm 文件内容 
	 * @return 返回pdm模型
	 * @throws Exception
	 */
	public IDBModel parse(byte bytes[]) throws Exception;		

}
