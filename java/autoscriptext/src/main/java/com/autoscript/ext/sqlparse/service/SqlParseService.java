package com.autoscript.ext.sqlparse.service;


import com.autoscript.ext.sqlparse.entity.TableItem;

import java.util.List;

public interface SqlParseService {

    /**
     * 解析创建语句
     * @param sql
     * @param dbtype
     * @return
     */
    List<TableItem> parseCreateSql(String sql, String dbtype);

    /**
     * 解析查询Sql
     * @param sql
     * @param dbtype
     * @return
     */
    List<TableItem> parseQuerySql(String sql, String dbtype);
}

