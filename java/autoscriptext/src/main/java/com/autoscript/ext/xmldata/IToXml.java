/**
 * 
 */
package com.autoscript.ext.xmldata;

/**
 * 转换为 xml接口
 * 作者:龙色波
 * 日期:2013-10-20
 */
public interface IToXml {
	/**
	 * 转换为xml
	 * @return
	 */
	public String toXml();
}
