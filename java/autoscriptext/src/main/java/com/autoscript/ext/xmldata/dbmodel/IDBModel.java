/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.List;

/**
 * DB模型接口
 * 作者:龙色波
 * 日期:2013-10-18
 */
public interface IDBModel {
	/**
	 * 转换为xml
	 * @return
	 * @throws Exception
	 */
	public String toXml() throws Exception;
	/**
	 * 获取表模型列表
	 * @return
	 */
	public List<ITableModel> getTableModels();
	/**
	 * 设置表模型列表
	 * @param tableModels
	 */
	public void setTableModels(List<ITableModel> tableModels);
	/**
	 * 获取参考模型列表
	 * @return
	 */
	public List<IReferenceModel> getReferenceModels();
	/**
	 * 设置参考模型列表
	 * @param referenceModels
	 */
	public void setReferenceModels(List<IReferenceModel> referenceModels);
	/**
	 *	 获取domain 模型列表
	 * @return
	 */
	public List<IDomainModel> getDomainModels();
	/**
	 * 	设置domain模型列表
	 * @param domainModels
	 */
	public void setDomainModels(List<IDomainModel> domainModels);	
}
