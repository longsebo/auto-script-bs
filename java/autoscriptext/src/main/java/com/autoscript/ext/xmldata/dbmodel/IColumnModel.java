/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import com.autoscript.ext.xmldata.IToXml;

/**
 * 列模型接口 作者:龙色波 日期:2013-10-19
 */
public interface IColumnModel extends IToXml {
	/**
	 * 设置表名中文名
	 * 
	 * @param name
	 */
	public void setName(String name);

	/**
	 * 获取表名中文名
	 * 
	 * @return
	 */
	public String getName();

	/**
	 * 设置表名英文名
	 * 
	 * @param code
	 */
	public void setCode(String code);

	/**
	 * 获取表名英文名
	 * 
	 * @return
	 */
	public String getCode();

	/**
	 * 设置数据类型
	 * 
	 * @param dataType
	 *            数据类型
	 */
	public void setDataType(String dataType);

	/**
	 * 获取数据类型
	 * 
	 * @return
	 */
	public String getDataType();

	/**
	 * 设置字段长度
	 * 
	 * @param length
	 */
	public void setLength(int length);

	/**
	 * 获取字段长度
	 * 
	 * @return 字段长度
	 */
	public int getLength();

	/**
	 * 设置精度（小数位)
	 * 
	 * @param precision
	 */
	public void setPrecision(int precision);

	/**
	 * 获取精度(小数位)
	 * 
	 * @return
	 */
	public int getPrecision();

	/**
	 * 获取字段是否允许为空 true为必填
	 * 
	 * @return
	 */
	public boolean isMandatory();

	/**
	 * 设置字段是否允许为空
	 * 
	 * @param isMandatory
	 *            true为必填
	 */
	public void setMandatory(boolean isMandatory);

	/**
	 * 设置注释
	 * 
	 * @param comment
	 */
	public void setComment(String comment);

	/**
	 * 获取注释
	 * 
	 * @return
	 */
	public String getComment();

	/**
	 * 设置参考模型名称
	 * 
	 * @param referenceName
	 */
	public void setReferenceName(String referenceName);

	/**
	 * 获取参考模型名称
	 * 
	 * @return 返回参考模型名称
	 */
	public String getReferenceName();

	/**
	 * 设置id
	 * 
	 * @param id   唯一标识
	 */
	public void setId(String id);

	/**
	 * 获取id
	 * 
	 * @return 返回id
	 */
	public String getId();
	/**
	 *	 获取domain
	 * @return
	 */
	public String getDomain();
	/**
	 * 	设置domain
	 * @param domain
	 */
	public void setDomain(String domain);
}
