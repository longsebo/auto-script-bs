/**
 * 
 */
package com.autoscript.ext.xmldata.database.model.datasource;

import java.util.List;

/**
 * 数据源模型接口
 * 作者:龙色波
 * 日期:2013-10-28
 */
public interface IDataSourceModel {
	 /**
	  * 设置数据源项列表
	  * @param items
	  */
     public void setDataSourceItems(List<IDataSourceItemModel> items);
     /**
      * 获取数据源项列表
      * @return 数据源项列表
      */
     public List<IDataSourceItemModel> getDataSourceItems();
	 /**
	  * 验证
	  * @throws Exception
	  */
     public void verify() throws Exception;
}
