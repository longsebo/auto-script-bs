package com.autoscript.ext.xmldata.database;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.autoscript.ext.ISystemConstant;
import com.autoscript.ext.xmldata.ProcessInfo;
import com.autoscript.ext.xmldata.database.config.DBConfigProxy;
import com.autoscript.ext.xmldata.database.dbmodelext.ITableModelExt;
import com.autoscript.ext.xmldata.database.model.datasource.IDataSourceItemModel;
import com.autoscript.ext.xmldata.database.model.datasource.IDataSourceModel;
import com.autoscript.ext.xmldata.database.process.IDataSourceProcess;
import com.autoscript.ext.xmldata.database.process.MysqlDataSourceProcess;
import com.autoscript.ext.xmldata.database.process.OracleDataSourceProcess;
import com.autoscript.ext.xmldata.dbmodel.DBModel;
import com.autoscript.ext.xmldata.dbmodel.IDBModel;
import com.autoscript.ext.xmldata.dbmodel.ITableModel;
import com.autoscript.ui.helper.StringHelper;
/**
 * 数据库对话框
 *
 * 作者:龙色波
 * 日期:2013-10-22
 */
public class DataBaseDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3777251102369744024L;
	private final JPanel contentPanel = new JPanel();
	private JList leftTableList;
	private JList rightTableList;
	private DefaultListModel leftListModel;
	protected IDBModel leftDBModel;
	protected IDBModel rightDBModel;
	private DefaultListModel rightListModel;
	private JTextField textField_DriversClass;
	private JTextField textField_URL;
	private JTextField textField_user;
	private JPasswordField passwordField;
	protected IDataSourceItemModel selectDataSourceItemModel = null;
	private JComboBox dataSourceComboBox;
	private IDataSourceProcess dataSourceProcess;
	private JLabel loadTableLable;
	private ProcessInfo processMessage =new ProcessInfo();
	private boolean loadFinish;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DataBaseDialog dialog = new DataBaseDialog(null,"选择数据源",true);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DataBaseDialog(Frame frame, String title, boolean modal) {
		super(frame,title,modal);
		setBounds(100, 100, 554, 473);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
//		contentPanel.setLayout(null);
		JPanel northpanel ;
		
		northpanel  = createNorthPanel();
		getContentPane().add(northpanel, BorderLayout.NORTH);
		
		
		JPanel rightPanel = createRightPanel();
		JPanel leftPanel = createLeftPanel();

		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel);
		splitPane.setBounds(10, 61, 414, 249);
		//设置左右宽度比例
		splitPane.setDividerLocation(0.5);
		contentPanel.add(splitPane);
		
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				{
					JButton okButton = new JButton("确定");
					okButton.setAlignmentX(Component.RIGHT_ALIGNMENT);
					okButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							//检查是否选择表
							if(rightDBModel== null || rightDBModel.getTableModels()==null||rightDBModel.getTableModels().size()==0){
								JOptionPane.showMessageDialog(DataBaseDialog.this, "请选择表名!");
								return;
							}
							//填充列信息
							try{
								IDataSourceProcess dbProcess = makeDataSourceProcess(selectDataSourceItemModel);
								dbProcess.fillTabModelsInfo(selectDataSourceItemModel,rightDBModel.getTableModels());
								System.out.println(rightDBModel.toXml());
							}catch(Throwable e1){
								e1.printStackTrace();
								JOptionPane.showMessageDialog(DataBaseDialog.this, e1.getMessage());
								return;
							}
							DataBaseDialog.this.dispose();
						}
					});
					buttonPane.setLayout(new GridLayout(1, 3, 0, 0));
					
					loadTableLable = new JLabel("");
					loadTableLable.setHorizontalAlignment(SwingConstants.LEFT);
					buttonPane.add(loadTableLable);
					JButton cancelButton = new JButton("取消");
//					cancelButton.setAlignmentX(2.0f);
					cancelButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							if(rightDBModel!=null && rightDBModel.getTableModels()!=null
									&& rightDBModel.getTableModels().size()>0){
								rightDBModel.getTableModels().clear();	
							}
							DataBaseDialog.this.dispose();
						}
					});
					cancelButton.setActionCommand("Cancel");
					
					okButton.setActionCommand("OK");
					buttonPane.add(okButton);
					buttonPane.add(cancelButton);
					getRootPane().setDefaultButton(okButton);
				}
			}
		}
	}
	/**
	 * 创建北边的panel
	 * @return
	 */
	private JPanel createNorthPanel() {
		JPanel northpanel = new JPanel();
		GridBagLayout gbl_northpanel = new GridBagLayout();
		gbl_northpanel.columnWidths = new int[] {66, 367};
		gbl_northpanel.rowHeights = new int[]{23, 0, 0, 0, 0, 0, 0};
		gbl_northpanel.columnWeights = new double[]{1.0, 1.0};
		gbl_northpanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		northpanel.setLayout(gbl_northpanel);
		{
			{
				JLabel lblNewLabel = new JLabel("数据源：");
				lblNewLabel.setBounds(10, 14, 66, 15);
				lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
				GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
				gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
				gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel.gridx = 0;
				gbc_lblNewLabel.gridy = 0;
				northpanel.add(lblNewLabel, gbc_lblNewLabel);
			}
			
			dataSourceComboBox = new JComboBox();
			dataSourceComboBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					selectDataSourceItemModel  = (IDataSourceItemModel) dataSourceComboBox.getSelectedItem();
					
					dispDataSourceInfo(selectDataSourceItemModel);
				}
			});
			dataSourceComboBox.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					ItemSelectable itemSeltab;
					itemSeltab = e.getItemSelectable();
					Object objArray[];
					objArray = itemSeltab.getSelectedObjects();
					if(objArray!=null){
						if(objArray.length>0){
							selectDataSourceItemModel  = (IDataSourceItemModel)objArray[0];
							dispDataSourceInfo(selectDataSourceItemModel);
						}else{
							selectDataSourceItemModel = null;
						}
					}
					
				}
			});
			GridBagConstraints gbc_dataSourceComboBox = new GridBagConstraints();
			gbc_dataSourceComboBox.insets = new Insets(0, 0, 5, 0);
			gbc_dataSourceComboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_dataSourceComboBox.gridx = 1;
			gbc_dataSourceComboBox.gridy = 0;
			northpanel.add(dataSourceComboBox, gbc_dataSourceComboBox);
			JLabel label = new JLabel("驱动类名：");
			label.setHorizontalAlignment(SwingConstants.LEFT);
			GridBagConstraints gbc_label = new GridBagConstraints();
			gbc_label.anchor = GridBagConstraints.EAST;
			gbc_label.insets = new Insets(0, 0, 5, 5);
			gbc_label.gridx = 0;
			gbc_label.gridy = 1;
			northpanel.add(label, gbc_label);
			
			textField_DriversClass = new JTextField();
			textField_DriversClass.setEditable(false);
			GridBagConstraints gbc_textField_DriversClass = new GridBagConstraints();
			gbc_textField_DriversClass.insets = new Insets(0, 0, 5, 0);
			gbc_textField_DriversClass.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField_DriversClass.gridx = 1;
			gbc_textField_DriversClass.gridy = 1;
			northpanel.add(textField_DriversClass, gbc_textField_DriversClass);
			textField_DriversClass.setColumns(10);
			
			JLabel lblurl = new JLabel("数据库URL:");
			GridBagConstraints gbc_lblurl = new GridBagConstraints();
			gbc_lblurl.anchor = GridBagConstraints.EAST;
			gbc_lblurl.insets = new Insets(0, 0, 5, 5);
			gbc_lblurl.gridx = 0;
			gbc_lblurl.gridy = 2;
			northpanel.add(lblurl, gbc_lblurl);
			
			textField_URL = new JTextField();
			textField_URL.setEditable(false);
			GridBagConstraints gbc_textField_URL = new GridBagConstraints();
			gbc_textField_URL.insets = new Insets(0, 0, 5, 0);
			gbc_textField_URL.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField_URL.gridx = 1;
			gbc_textField_URL.gridy = 2;
			northpanel.add(textField_URL, gbc_textField_URL);
			textField_URL.setColumns(10);
			
			JLabel label_user = new JLabel("用户名：");
			GridBagConstraints gbc_label_user = new GridBagConstraints();
			gbc_label_user.anchor = GridBagConstraints.EAST;
			gbc_label_user.insets = new Insets(0, 0, 5, 5);
			gbc_label_user.gridx = 0;
			gbc_label_user.gridy = 3;
			northpanel.add(label_user, gbc_label_user);
			
			textField_user = new JTextField();
			textField_user.setEditable(false);
			GridBagConstraints gbc_textField_user = new GridBagConstraints();
			gbc_textField_user.insets = new Insets(0, 0, 5, 0);
			gbc_textField_user.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField_user.gridx = 1;
			gbc_textField_user.gridy = 3;
			northpanel.add(textField_user, gbc_textField_user);
			textField_user.setColumns(10);
			
			JLabel label_password = new JLabel("密码：");
			GridBagConstraints gbc_label_password = new GridBagConstraints();
			gbc_label_password.anchor = GridBagConstraints.EAST;
			gbc_label_password.insets = new Insets(0, 0, 5, 5);
			gbc_label_password.gridx = 0;
			gbc_label_password.gridy = 4;
			northpanel.add(label_password, gbc_label_password);
			
			passwordField = new JPasswordField();
			passwordField.setEditable(false);
			GridBagConstraints gbc_passwordField = new GridBagConstraints();
			gbc_passwordField.insets = new Insets(0, 0, 5, 0);
			gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
			gbc_passwordField.gridx = 1;
			gbc_passwordField.gridy = 4;
			northpanel.add(passwordField, gbc_passwordField);
			
			JPanel panel = new JPanel();
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.gridwidth = 2;
			gbc_panel.insets = new Insets(0, 0, 0, 5);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 5;
			northpanel.add(panel, gbc_panel);
			panel.setLayout(new GridLayout(1, 1, 30, 0));
			
			JButton querytabbutton = new JButton("获取表信息");
			querytabbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					loadDBTables(selectDataSourceItemModel);
				}
			});
			querytabbutton.setBounds(10, 14, 150, 30);
			querytabbutton.setHorizontalAlignment(SwingConstants.LEFT);
			panel.add(querytabbutton);
			
			JButton adddsbutton = new JButton("新增数据源");
			panel.add(adddsbutton);
			
			JButton editdsbutton = new JButton("编辑数据源");
			editdsbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(selectDataSourceItemModel!=null){
						DataSourceConfigDialog dialog = new DataSourceConfigDialog(selectDataSourceItemModel,null,"编辑数据源",true);
						dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
						dialog.setVisible(true);
						//重新填充数据源组合框
						fillDsComboBox(dataSourceComboBox);
					}
				}
			});
			panel.add(editdsbutton);
			
			JButton deldsbutton = new JButton("删除数据源");
			deldsbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(selectDataSourceItemModel!=null){
						if(JOptionPane.showConfirmDialog(DataBaseDialog.this, "是否删除该数据源?", "删除数据源", JOptionPane.YES_NO_OPTION)== JOptionPane.YES_OPTION){
					        delDataSource(selectDataSourceItemModel);
						}
					}
				}
			});
			panel.add(deldsbutton);
			adddsbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					DataSourceConfigDialog dialog = new DataSourceConfigDialog(null,null,"新增数据源",true);
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
					if(dialog.getDataSourceItemModel()!=null){
						//重新填充数据源组合框
						fillDsComboBox(dataSourceComboBox);
					}
				}
			});
		}
		//填充数据源组合框
		fillDsComboBox(dataSourceComboBox);
		return northpanel;
	}
	/**
	 * 删除数据源
	 * @param selectDataSourceItemModel2
	 */
	protected void delDataSource(IDataSourceItemModel selectDataSourceItemModel2) {
		try {
			DefaultComboBoxModel comModel = (DefaultComboBoxModel) dataSourceComboBox.getModel();
			IDataSourceModel dsModel = DBConfigProxy.getInstance().readDataSourceConfig();
			if(dsModel!=null){
				IDataSourceItemModel item;
				for(int i=0;i<dsModel.getDataSourceItems().size();i++){
					item = dsModel.getDataSourceItems().get(i);
					if(item.getName().equals(selectDataSourceItemModel2.getName())){
						comModel.removeElementAt(i);
						dsModel.getDataSourceItems().remove(i);
						break;
					}
				}
				DBConfigProxy.getInstance().saveDataSourceConfig();
				dataSourceComboBox.setModel(comModel);	
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage());
			return;
		}
	}

	/**
	 * 界面上显示数据源信息
	 * @param itemModel
	 */
	protected void dispDataSourceInfo(IDataSourceItemModel itemModel) {
		if(itemModel!=null){
		textField_DriversClass.setText(itemModel.getDriverClass());
		textField_URL.setText(itemModel.getURL());
		textField_user.setText(itemModel.getUser());
		passwordField.setText(itemModel.getPassword());
		}
		
	}

	private void fillDsComboBox(JComboBox dataSourceComboBox)  {
		DefaultComboBoxModel comModel = new DefaultComboBoxModel();
		//为模型添加数据
		try {
			IDataSourceModel dsModel = DBConfigProxy.getInstance().readDataSourceConfig();
			if(dsModel!=null){
				for(IDataSourceItemModel item:dsModel.getDataSourceItems()){
					//默认选择第一项
					if(selectDataSourceItemModel==null){
						selectDataSourceItemModel = item;
						dispDataSourceInfo(selectDataSourceItemModel);
					}
					comModel.addElement(item);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage());
			return;
		}
		dataSourceComboBox.setModel(comModel);
		
	}

	protected void fillLeftList(IDBModel model) {
		leftListModel = new DefaultListModel();
		for(ITableModel tableModel:model.getTableModels()){
			leftListModel.addElement(tableModel);
		}
		leftTableList.setModel(leftListModel);
	}

	protected void clearRightList() {
		rightListModel = new DefaultListModel();
		rightTableList.setModel(rightListModel);
		
	}

	protected void clearLeftList() {
		leftListModel = new DefaultListModel();
		leftTableList.setModel(leftListModel);
		
	}

	/**
	 * 创建左边面板
	 * @return
	 */
	private JPanel createLeftPanel() {
		GridBagLayout gridbag = new GridBagLayout();
		gridbag.rowWeights = new double[]{0.0, 0.0, 1.0};
		gridbag.columnWeights = new double[]{1.0, 1.0};

		JPanel leftPanel = new JPanel();


        leftPanel.setLayout(gridbag);
		JLabel noSelectTabLable = new JLabel("未选择的表:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
    	gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
    	gbc_lblNewLabel.gridwidth = 2;
    	gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
    	gbc_lblNewLabel.gridx = 0;
    	gbc_lblNewLabel.gridy = 0;
    	leftPanel.add(noSelectTabLable, gbc_lblNewLabel);
		JLabel filterLable = new JLabel("表名过滤：");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
    	gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
    	gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
    	gbc_lblNewLabel_1.gridx = 0;
    	gbc_lblNewLabel_1.gridy = 1;
    	leftPanel.add(filterLable, gbc_lblNewLabel_1);
		JTextField textField = new JTextField();
		textField.setColumns(20);
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				JTextField text = (JTextField) e.getSource();
				filterTable(leftDBModel,text.getText());
			}
		});
		GridBagConstraints gbc_textField = new GridBagConstraints();
    	gbc_textField.anchor = GridBagConstraints.WEST;
    	gbc_textField.insets = new Insets(0, 0, 5, 0);
    	gbc_textField.fill = GridBagConstraints.HORIZONTAL;
    	gbc_textField.gridx = 1;
    	gbc_textField.gridy = 1;
    	leftPanel.add(textField, gbc_textField);
		
		
		leftTableList = new JList();
		JScrollPane leftListPane;
		leftListPane = new JScrollPane(leftTableList);
		GridBagConstraints gbc_list = new GridBagConstraints();
    	gbc_list.gridwidth = 2;
    	gbc_list.insets = new Insets(0, 0, 0, 5);
    	gbc_list.fill = GridBagConstraints.BOTH;
    	gbc_list.gridx = 0;
    	gbc_list.gridy = 2;
    	leftPanel.add(leftListPane, gbc_list);		
//		leftPanel.add(leftListPane,"Center");
		return leftPanel;
	}
	/**
	 * 过滤表
	 * @param leftPdmModel2
	 * @param text
	 */
	protected void filterTable(IDBModel leftPdmModel2, String text) {
		leftListModel = new DefaultListModel();
		for(ITableModel tableModel:leftPdmModel2.getTableModels()){
			if(!StringHelper.isEmpty(text)){
				String fullTableName;
				if(!StringHelper.isEmpty(((ITableModelExt)tableModel).getTableSchem())){
					fullTableName = ((ITableModelExt)tableModel).getTableSchem()+"."+
				                        tableModel.getCode();
				}else{
					fullTableName =   tableModel.getCode();
				}
				fullTableName = fullTableName.toLowerCase();
				text = text.toLowerCase();
				if(text.endsWith("*")){
					if(fullTableName.startsWith(text.substring(0,text.length()-1))){
						leftListModel.addElement(tableModel);
					}
				}else if(text.startsWith("*")){
					if(fullTableName.endsWith(text.substring(1,text.length()))){
						leftListModel.addElement(tableModel);
					}
				}else if(fullTableName.equals(text)){
					leftListModel.addElement(tableModel);
				}
			}else{
				leftListModel.addElement(tableModel);
			}
		}
		leftTableList.setModel(leftListModel);
	}

	/**
	 * 创建右边面板
	 * @return
	 */
	private JPanel createRightPanel() {
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BorderLayout(0, 0));
		
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(4, 1,0,30));
		rightPanel.add(buttonPanel, BorderLayout.WEST);
		JButton delButton = new JButton("<");
		delButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectIndexs[] = rightTableList.getSelectedIndices();
				for(int i=selectIndexs.length-1;i>=0;i--){
					leftListModel.addElement(rightListModel.get(selectIndexs[i]));
					leftDBModel.getTableModels().add((ITableModel) rightListModel.get(selectIndexs[i]));
					rightDBModel.getTableModels().remove(selectIndexs[i]);
					rightListModel.remove(selectIndexs[i]);
				}
				if(selectIndexs!=null && selectIndexs.length>0){
					leftTableList.setModel(leftListModel);
					rightTableList.setModel(rightListModel);
				}
			}
		});
		JButton delAllButton = new JButton("<<");
		delAllButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int count;
				count = rightListModel.size();
				for(int i=count-1;i>=0;i--){
					leftListModel.addElement(rightListModel.get(i));
					leftDBModel.getTableModels().add((ITableModel) rightListModel.get(i));
					rightDBModel.getTableModels().remove(i);
					rightListModel.remove(i);
				}
				leftTableList.setModel(leftListModel);
				rightTableList.setModel(rightListModel);
			}
		});
		JButton addButton = new JButton(">");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectIndexs[] = leftTableList.getSelectedIndices();
				for(int i=selectIndexs.length-1;i>=0;i--){
					rightListModel.addElement(leftListModel.get(selectIndexs[i]));
					rightDBModel.getTableModels().add((ITableModel) leftListModel.get(selectIndexs[i]));
					leftDBModel.getTableModels().remove(selectIndexs[i]);
					leftListModel.remove(selectIndexs[i]);
				}
				if(selectIndexs!=null && selectIndexs.length>0){
					rightTableList.setModel(rightListModel);
					leftTableList.setModel(leftListModel);
					rightTableList.updateUI();
					leftTableList.updateUI();
				}				
			}
		});
		JButton addAllButton = new JButton(">>");
		addAllButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int count;
				count = leftListModel.size();
				for(int i=count-1;i>=0;i--){
					rightListModel.addElement(leftListModel.get(i));
					rightDBModel.getTableModels().add((ITableModel) leftListModel.get(i));
					leftDBModel.getTableModels().remove(i);
					leftListModel.remove(i);
				}
				leftTableList.setModel(leftListModel);
				rightTableList.setModel(rightListModel);				
			}
		});
		buttonPanel.add(delButton);
		buttonPanel.add(delAllButton);
		buttonPanel.add(addButton);
		buttonPanel.add(addAllButton);
		rightTableList = new JList();
		JScrollPane listPane = new JScrollPane(rightTableList);
		rightPanel.add(listPane);
		
		JPanel panel = new JPanel();
		rightPanel.add(panel, BorderLayout.NORTH);
		
		JLabel label_1 = new JLabel("已选择的表:");
		panel.add(label_1);
		label_1.setBounds(179, 42, 101, 15);
		
		return rightPanel;
	}

	public IDBModel getRightPdmModel() {
		return rightDBModel;
	}
	/**
	 * 从数据源装入表模型，并填充界面
	 * @param itemModel
	 */
	private void loadDBTables(IDataSourceItemModel itemModel) {
		try {
			loadFinish = false;
			if(dataSourceProcess==null){
				dataSourceProcess = makeDataSourceProcess(itemModel);
				dataSourceProcess.setDispMsg(processMessage);
				leftDBModel= null;
			}
			//“所有的界面相关更新，都应当在 EDT 中执行”
			//“不要在 EDT 中执行耗时代码，耗时工作应当有一个单独的线程去处理”
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					try {
						leftDBModel =dataSourceProcess.parse(selectDataSourceItemModel);
						processMessage.setMessage("装入完成!");
					} catch (Throwable e) {
						processMessage.setMessage(e.getMessage());
						e.printStackTrace();
					}finally{
						loadFinish = true;
					}
					
				}
			};
			Runnable runnable1 = new Runnable() {

				@Override
				public void run() {
					do{
						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
							 try{
							 if(processMessage!=null){
							   loadTableLable.setText(processMessage.getMessage());
							 }

							 }catch(Throwable e){
								 e.printStackTrace();
							 }
							}
							});	
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}while(!loadFinish);
					loadTableLable.setText(processMessage.getMessage());
					 if(loadFinish && leftDBModel!=null){
						 rightDBModel = new DBModel();
						 //清空左边列表
						 clearLeftList();
						 //清空右边列表
						 clearRightList();
						 //填充左边列表
						 fillLeftList(leftDBModel);
					}
				}
				
			};
			
			new Thread(runnable1).start();
			new Thread(runnable).start();
		} catch (Exception e1) {
			e1.printStackTrace();
			JOptionPane.showMessageDialog(DataBaseDialog.this, e1.getMessage());
		}
	}
	/**
	 * 创建数据源处理器
	 * @param itemModel
	 * @return
	 * @throws Exception 
	 */
	private IDataSourceProcess makeDataSourceProcess(
			IDataSourceItemModel itemModel) throws Exception {
		if(ISystemConstant.DBTYPE_ORACE.equals(itemModel.getDBType())){
			return new OracleDataSourceProcess();
			
		}else if(ISystemConstant.DBTYPE_MYSQL.equals(itemModel.getDBType())){
				return new MysqlDataSourceProcess();
		}else {
			throw new Exception("不支持的数据库类型:"+itemModel.getDBType());
		}
	}
}
