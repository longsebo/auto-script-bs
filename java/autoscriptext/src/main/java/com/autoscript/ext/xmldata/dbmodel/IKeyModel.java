/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.List;

import com.autoscript.ext.xmldata.IToXml;

/**
 * 主键模型
 * 作者:龙色波
 * 日期:2013-10-20
 */
public interface IKeyModel extends IToXml {
	/**
	 * 设置表名中文名
	 * @param name
	 */
	public void setName(String name);
	/**
	 * 获取表名中文名
	 * @return
	 */
	public String getName();
	/**
	 * 设置表名英文名
	 * @param code
	 */
	public void setCode(String code);
	/**
	 * 获取表名英文名
	 * @return
	 */
	public String getCode();
	/**
	 * 设置主键列名列表
	 * @param columns
	 */
	public void setColumns(List<String> columns);
	/**
	 * 获取主键列名列表
	 * @return
	 */
	public List<String> getColumns();
}
