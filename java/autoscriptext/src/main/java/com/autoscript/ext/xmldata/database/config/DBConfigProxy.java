/**
 * 
 */
package com.autoscript.ext.xmldata.database.config;

import java.io.FileNotFoundException;

import com.autoscript.ext.ISystemConstant;
import com.autoscript.ext.xmldata.database.model.datasource.DataSourceModel;
import com.autoscript.ext.xmldata.database.model.datasource.IDataSourceModel;
import com.autoscript.ui.helper.FileCtrlUtils;
import com.autoscript.ui.helper.SerialHelper;

/**
 * 数据源配置类代理
 * 作者:龙色波
 * 日期:2013-10-28
 */
public class DBConfigProxy {
	private static DBConfigProxy proxy;
	private static IDataSourceModel dataSourceConfig;
	private DBConfigProxy(){
		
	} 
	public synchronized static DBConfigProxy getInstance(){
		if(proxy==null){
			proxy = new DBConfigProxy();
		}
		return proxy;
	}
	/**
	 * 从xml读取数据源配置
	 * @return
	 * @throws FileNotFoundException
	 */
	public  IDataSourceModel readDataSourceConfig() throws FileNotFoundException{
		String xmlFile;
		xmlFile = ISystemConstant.DATASOURCE_CONFFILE;
		if(dataSourceConfig==null){
			if(FileCtrlUtils.isExists(xmlFile)){
				dataSourceConfig = (IDataSourceModel) SerialHelper.readFromXml(xmlFile);
			}else{
				dataSourceConfig = new DataSourceModel();
			}
		}
		return dataSourceConfig;
	}
	/**
	 * 数据源配置复位，方便重读
	 */
	public void resetDataSourceConfig(){
		dataSourceConfig = null;
	}
	/**
	 * 保存数据源配置
	 * @throws FileNotFoundException
	 */
	public void saveDataSourceConfig() throws FileNotFoundException{
		String xmlFile;
		xmlFile = ISystemConstant.DATASOURCE_CONFFILE;
		if(dataSourceConfig!=null){
			SerialHelper.saveToXml(xmlFile, dataSourceConfig);
		}
	}
}
