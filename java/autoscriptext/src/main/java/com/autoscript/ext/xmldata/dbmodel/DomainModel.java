/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.XMLHelper;

/**
 * @author Administrator
 *	 列Domain
 */
public class DomainModel implements IDomainModel {
	/**
	 * id
	 */
	private String id;
	/**
	 * 	名称
	 */
	private String name;
	/**
	 * 	编码
	 */
	private String code;
	/**
	 * 	数据类型
	 */
	private String dataType;
	/**
	 * 	数据类型长度
	 */
	private int length;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	@Override
	public String toXml() {
		StringBuilder builder = new StringBuilder();
		builder.append("<Doamin ");
		if(id!=null) {
			builder.append("id=\"").append(XMLHelper.transferred(id)).append("\" ");
		}
		builder.append(" code=\"").append(XMLHelper.transferred(code)).append("\" ");
		if(name!=null){
			builder.append("name=\"").append(XMLHelper.transferred(name)).append("\" ");
		}
		
		builder.append("datatype=\"").append(XMLHelper.transferred(dataType)).append("\" ");
		if(length>0){
			builder.append("length=\"").append(length).append("\" ");
		}
		
		builder.append("/>\r\n");
		return builder.toString();
	}
	
}
