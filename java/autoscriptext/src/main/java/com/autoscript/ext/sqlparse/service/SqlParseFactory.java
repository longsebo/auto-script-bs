package com.autoscript.ext.sqlparse.service;


import com.autoscript.ext.sqlparse.SqlParseConstant;

import java.util.Objects;
import java.util.logging.Logger;

public class SqlParseFactory {

    private static final Logger log = Logger.getLogger(String.valueOf(SqlParseFactory.class));

    /**
     * 多个线程见可见
     */
    private static volatile SqlParseFactory sqlParseFactory = null;

    private SqlParseFactory() {

    }

    /**
     * 线程同步
     * @return
     */
    public static synchronized SqlParseFactory getInstance() {
        if (Objects.isNull(sqlParseFactory)) {
            return new SqlParseFactory();
        }
        return sqlParseFactory;
    }

    public MysqlSqlParse getService(String dbType){
        if(SqlParseConstant.MYSQL.equalsIgnoreCase(dbType)){
            return new MysqlSqlParse();

        }
//        return new OracleSqlParse();

        return null;
    }
}
