/**
 * 
 */
package com.autoscript.ext.xmldata.database.dbmodelext;

import com.autoscript.ext.xmldata.dbmodel.IColumnModel;

/**
 * 列扩展模型
 * 作者:龙色波
 * 日期:2013-10-29
 */
public interface IColumnModelExt extends IColumnModel {
	/**
	 * 设置java.sql.Types 的 SQL 类型
	 * @param dataType
	 */
	public void setJavaDataType(int dataType); 
	/**
	 * 设置基数（通常为 10 或 2）
	 * @param numPrecRadix
	 */
	public void setNumPrecRadix(int numPrecRadix);
	/**
	 * 设置列的缺省值
	 * @param columnDef
	 */
	public void setColumnDef(String columnDef);
	/**
	 * 对于 char 类型，该长度是列中的最大字节数
	 * @param charOctetLength
	 */
	public void setCharOctetLength(int charOctetLength);
	/**
	 * 表中的列的索引（从 1 开始）
	 * @param ordinalPosition
	 */
	public void setOrdinalPosition(int ordinalPosition);
	/**
	 * ISO 规则用于确定列是否包括 null。 
	 * YES --- 如果参数可以包括 NULL 
	 * NO --- 如果参数不可以包括 NULL 
	 * 空字符串 --- 如果不知道参数是否可以包括 null 
	 * @param isNullable
	 */
	public void setIsNullable(String isNullable);
	/**
	 * 指示此列是否自动增加 
	 * YES --- 如果该列自动增加 
	 * NO --- 如果该列不自动增加 
	 * 空字符串 --- 如果不能确定该列是否是自动增加参数 
	 * @param isAutoincrement
	 */
	public void setIsAutoincrement(String isAutoincrement);
	/**
	 * 获取java.sql.Types 的 SQL 类型
	 * @return
	 */
	public int getJavaDataType(); 
	/**
	 * 获取基数（通常为 10 或 2）
	 * @return
	 */
	public int getNumPrecRadix();
	/**
	 * 获取列的缺省值
	 * @return
	 */
	public String getColumnDef();
	/**
	 * 获取对于 char 类型，该长度是列中的最大字节数
	 * @return
	 */
	public int getCharOctetLength();
	/**
	 * 表中的列的索引（从 1 开始）
	 * @return
	 */
	public int getOrdinalPosition();
	/**
	 * ISO 规则用于确定列是否包括 null。 
	 * YES --- 如果参数可以包括 NULL 
	 * NO --- 如果参数不可以包括 NULL 
	 * 空字符串 --- 如果不知道参数是否可以包括 null  
	 * @return
	 */
	public String getIsNullable();
	/**
	 * 指示此列是否自动增加 
	 * YES --- 如果该列自动增加 
	 * NO --- 如果该列不自动增加 
	 * 空字符串 --- 如果不能确定该列是否是自动增加参数  
	 * @return
	 */
	public String getIsAutoincrement();
}
