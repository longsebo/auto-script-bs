/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.ArrayList;
import java.util.List;

/**
 * db模型
 * 作者:龙色波
 * 日期:2013-10-18
 */
public class DBModel implements IDBModel {
	private List<ITableModel> tableModels  = new ArrayList<ITableModel>();
	private List<IReferenceModel> referenceModels = new ArrayList<IReferenceModel>(); 
	/**
	 * domain模型列表
	 */
	private List<IDomainModel> domainModels;
	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.pdm.model.IPdmModel#toXml()
	 */
	@Override
	public String toXml() throws Exception {
		String xmlHead="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<DB>\r\n";
		StringBuilder xml = new StringBuilder();
		xml.append(xmlHead);
		if(tableModels!=null){
			for(ITableModel tableModel:tableModels){
				xml.append(tableModel.toXml());
			}
			
		}
		//domain模型
		if(domainModels!=null) {
			if(domainModels.size()>0) {
				for(IDomainModel domainModel:domainModels) {
					xml.append(domainModel.toXml());
				}
			}
		}
		//参考模型
		if(referenceModels!=null){
			if(referenceModels.size()>0){
				for(IReferenceModel referenceModel:referenceModels){
					xml.append(referenceModel.toXml());
				}
			}
		}
		xml.append("</DB>\r\n");
		
		return xml.toString();
	}
	public List<ITableModel> getTableModels() {
		return tableModels;
	}
	public void setTableModels(List<ITableModel> tableModels) {
		this.tableModels = tableModels;
	}
	@Override
	public List<IReferenceModel> getReferenceModels() {
		return referenceModels;
	}
	@Override
	public void setReferenceModels(List<IReferenceModel> referenceModels) {
		this.referenceModels = referenceModels;
	}
	@Override
	public List<IDomainModel> getDomainModels() {
		return domainModels;
	}
	@Override
	public void setDomainModels(List<IDomainModel> domainModels) {
		this.domainModels = domainModels;
	}
}
