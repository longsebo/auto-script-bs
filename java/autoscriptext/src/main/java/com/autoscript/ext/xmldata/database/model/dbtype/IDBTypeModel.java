/**
 * 
 */
package com.autoscript.ext.xmldata.database.model.dbtype;

/**
 * 数据库类型模型
 * 作者:龙色波
 * 日期:2013-11-1
 */
public interface IDBTypeModel {
	/**
	 * 设置数据库类型
	 * @param dbType
	 */
	public void setDBType(String dbType);
	/**
	 * 获取数据库类型
	 * @return
	 */
	public String getDBType();
	/**
	 * 设置获取表注释的SQL
	 * @param sql
	 * @remark SQL格式为：动态条件为表名，结果列只为一列为表注释
	 * 例如：select COMMENTS from user_tab_comments where table_name=?
	 */
	public void setTableCommentsSQL(String sql);
	/**
	 * 获取表注释SQL
	 * @return
	 */
	public String getTableCommentsSQL();
	/**
	 * 设置获取列注释SQL
	 * @param sql
	 * @remark SQL格式为：动态条件为表名，结果列只为一列为表注释 
	 */
	public void setColumnCommentsSQL(String sql);
	/**
	 * 获取列注释SQL
	 * @return
	 */
	public String getColumnCommentsSQL();
}
