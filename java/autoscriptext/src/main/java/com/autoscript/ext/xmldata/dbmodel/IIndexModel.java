/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.List;

import com.autoscript.ext.xmldata.IToXml;

/**
 * 索引模型接口
 * 作者:龙色波
 * 日期:2013-10-19
 */
public interface IIndexModel extends IToXml {
	/**
	 * 设置表名中文名
	 * @param name
	 */
	public void setName(String name);
	/**
	 * 获取表名中文名
	 * @return
	 */
	public String getName();
	/**
	 * 设置表名英文名
	 * @param code
	 */
	public void setCode(String code);
	/**
	 * 获取表名英文名
	 * @return
	 */
	public String getCode();
	/**
	 * 设置列名列表
	 * @param columnNames 列名列表 
	 */
	public void setColumnNames(List<String> columnNames);
	/**
	 * 获取列名列表
	 * @return 列名列表
	 */
	public List<String> getColumnNames();
}
