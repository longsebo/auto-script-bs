/**
 * 
 */
package com.autoscript.ext.xmldata.database.dbmodelext;

import com.autoscript.ext.xmldata.dbmodel.IIndexModel;

/**
 * 索引模型扩展
 * 作者:龙色波
 * 日期:2013-10-29
 */
public interface IIndexModelExt extends IIndexModel {
	/**
	 * 是否唯一键索引
	 * @param nonUnique
	 */
	public void setNonUnique(boolean nonUnique);
	/**
	 * 索引类别
	 * @param indexQualifier
	 */
	public void setIndexQualifier(String indexQualifier);
	/**
	 * 索引类型
	 * @param type
	 */
	public void setType(String type);
	/**
	 * 列排序序列，"A" => 升序，"D" => 降序，如果排序序列不受支持，可能为 null
	 * @param ascOrDesc
	 */
	public void setAscOrDesc(String ascOrDesc);
	/**
	 * 是否唯一键索引
	 * @return
	 */
	public boolean isNonUnique();
	/**
	 * 索引类别
	 * @return
	 */
	public String getIndexQualifier();
	/**
	 * 索引类型
	 * @return
	 */	
	public String getType();
	/**
	 * 列排序序列，"A" => 升序，"D" => 降序，如果排序序列不受支持，可能为 null
	 * @return
	 */	
	public String getAscOrDesc();
}
