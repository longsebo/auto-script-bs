/**
 * 
 */
package com.autoscript.ext.xmldata.dbmodel;

import java.util.ArrayList;
import java.util.List;

import com.autoscript.ui.helper.XMLHelper;
import com.autoscript.ui.model.xml.IXmlAttribute;
import com.autoscript.ui.model.xml.IXmlNode;

/**
 * 表模型实现
 * 作者:龙色波
 * 日期:2013-10-19
 */
public class TableModel implements ITableModel {
    /**
     * 表英文名
     */
	private String code;
	/**
	 * 表注解
	 */
    private String comment;
    /**
     * 表中文名
     */
    private String name;
    /**
     * 列模型列表
     */
    private List<IColumnModel> columnModels;
    /**
     * 索引模型列表
     */
    private List<IIndexModel> indexModels;
    /**
     * 主键模型
     */
	private IKeyModel keyModel;
	/**
	 * 唯一标识
	 */
	private String id;

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.pdm.model.ITableModel#getCode()
	 */
	@Override
	public String getCode() {
		return code;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.pdm.model.ITableModel#getComment()
	 */
	@Override
	public String getComment() {
		return comment;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.pdm.model.ITableModel#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.pdm.model.ITableModel#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.pdm.model.ITableModel#setComment(java.lang.String)
	 */
	@Override
	public void setComment(String comment) {
		this.comment = comment;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ext.xmldata.pdm.model.ITableModel#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public List<IColumnModel> getColumnModels() {
		return columnModels;
	}

	@Override
	public void setColumnModels(List<IColumnModel> columnModels) {
		this.columnModels = columnModels;
	}

	@Override
	public void setIndexModels(List<IIndexModel> indexModels) {
		this.indexModels = indexModels;
		
	}

	@Override
	public List<IIndexModel> getIndexModels() {
		return indexModels;
	}

	@Override
	public IKeyModel getKey() {
		return keyModel;
	}

	@Override
	public void setKey(IKeyModel keyModel) {
		this.keyModel = keyModel;
	}

	@Override
	public String toXml() {
		StringBuilder builder = new StringBuilder();
		//头部xml
		builder.append("<Table ").append(" code=\"")
		.append(XMLHelper.transferred(code)).append("\"");
		if(name!=null){
		 builder.append(" name=\"").append(XMLHelper.transferred(name))
		.append("\"");
		}
		if(comment!=null){
			builder.append(" comment=\"")
		.append(XMLHelper.transferred(comment)).append("\"");
		}
		builder.append(" >\r\n");
		//转换列为xml
		for(IColumnModel column:columnModels){
			builder.append(column.toXml()).append("\r\n");
		}
		//转换主键为xml
		if(keyModel!=null){
			builder.append(keyModel.toXml()).append("\r\n");
		}
		//转换索引为xml
		if(indexModels!=null && indexModels.size()>0){
			for(IIndexModel indexModel:indexModels){
				builder.append(indexModel.toXml()).append("\r\n");
			}
		}
		//尾部
		builder.append("</Table>\r\n");
		return builder.toString();
	}
	/**
	 * 转换为String 
	 * @return
	 */
	public String toString(){
		return code+"("+name+")";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public TableModel() {
		
	}
	/**
	 * 	直接根据xml节点构造表模型，即将xml节点转换为TableModel
	 * @param xmlNode
	 */
	public TableModel(IXmlNode xmlNode) {
		//设置表属性
		setCode(xmlNode.getAttributeVal("code"));
		setName(xmlNode.getAttributeVal("name"));
		
		this.columnModels = new ArrayList<IColumnModel>();
		
		//循环转换列对象,主键对象
		for(IXmlNode childNode:xmlNode.getChildNodes()) {
			if(childNode.getName().equals("Column")) {
				columnModels.add(new ColumnModel(childNode));
			}else if(childNode.getName().equals("PrimaryKey")) {
				this.keyModel = new KeyModel(childNode);		
			}
		}
	}

}
