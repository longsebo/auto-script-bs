package com.autoscript.ext.xmldata.sql.parse;

import com.autoscript.ext.sqlparse.SqlParseConstant;
import com.autoscript.ext.xmldata.dbmodel.IDBModel;
import com.autoscript.ui.helper.FileCtrlUtils;
import junit.framework.TestCase;
import org.junit.Test;

import java.io.File;

public class SqlParseTest extends TestCase {
    /**
     * 测试sql解析
     */
    @Test
    public void testParseCreateSql() throws Exception {
//        String ddl="\n" +
//                "CREATE TABLE `pm_company_finance` (\n" +
//                "  `ID` bigint NOT NULL COMMENT 'ID',\n" +
//                "  `USER_ID` bigint DEFAULT NULL COMMENT '用户ID',\n" +
//                "  `USER_NAME` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户姓名',\n" +
//                "  `REMARK` varchar(300) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',\n" +
//                "  `IS_VALID` char(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否有效',\n" +
//                "  `PHONE` char(11) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户手机号',\n" +
//                "  `ATTRIBUTE1` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段1',\n" +
//                "  `ATTRIBUTE2` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段2',\n" +
//                "  `ATTRIBUTE3` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段3',\n" +
//                "  `ATTRIBUTE4` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段4',\n" +
//                "  `ATTRIBUTE5` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段5',\n" +
//                "  `ATTRIBUTE6` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段6',\n" +
//                "  `ATTRIBUTE7` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段7',\n" +
//                "  `ATTRIBUTE8` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段8',\n" +
//                "  `ATTRIBUTE9` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段9',\n" +
//                "  `ATTRIBUTE10` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展字段10',\n" +
//                "  `created_by` bigint DEFAULT NULL COMMENT '创建人id',\n" +
//                "  `created_time` datetime DEFAULT NULL COMMENT '创建时间',\n" +
//                "  `updated_by` bigint DEFAULT NULL COMMENT '更新人id',\n" +
//                "  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',\n" +
//                "  PRIMARY KEY (`ID`)\n" +
//                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='公司财务人员';";
//        String ddl1="create table MS_HOSPITAL_DEPARTMENT\n" +
//                "(\n" +
//                "   ID                   bigint not null  comment '',\n" +
//                "   MS_HOSPITAL_INFO_ID  bigint  comment '',\n" +
//                "   HOSPITAL_NAME        varchar(300)  comment '',\n" +
//                "   MS_DEPARTMENT_INFO_ID bigint  comment '',\n" +
//                "   NAME                 varchar(300)  comment '',\n" +
//                "   ATTRIBUTE1           varchar(100)  comment '',\n" +
//                "   ATTRIBUTE2           varchar(100)  comment '',\n" +
//                "   ATTRIBUTE3           varchar(100)  comment '',\n" +
//                "   ATTRIBUTE4           varchar(100)  comment '',\n" +
//                "   ATTRIBUTE5           varchar(100)  comment '',\n" +
//                "   ATTRIBUTE6           varchar(100)  comment '',\n" +
//                "   ATTRIBUTE7           varchar(100)  comment '',\n" +
//                "   ATTRIBUTE8           varchar(100)  comment '',\n" +
//                "   ATTRIBUTE9           varchar(100)  comment '',\n" +
//                "   ATTRIBUTE10          varchar(100)  comment '',\n" +
//                "   primary key (ID),\n" +
//                "   constraint FK_MS_HOSPI_REFERENCE_MS_HOSPI foreign key (MS_HOSPITAL_INFO_ID)\n" +
//                "      references MS_HOSPITAL_INFO (ID) on delete restrict on update restrict,\n" +
//                "   constraint FK_MS_HOSPI_REFERENCE_MS_DEPAR foreign key (MS_DEPARTMENT_INFO_ID)\n" +
//                "      references MS_DEPARTMENT_INFO (ID) on delete restrict on update restrict\n" +
//                ")\n" +
//                ";\n";
        String ddl1;
        ddl1 = FileCtrlUtils.readFileToString(new File("D:\\dfhc2022\\学会项目管理\\05实施\\learn_pm.sql"),"UTF-8");
        ISqlParse sqlParse = new SqlParse();

        try {
            IDBModel dbModel = sqlParse.parseCreateSql(ddl1, SqlParseConstant.MYSQL);
            System.out.println(dbModel.toXml());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}