/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.List;

import com.autoscript.ui.helper.FileCtrlUtils;
import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author Administrator
 * 判断文件是否存在
 */
public class FileIsExist implements TemplateMethodModel {

	@Override
	public Object exec(List args) throws TemplateModelException {
		//格式: FileIsExist(文件名)
		if(args.size()!=1 ){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","FileIsExist",args.size(),"1"));
		}
		String fileName;
		fileName = (String) args.get(0);
		if(StringHelper.isEmpty(fileName))
			throw new TemplateModelException(UIPropertyHelper.getString("exception.object_is_empty", "文件名"));
		return FileCtrlUtils.isExists(fileName);
	}

}
