/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.List;

import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.TypeConversionHelper;
import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author Administrator
 * 字符串倒查
 */
public class StringReverse implements TemplateMethodModel {

	@Override
	public Object exec(List args) throws TemplateModelException {
		//格式: StringReverse(字符串,被搜字符串,结束位置)
		//返回：找到，返回>=0位置；否则返回-1
		if(args.size()!=3 ){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","StringReverse",args.size(),"3"));
		}
		String strMonther;
		strMonther = (String) args.get(0);
		String strFind;
		strFind = (String) args.get(1);
		String strEndPos;
		strEndPos = (String) args.get(2);
		//去掉逗号
		strEndPos = StringHelper.replaceAllIgnoreCase(strEndPos, ",", "");

		int endPos;
		try {
			endPos = TypeConversionHelper.StringToInt(strEndPos.trim());
		} catch (Exception e) {
			e.printStackTrace();
			throw new TemplateModelException(UIPropertyHelper.getString("exception.invalidateFuncParamType","StringReverse",3,"数字","字符串"));
		}
		//如果endPos大于strMonther长度报错
		if(endPos>strMonther.length()-1) 
			throw new TemplateModelException(UIPropertyHelper.getString("exception.subscript_out_of_range", endPos,strMonther.length()));
		String strTemp = strMonther.substring(0, endPos);
		int lastPos= strTemp.lastIndexOf(strFind);
		
		
		return lastPos;
		
	}

}
