/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.List;

import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author ASUS
 * 字符串格式化
 */
public class StringFormat implements TemplateMethodModel {
	/* (non-Javadoc)
	 * @see freemarker.template.TemplateMethodModel#exec(java.util.List)
	 * 第一个参数：参数格式
	 * 第二个及以后参数:为对象
	 */
	public Object exec(List args) throws TemplateModelException {
		if(args.size()<2){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_less","StringFormat",args.size(),2));
		}
		String format = (String)args.get(0);
		Object objs[];
		objs= new Object[args.size()-1];
		for(int i=1;i<args.size();i++){
			if(args.get(i) instanceof String ){
				//尝试转换为整型
				try{
					Integer ii = Integer.valueOf((String)args.get(i));
					objs[i-1]= ii;
				}catch(Exception e ){
					objs[i-1]= args.get(i);
				}
			}else{
				objs[i-1]= args.get(i);
			}
		}
		return String.format(format, objs);
	}
}
