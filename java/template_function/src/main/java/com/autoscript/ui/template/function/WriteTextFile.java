/**
 * 
 */
package com.autoscript.ui.template.function;

import java.io.File;
import java.util.List;

import com.autoscript.ui.helper.FileCtrlUtils;
import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author Administrator
 * 写入文本文件
 */
public class WriteTextFile implements TemplateMethodModel {

	@Override
	public Object exec(List args) throws TemplateModelException {
		//格式: WriteTextFile(文件名,文件内容,字符集)
		if(args.size()!=3 ){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","WriteTextFile",args.size(),"3"));
		}
		String fileName;
		fileName = (String) args.get(0);
		String fileContent;
		fileContent = (String) args.get(1);
		String encoding;
		encoding = (String) args.get(2);
		if(StringHelper.isEmpty(fileName))
			throw new TemplateModelException(UIPropertyHelper.getString("exception.object_is_empty", "文件名"));
		if(StringHelper.isEmpty(encoding))
			throw new TemplateModelException(UIPropertyHelper.getString("exception.object_is_empty", "字符集"));
		
		try {
			FileCtrlUtils.writeStringToFile(new File(fileName), fileContent, encoding);
		} catch (Exception e) {
			e.printStackTrace();
			throw new TemplateModelException(e.getMessage());
		}
		
		return "";
	}

}
