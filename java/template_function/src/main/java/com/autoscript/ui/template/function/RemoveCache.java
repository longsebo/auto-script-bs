/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.List;

import com.autoscript.ui.cache.ModulesCache;
import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author Administrator
 * 从缓存移除对象
 */
public class RemoveCache implements TemplateMethodModel {

	@Override
	public Object exec(List args) throws TemplateModelException {
		//格式:removecache(key名称)
		if(args.size()!=1){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","removecache",args.size(),1));
		}		
		String key=(String) args.get(0);		
		ModulesCache.getInstance().remove(key);
		return "";
	}

}
