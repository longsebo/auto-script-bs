package com.autoscript.ui.template.function;

import java.util.List;

import com.autoscript.ui.helper.FileCtrlUtils;
import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;
/**
 * 	创建目录
 * @author Administrator
 *
 */
public class CreateDir implements TemplateMethodModel {
	/**
	 *  就一个参数：目录名
	 */
	@Override
	public Object exec(List args) throws TemplateModelException {
		if(args.size()!=1){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","CreateDir",args.size(),1));
		}
		String dir = (String) args.get(0);
		if(!FileCtrlUtils.createDir(dir))
			throw new TemplateModelException("创建目录"+dir+"失败!");
		return "";
	}

}
