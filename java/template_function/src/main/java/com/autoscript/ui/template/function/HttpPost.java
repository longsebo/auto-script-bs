package com.autoscript.ui.template.function;

import com.autoscript.ui.helper.HttpClientUtil;
import com.autoscript.ui.helper.UIPropertyHelper;
import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * 封装http get请求  格式:httppost(url,postDataJson)
 *                  返回json格式
 */
public class HttpPost implements TemplateMethodModel {
    private static final Logger log = LoggerFactory.getLogger(HttpPost.class);
    @Override
    public Object exec(List list) throws TemplateModelException {
        if(list.size()!=2){
            throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","httppost",list.size(),2));
        }
        String url = (String) list.get(0);
        String postDataJosn = (String)list.get(1);
        try {
            return HttpClientUtil.postJson(url,postDataJosn);
        } catch (Exception e) {
//            e.printStackTrace();
            log.error("httppost失败",e);
            throw new TemplateModelException("请求"+url+"失败!");
        }
    }
}
