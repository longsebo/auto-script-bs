package com.autoscript.ui.cache;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Administrator 消息缓存
 */
public class MessageCache {
	private static MessageCache instance;
	private List<String> messageList;

	private MessageCache() {
		messageList = new ArrayList<String>();
	}

	public static MessageCache getInstance() {
		if (instance == null) {
			instance = new MessageCache();
		}
		return instance;
	}
	public void put(String msg) {
		messageList.add(msg);
	}

	public List<String> getMessageList() {
		return messageList;
	}
	
}
