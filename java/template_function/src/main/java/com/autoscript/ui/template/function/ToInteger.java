/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.List;

import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author ASUS
 * 字符串转换为整形
 */
public class ToInteger implements TemplateMethodModel {

	/* (non-Javadoc)
	 * @see freemarker.template.TemplateMethodModel#exec(java.util.List)
	 */
	@Override
	public Object exec(List args) throws TemplateModelException {
		//格式: ToInteger(字符串)
		if(args.size()!=1 ){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","ToInteger",args.size(),"1"));
		}
		
		return Integer.valueOf((String)args.get(0));
	}

}
