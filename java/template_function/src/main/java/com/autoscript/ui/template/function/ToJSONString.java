/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author Administrator
 * 	转换对象为JSON串
 */
public class ToJSONString implements TemplateMethodModel {

	@Override
	public Object exec(List args) throws TemplateModelException {
		// 调用格式:tojsonstring(对象)
		if(args.size()!=1){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","tojsonstring",args.size(),"1"));
		}
		return JSON.toJSONString(args.get(0));
	}

}
