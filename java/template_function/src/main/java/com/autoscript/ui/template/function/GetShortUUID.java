/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.ArrayList;
import java.util.List;

import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author Administrator
  *   获取短的唯一数(如果随机数位数为10位，10万两次 没重复，大于10万次没验证)
 */
public class GetShortUUID implements TemplateMethodModel {

	@Override
	public Object exec(List args) throws TemplateModelException {
		if(args.size()!=1 ){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","GetShortUUID",args.size(),"1"));
		}
		RandomNumber randomNum = new RandomNumber();
		List<String> args1 = new ArrayList<String>();
		//随机数位数
		args1.add(args.get(0).toString());
		//10进制
		args1.add("10");
		Long randomVal = Long.valueOf((String) randomNum.exec(args1));
		return Long.toHexString(System.currentTimeMillis()+randomVal);
	}

}
