/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author longsebo
 * 获取map的key列表  第一个参数为map的json,返回List<String>
 */
public class MapKeys implements TemplateMethodModel {

	/* (non-Javadoc)
	 * @see freemarker.template.TemplateMethodModel#exec(java.util.List)
	 */
	@Override
	public Object exec(List args) throws TemplateModelException {
		if(args.size()!=1){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","MapKeys",args.size(),1));
		}
		String mapJson = (String)args.get(0);
		if(StringHelper.isEmpty(mapJson)){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.map_isempty"));
		}
		Map map = JSON.parseObject(mapJson, HashMap.class);
		if(map == null){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.map_isempty"));
		}
		
		Iterator it = map.keySet().iterator();
		List<String> keys = new ArrayList<String>();
		while(it.hasNext()){
			keys.add(it.next().toString());
		}
		return keys;
	}

}
