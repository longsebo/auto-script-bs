/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.List;

import com.autoscript.ui.cache.ModulesCache;
import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author Administrator
 * 对象放入缓存
 */
public class PutCache implements TemplateMethodModel {

	@Override
	public Object exec(List args) throws TemplateModelException {
		//格式:putcache(key名称,对象)
		if(args.size()!=2){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","putcache",args.size(),2));
		}
		
		String key=(String) args.get(0);
		ModulesCache.getInstance().put(key, args.get(1));
		return "";
	}

}
