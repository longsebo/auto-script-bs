package com.autoscript.ui.cache;

import java.util.HashMap;
/**
 * 缓存对象
 * @author Administrator
 *
 */
public final class ModulesCache  {
	private static ModulesCache instance;
	private HashMap modules = new HashMap();

	public static ModulesCache getInstance() {
		if (instance == null) {
			instance = new ModulesCache();
		}
		return instance;
	}

	public void put(String key, Object obj) {
		this.modules.put(key, obj);
	}

	public Object get(String key) {
		return this.modules.get(key);
	}

	public int size() {
		return this.modules.size();
	}

	public Object remove(String key) {
		return modules.remove(key);
	}
}
