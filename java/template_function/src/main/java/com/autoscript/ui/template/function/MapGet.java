/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.autoscript.ui.helper.StringHelper;
import com.autoscript.ui.helper.UIPropertyHelper;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author longsebo
 * 获取哈希表值,第一个参数为map json串,第二个参数为key,返回value对象
 */
public class MapGet implements TemplateMethodModel {

	/* (non-Javadoc)
	 * @see freemarker.template.TemplateMethodModel#exec(java.util.List)
	 */
	@Override
	public Object exec(List args) throws TemplateModelException {
		if(args.size()!=2){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","MapGet",args.size(),2));
		}
		String mapJson = (String)args.get(0);
		if(StringHelper.isEmpty(mapJson)){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.map_isempty"));
		}
		Map map = JSON.parseObject(mapJson, HashMap.class);
		if(map == null){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.map_isempty"));
		}
		String key = (String)args.get(1);
		if(StringHelper.isEmpty(key)){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.mapkey_isempty"));
		}
		
		return map.get(key);
	}

}
