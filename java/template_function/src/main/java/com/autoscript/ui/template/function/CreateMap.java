/**
 * 
 */
package com.autoscript.ui.template.function;

import java.util.HashMap;
import java.util.List;

import com.alibaba.fastjson.JSON;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

/**
 * @author longsebo
 * 创建哈希表 无参数，返回值为Map的json
 */
public class CreateMap implements TemplateMethodModel {

	/* (non-Javadoc)
	 * @see freemarker.template.TemplateMethodModel#exec(java.util.List)
	 */
	@Override
	public Object exec(List arg0) throws TemplateModelException {
		HashMap map = new HashMap();
		return JSON.toJSONString(map);
	}

}
