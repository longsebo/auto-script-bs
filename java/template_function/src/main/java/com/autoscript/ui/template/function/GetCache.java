package com.autoscript.ui.template.function;

import  com.autoscript.ui.cache.ModulesCache;
import com.autoscript.ui.helper.UIPropertyHelper;
import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

import java.util.List;

/**
 * 
 * @author Administrator
 * 从缓存获取对象
 */
public class GetCache implements TemplateMethodModel {

	@Override
	public Object exec(List args) throws TemplateModelException {
		//格式:getcache(key名称)
		if(args.size()!=1){
			throw new TemplateModelException(UIPropertyHelper.getString("exception.func_para_number_not_match","getcache",args.size(),1));
		}
		
		String key=(String) args.get(0);		
		return ModulesCache.getInstance().get(key);
	}

}
