package com.autoscript.ui.helper;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class UIPropertyHelper {
	private static ResourceBundle bundle = null;

	private static final ResourceBundle getResourceBundle() {
		if (bundle == null)
			bundle = ResourceBundle.getBundle("com.autoscript.ui.message.ui");
		return bundle;
	}

	public static String getString(String key) {
		String value = null;
		try {
			value = getResourceBundle().getString(key);
		} catch (MissingResourceException e) {
			System.out
					.println("java.util.MissingResourceException: Couldn't find value for: "
							+ key);
		}
		if (value == null) {
			value = "Could not find resource: " + key + "  ";
		}
		return value;
	}

	public static char getMnemonic(String key) {
		return getString(key).charAt(0);
	}

	/**
	 * 带参数的获取消息名称
	 * 
	 * @param key
	 * @param params
	 * @return
	 */
	public static String getString(String key, Object... params) {
		try {
			String value = getString(key);
			return MessageFormat.format(value, params);
		} catch (Exception e) {
			e.printStackTrace();
			return key;
		}

	}
}
