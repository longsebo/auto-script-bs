/**
 * 
 */
package com.autoscript.ui.model.xml;

/**
 * xml属性实现类
 * 作者:龙色波
 * 日期:2013-10-13
 */
public class XmlAttribute implements IXmlAttribute {
	/**
	 * 属性名称
	 */
	private String name;
	/**
	 * 属性值
	 */
	private String value;
	/* (non-Javadoc)
	 * @see com.autoscript.ui.model.xml.IXmlAttribute#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
 
	/* (non-Javadoc)
	 * @see com.autoscript.ui.model.xml.IXmlAttribute#getValue()
	 */
	@Override
	public String getValue() {
		return value;
	}
	/* (non-Javadoc)
	 * @see com.autoscript.ui.model.xml.IXmlAttribute#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.autoscript.ui.model.xml.IXmlAttribute#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;
	}

}
