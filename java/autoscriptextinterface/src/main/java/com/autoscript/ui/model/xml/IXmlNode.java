/**
 * 
 */
package com.autoscript.ui.model.xml;

import java.util.List;

/**
 * xml节点接口
 * 作者:龙色波
 * 日期:2013-10-13
 */
public interface IXmlNode {
	/**
	 * 设置节点名
	 * @param name
	 */
	public void setName(String name);
	/**
	 * 获取节点名
	 * @return
	 */
	public String getName();
	/**
	 * 设置节点值
	 * @param value
	 */
	public void setValue(String value);
	/**
	 * 获取节点值
	 * @return
	 */
	public String getValue();	
	/**
	 * 获取子节点列表
	 * @return
	 */
	public List<IXmlNode> getChildNodes();
	/**
	 * 获取当前节点属性列表
	 * @return
	 */
	public List<IXmlAttribute> getAttributes();
	/**
	 * 以当前节点为起点根据路径获取子节点列表
	 * @param path 以.作为分隔符，例如node1.node2.node3
	 * @return
	 * @throws Exception 
	 */
	public List<IXmlNode> getChildNodesByPath(String path) throws Exception;
	/**
	 * 判断当前节点是否包含指定的属性名
	 * @param attrName 属性名
	 * @return 包含返回true;否则返回false
	 */
	public boolean isContain(String attrName);
	/**
	 * 根据属性名获取属性值
	 * @param attrName 属性名
	 * @return
	 */
	public String getAttributeVal(String attrName);
	/**
	 *    以当前节点为起点根据路径获取子节点列表,返回JSON格式字符串
	 * @param path 以.作为分隔符，例如node1.node2.node3
	 * @return
	 * @throws Exception 
	 */
	public String getChildNodesByPathForJSON(String path) throws Exception;	
}
