package com.autoscript.ui.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;


public class FileCtrlUtils {
    public static String getTempDirectoryPath() {
        return FileUtils.getTempDirectoryPath();
    }


    public static String getUserDirectoryPath() {
        return FileUtils.getUserDirectoryPath();
    }


    public static FileInputStream openInputStream(File file) throws Exception {
        return FileUtils.openInputStream(file);
    }


    public static FileOutputStream openOutputStream(File file) throws Exception {
        return FileUtils.openOutputStream(file);
    }


    public void touch(File file) throws Exception {
        FileUtils.touch(file);
    }


    public static File[] convertFileCollectionToFileArray(Collection files) {
        return FileUtils.convertFileCollectionToFileArray(files);
    }


    public static Collection listFiles(File directory, String[] extensions, boolean recursive) {
        return FileUtils.listFiles(directory, extensions, recursive);
    }


    public static boolean contentEquals(File file1, File file2) throws Exception {
        return FileUtils.contentEquals(file1, file2);
    }


    public static File[] toFiles(URL[] urls) {
        return FileUtils.toFiles(urls);
    }


    public static URL[] toURLs(File[] files) throws Exception {
        return FileUtils.toURLs(files);
    }


    public static void copyFileToDirectory(File srcFile, File destDir, boolean preserveFileDate) throws Exception {
        FileUtils.copyFileToDirectory(srcFile, destDir, preserveFileDate);
    }


    public static void copyFile(File srcFile, File destFile, boolean preserveFileDate) throws Exception {
        FileUtils.copyFile(srcFile, destFile, preserveFileDate);
    }


    public static void copyDirectory(File srcDir, File destDir, String filter, boolean preserveFileDate) throws Exception {
        IOFileFilter iOFileFilter = FileFilterUtils.suffixFileFilter(filter);
        FileUtils.copyDirectory(srcDir, destDir, iOFileFilter, preserveFileDate);
    }


    public static void deleteDirectory(File directory) throws Exception {
        FileUtils.deleteDirectory(directory);
    }


    public static String readFileToString(File file, String encoding) throws Exception {
        return FileUtils.readFileToString(file, encoding);
    }


    public static byte[] readFileToByteArray(File file) throws Exception {
        return FileUtils.readFileToByteArray(file);
    }


    public static List readLines(File file, String encoding) throws Exception {
        return FileUtils.readLines(file, encoding);
    }


    public static void writeStringToFile(File file, String data, String encoding) throws Exception {
        FileUtils.writeStringToFile(file, data, encoding);
    }


    public static void writeByteArrayToFile(File file, byte[] data) throws IOException {
        FileUtils.writeByteArrayToFile(file, data);
    }


    public static void writeLines(File file, String encoding, Collection lines, String lineEnding) throws IOException {
        FileUtils.writeLines(file, encoding, lines, lineEnding);
    }


    public static long sizeOf(File file) {
        return FileUtils.sizeOf(file);
    }


    public static boolean isFileNewer(File file, File reference) throws Exception {
        return FileUtils.isFileNewer(file, reference);
    }


    public static boolean isFileOlder(File file, File reference) throws Exception {
        return FileUtils.isFileOlder(file, reference);
    }


    public static void moveDirectory(File srcDir, File destDir) throws Exception {
        FileUtils.moveDirectory(srcDir, destDir);
    }


    public static void moveFile(File srcFile, File destFile) throws Exception {
        FileUtils.moveFile(srcFile, destFile);
    }


    public static void moveFileToDirectory(File srcFile, File destDir, boolean createDestDir) throws Exception {
        FileUtils.moveFileToDirectory(srcFile, destDir, createDestDir);
    }


    public static void main(String[] args) {
        System.out.println(getUserDirectoryPath());
    }


    public static boolean isExists(String fileName) {
        File file = new File(fileName);
        return file.exists();
    }


    public static String getFilePath(String fullFileName) {
        int lastpos = getLastPathIndicatePos(fullFileName);
        if (lastpos == -1) {
            return fullFileName;
        }
        return fullFileName.substring(0, lastpos + 1);
    }


    public static boolean createDir(String dir) {
        File file = new File(dir);
        return file.mkdirs();
    }


    public static boolean rename(String oldFileName, String newFileName) {
        File oldFile = new File(oldFileName);
        return oldFile.renameTo(new File(newFileName));
    }


    public static File createTempFile(String prefix, String suffix, String directory) throws IOException {
        return File.createTempFile(prefix, suffix, new File(directory));
    }


    public static long getTextFileLineNum(String fileName, String encode) throws Exception {
        LineNumberReader lineReader = null;
        InputStreamReader streamReader = null;
        FileInputStream fileStream = null;
        long sumNum = 0L;
        String line = "";
        try {
            fileStream = new FileInputStream(fileName);
            streamReader = new InputStreamReader(fileStream, encode);
            lineReader = new LineNumberReader(streamReader);
            while (line != null) {
                line = lineReader.readLine();
                if (line != null) {
                    sumNum++;
                }
            }
            return sumNum;
        } finally {
            if (fileStream != null) {
                try {
                    fileStream.close();
                } catch (IOException e) {
                    throw new Exception(e);
                }
            }
            if (streamReader != null) {
                try {
                    streamReader.close();
                } catch (IOException e) {
                    throw new Exception(e);
                }
            }
            if (lineReader != null) {
                try {
                    lineReader.close();
                } catch (IOException e) {
                    throw new Exception(e);
                }
            }
        }
    }


    public static String getFileName(String fullFileName) {
        int lastpos = getLastPathIndicatePos(fullFileName);
        if (lastpos == -1) {
            return fullFileName;
        }
        return fullFileName.substring(lastpos + 1);
    }


    private static int getLastPathIndicatePos(String fullFileName) {
        int lastpos = fullFileName.lastIndexOf('\\');
        if (lastpos == -1) {
            lastpos = fullFileName.lastIndexOf('/');
        } else {
            int lastpos1 = fullFileName.lastIndexOf('/');
            if (lastpos1 > lastpos) {
                lastpos = lastpos1;
            }
        }

        return lastpos;
    }


    public static String getParentPath(String path) {
        int lastpos = getLastPathIndicatePos(path);
        if (lastpos == -1) {
            return "";
        }

        if (lastpos == path.length() - 1) {
            lastpos = getLastPathIndicatePos(path.substring(0, lastpos));
            if (lastpos == -1) {
                return "";
            }
        }
        return path.substring(0, lastpos);
    }


    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        return file.delete();
    }


    public static long getFileLength(String fileName) {
        File file = new File(fileName);
        return file.length();
    }


    public static boolean contentRule(File file, String key) throws IOException {
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            if (file.exists()) {
                fileReader = new FileReader(file);

                bufferedReader = new BufferedReader(fileReader);

                while (bufferedReader.ready()) {
                    String line = bufferedReader.readLine();
                    if (line != null && line.contains(key)) {
                        return true;
                    }
                }
            }
            return false;
        } finally {

            if (fileReader != null) {
                fileReader.close();
            }
            if (bufferedReader != null)
                bufferedReader.close();
        }
    }
}
