package com.autoscript.ui.helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

public class TypeConversionHelper {
	public static int StringToInt(String str) throws Exception {
		try {
			Integer in = new Integer(str);
			return in.intValue();
		} catch (Exception e) {
		throw new Exception("String to int conversion failed", e);
		}
	}

	public static long StringToLong(String str) throws Exception {
		try {
			Long in = new Long(str);
			return in.longValue();
		} catch (Exception e) {
		
		throw new Exception("String to long conversion failed", e);}
	}

	public static String intToString(int str) throws Exception {
		try {
			Integer in = new Integer(str);
			return in.toString();
		} catch (Exception e) {
		throw new Exception("int to String conversion failed", e);
		}
	}

	public static String doubleToString(double d)
			throws Exception {
		try {
			Double dou = new Double(d);
			return dou.toString();
		} catch (Exception e) {
		
		throw new Exception(
				" doubule to String conversion failes", e);}
	}

	public static Integer doubleToInteger(double d)
			throws Exception {
		try {
			Double dou = new Double(d);
			Integer i = new Integer(dou.intValue());
			return i;
		} catch (Exception e) {
		
		throw new Exception(
				" double to Integer conversion failes", e);}
	}

	public static Vector convertToVector(HashMap map) {
		Iterator iter = map.keySet().iterator();
		Vector temp = new Vector();
		while (iter.hasNext())
			temp.addElement(map.get(iter.next()));

		return temp;
	}

	public static Vector convertToVector(Object[] anArray) {
		if (anArray == null)
			return null;
		Vector v = new Vector(anArray.length);
		for (int i = 0; i < anArray.length; i++) {
			v.addElement(anArray[i]);
		}
		return v;
	}

	public static Vector convertToVector(Object[][] anArray) {
		if (anArray == null)
			return null;
		Vector v = new Vector(anArray.length);
		for (int i = 0; i < anArray.length; i++) {
			v.addElement(convertToVector(anArray[i]));
		}
		return v;
	}

	public static String[][] convertToArray(Vector data) {
		if (data == null)
			return null;
		int length = data.size();
		String[][] datas = new String[length][2];
		for (int i = 0; i < length; i++) {
			datas[i][0] = ((String) ((Vector) data.elementAt(i)).elementAt(0));
			datas[i][1] = ((String) ((Vector) data.elementAt(i)).elementAt(1));
		}

		return datas;
	}

	public static String[][] convertNodeParamToArray(HashMap nodeParamHashMap) {
		if (nodeParamHashMap == null)
			return null;
		String[][] datas = new String[nodeParamHashMap.size()][2];

		Set keyset = nodeParamHashMap.keySet();
		Iterator keyiterator = keyset.iterator();

		Object[] subkeys = (Object[]) null;
		HashMap element = null;
		for (int i = 0; keyiterator.hasNext(); i++) {
			String key = (String) keyiterator.next();
			element = (HashMap) nodeParamHashMap.get(key);
			subkeys = element.keySet().toArray();
			for (int j = 0; j < subkeys.length; j++) {
				datas[i][0] = subkeys[j].toString();
				datas[i][1] = ((String) element.get(subkeys[j]));
			}
		}
		return datas;
	}

	public static HashMap cloneHashMap(HashMap orgHashMap,
			Object[] notCopyKeys, boolean isRecursion) {
		if (orgHashMap == null)
			return null;
		HashMap retHashMap = new HashMap();
		HashMap compareHashMap = new HashMap();

		if (notCopyKeys != null) {
			for (int i = 0; i < notCopyKeys.length; i++) {
				compareHashMap.put(notCopyKeys[i], "1");
			}
		}
		Object[] keys = orgHashMap.keySet().toArray();
		for (int i = 0; i < keys.length; i++) {
			if (!compareHashMap.containsKey(keys[i])) {
				if ((orgHashMap.get(keys[i]) instanceof HashMap)) {
					if (isRecursion)
						retHashMap.put(cloneObject(keys[i]),
								cloneObject(orgHashMap.get(keys[i])));
				} else
					retHashMap.put(cloneObject(keys[i]), cloneObject(orgHashMap
							.get(keys[i])));
			}
		}
		return retHashMap;
	}

	public static Object cloneObject(Object orgobj) {
		if (orgobj == null)
			return null;
		if ((orgobj instanceof String))
			return new String((String) orgobj);
		if ((orgobj instanceof Integer))
			return new Integer(((Integer) orgobj).intValue());
		if ((orgobj instanceof HashMap))
			return cloneHashMap((HashMap) orgobj, null, true);
		return null;
	}

	public static boolean isInt(String val) {
		if (val == null)
			return false;
		try {
			new Integer(val);
			return true;
		} catch (NumberFormatException e) {
		}
		return false;
	}

	public static String[][] convertToTradeInfoData(HashMap hashMap) {
		if (hashMap == null) {
			return null;
		}
		String[][] datas = new String[hashMap.size()][8];
		Object[] keys = hashMap.keySet().toArray();

		for (int i = 0; i < keys.length; i++) {
			HashMap rowHashMap = (HashMap) hashMap.get(keys[i]);
			datas[i][0] = ((String) rowHashMap.get("ACCDATE"));
			datas[i][1] = ((Integer) rowHashMap.get("TRDNUM")).toString();
			datas[i][2] = ((Integer) rowHashMap.get("FACVCRNUM")).toString();
			datas[i][3] = ((String) rowHashMap.get("FACTALTOTAMT"));
			datas[i][4] = ((Integer) rowHashMap.get("MACVCRNUM")).toString();
			datas[i][5] = ((String) rowHashMap.get("MACTALTOTAMT"));
			datas[i][6] = ((Integer) rowHashMap.get("BGTVCRNUM")).toString();
			datas[i][7] = ((String) rowHashMap.get("BGTTALTOTAMT"));
		}
		return datas;
	}
}
