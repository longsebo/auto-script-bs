/**
 * 
 */
package com.autoscript.ui.model.xml;

/**
 * Xml属性接口
 * 作者:龙色波
 * 日期:2013-10-13
 */
public interface IXmlAttribute {
	/**
	 * 设置属性名
	 * @param name
	 */
	public void setName(String name);
	/**
	 * 获取属性名
	 * @return
	 */
	public String getName();
	/**
	 * 设置属性值
	 * @param value
	 */
	public void setValue(String value);
	/**
	 * 获取属性值
	 * @return
	 */
	public String getValue();	
}
