/**
 * 
 */
package com.autoscript.ui.model.xml;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.autoscript.ui.helper.StringHelper;

/**
 * Xml节点实现类
 * 作者:龙色波 
 * 日期:2013-10-13
 */ 
public class XmlNode implements IXmlNode {
	/**
	 * 属性列表
	 */
	private List<IXmlAttribute> xmlAttributes = new ArrayList<IXmlAttribute>();
	/**
	 * 子节点列表
	 */
	private List<IXmlNode> xmlChildNodes = new ArrayList<IXmlNode>();
	/**
	 * 节点名称
	 */
	private String name;
	/**
	 * 节点值
	 */
	private String value;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.autoscript.ui.model.xml.IXmlNode#getAttributes()
	 */
	@Override
	public List<IXmlAttribute> getAttributes() {
		return xmlAttributes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.autoscript.ui.model.xml.IXmlNode#getChildNodes()
	 */
	@Override
	public List<IXmlNode> getChildNodes() {
		return xmlChildNodes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.autoscript.ui.model.xml.IXmlNode#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.autoscript.ui.model.xml.IXmlNode#getValue()
	 */
	@Override
	public String getValue() {
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.autoscript.ui.model.xml.IXmlNode#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.autoscript.ui.model.xml.IXmlNode#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public List<IXmlNode> getChildNodesByPath(String path)
			throws Exception {
		return getChildNodesByPath(null, path);
	}
	private List<IXmlNode> getChildNodesByPath(IXmlNode parentNode, String path)
			throws Exception {
		String[] nodeNames = StringHelper.split(path, '.');
		if (parentNode == null) {
			parentNode = this;
		}
		List<IXmlNode> resultNode = new ArrayList<IXmlNode>();

		for (IXmlNode childNode : parentNode.getChildNodes()) {
			if (childNode.getName().equals(nodeNames[0])) {
				// 如果不是最后一级，递归到下一级比较
				if (nodeNames.length > 1) {
					// 切换父节点
					List<IXmlNode> tmpNodes;
					int pos;
					pos = path.indexOf('.');
					String subPath;
					subPath = path.substring(pos + 1);
					tmpNodes = getChildNodesByPath(childNode, subPath);
					if (tmpNodes != null) {
						resultNode.addAll(tmpNodes);
					}
				} else {
					resultNode.add(childNode);
				}
			}
		}

		return resultNode;
	}

	@Override
	public String getAttributeVal(String attrName) {
		if(xmlAttributes!=null){
			for(IXmlAttribute attr:xmlAttributes){
				if(attr.getName().equals(attrName)){
					return attr.getValue();
				}
			}
			return "";
		}else{
			return "";
		}
	}

	@Override
	public boolean isContain(String attrName) {
		if(xmlAttributes!=null){
			for(IXmlAttribute attr:xmlAttributes){
				if(attr.getName().equals(attrName)){
					return true;
				}
			}
			return false;
		}else{
			return false;
		}
	}

	@Override
	public String getChildNodesByPathForJSON(String path) throws Exception {
		List<IXmlNode> list = getChildNodesByPath(path);
		
		return JSON.toJSONString(list);
	}

}
