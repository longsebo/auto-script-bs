/**
 * 
 */
package com.autoscript.extinterface.xmldata;

import javax.swing.JFrame;

/**
 * 构造xml数据接口
 *
 * 作者:龙色波
 * 日期:2013-10-22
 */
public interface IMakeXmlData {
	/**
	 * 构造xml接口方法
	 * @param frame
	 * @return
	 * @throws Exception
	 */
	public String makeXml(JFrame frame) throws Exception;
}
