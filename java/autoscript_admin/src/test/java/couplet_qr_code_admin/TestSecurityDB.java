/**
 * 
 */
package couplet_qr_code_admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.autoscript.exception.ASException;
import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;

/**
 * @author Administrator
 *
 */
public class TestSecurityDB {
   @Test
   public void testSearch() {
	  String url="http://119.3.179.135:9999";
	  SecurityDBClientService.getInstance().setUrlPrefix(url);	
	  String password="lxb741126";
	  String userName="longsebo";
	  String database="autoscriptdev";
	  String dbSessionId = SecurityDBClientService.getInstance().login(userName, password, database);
	  if(StringHelper.isEmpty(dbSessionId))
			throw new ASException("登录失败!");
	  Map<String, Object> searchVo = new HashMap<String,Object>();
//	  searchVo.put("MOBILE_PHONE_NO","13520696966");
	  List<Map> maps = SecurityDBClientService.getInstance().search(dbSessionId, "AS_PROJECT", searchVo);
	  if(maps==null||maps.size()==0)
		 throw new ASException("手机号不正确!");
	  System.out.println(maps);
   }

}
