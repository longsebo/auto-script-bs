package couplet_qr_code_admin;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class TestJavaDump {

	public static void main(String[] args) throws InterruptedException {
		//testMemLeak();
        try {
			testFileNotClose();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void testFileNotClose() throws FileNotFoundException, InterruptedException {
		while(true) {
			FileInputStream in = new FileInputStream("1.txt");
			Thread.sleep(10);
		}
		
	}

	private static void testMemLeak() throws InterruptedException {
		List<String> list = new ArrayList<String>();
		while(true) {
			String str1 = new String("4q04sdlfjsfjsdlgjsdfjdsflvjalsdfjasdlfaslasfasldfjasldjasfg");
			list.add(str1);
			Thread.sleep(10);
		}
	}

}
