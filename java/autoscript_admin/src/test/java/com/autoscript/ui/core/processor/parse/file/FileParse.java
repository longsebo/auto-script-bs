//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.autoscript.ui.core.processor.parse.file;

import com.autoscript.ui.core.processor.parse.function.CreateTextFileParse;
import com.autoscript.ui.helper.FileCtrlUtils;
import com.autoscript.ui.helper.StringHelper;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextArea;

public class FileParse implements IFileParse {
    private JTextArea outputTextArea;

    public FileParse(JTextArea outputTextArea) {
        this.outputTextArea = outputTextArea;
    }

    public void parse(String fileName) throws Exception {
        String[] keys = new String[]{"@{createTextFile(", ")}", "@{closeFile()}"};
        List lines = FileCtrlUtils.readLines(new File(fileName), "UTF-8");
        int startpos = -1;
        boolean isWriting = false;
        String newFileName = "";
        List<String> writerLines = new ArrayList();
        CreateTextFileParse funParse = new CreateTextFileParse();

        for(int i = 0; i < lines.size(); ++i) {
            String line = (String)lines.get(i);
            if (StringHelper.findStrNum(line, keys[0]) > 0 && StringHelper.findStrNum(line, keys[1]) > 0) {
                startpos = StringHelper.findStrIgnoreCase(line, keys[0], 0);
                if (startpos == -1) {
                    System.out.println("行内容:" + line);
                    throw new Exception(keys[0] + "在同一行缺少" + keys[1]);
                }

                int startfunpos = startpos + keys[0].length();
                int endfunpos = StringHelper.findStrIgnoreCase(line, keys[1], startfunpos);
                if (endfunpos == -1) {
                    throw new Exception(keys[0] + "在同一行缺少" + keys[1]);
                }

                List<Object> parameters = funParse.parseFunctionParmeter(line.substring(startfunpos, endfunpos));
                if (parameters != null && parameters.size() > 0) {
                    newFileName = (String)parameters.get(0);
                    isWriting = true;
                }
            } else if (StringHelper.findStrNum(line, keys[2]) > 0 && StringHelper.findStrNum(line, keys[1]) > 0) {
                isWriting = false;
                FileCtrlUtils.writeLines(new File(newFileName), "UTF-8", writerLines, "\r\n");
                this.addOutputMessage("生成文件:" + newFileName);
                writerLines.clear();
            } else if (isWriting) {
                writerLines.add(line);
            }
        }

    }

    public void addOutputMessage(String msg) {
        if (this.outputTextArea != null) {
            if (msg.endsWith("\r\n")) {
                this.outputTextArea.setText(this.outputTextArea.getText() + msg);
            } else {
                this.outputTextArea.setText(this.outputTextArea.getText() + msg + "\r\n");
            }
        }

    }

    public void parse(String fileName, String rootPath) throws Exception {
        String[] keys = new String[]{"@{createTextFile(", ")}", "@{closeFile()}"};
        List lines = FileCtrlUtils.readLines(new File(fileName), "UTF-8");
        int startpos = -1;
        boolean isWriting = false;
        String newFileName = "";
        List<String> writerLines = new ArrayList();
        CreateTextFileParse funParse = new CreateTextFileParse();

        for(int i = 0; i < lines.size(); ++i) {
            String line = (String)lines.get(i);
            if (StringHelper.findStrNum(line, keys[0]) > 0 && StringHelper.findStrNum(line, keys[1]) > 0) {
                startpos = StringHelper.findStrIgnoreCase(line, keys[0], 0);
                if (startpos == -1) {
                    System.out.println("行内容:" + line);
                    throw new Exception(keys[0] + "在同一行缺少" + keys[1]);
                }

                int startfunpos = startpos + keys[0].length();
                int endfunpos = StringHelper.findStrIgnoreCase(line, keys[1], startfunpos);
                if (endfunpos == -1) {
                    throw new Exception(keys[0] + "在同一行缺少" + keys[1]);
                }

                List<Object> parameters = funParse.parseFunctionParmeter(line.substring(startfunpos, endfunpos));
                if (parameters != null && parameters.size() > 0) {
                    newFileName = (String)parameters.get(0);
                    if (!rootPath.endsWith(File.separator) && !newFileName.startsWith(File.separator)) {
                        newFileName = rootPath + File.separator + newFileName;
                    } else {
                        newFileName = rootPath + newFileName;
                    }

                    isWriting = true;
                }
            } else if (StringHelper.findStrNum(line, keys[2]) > 0 && StringHelper.findStrNum(line, keys[1]) > 0) {
                isWriting = false;
                FileCtrlUtils.writeLines(new File(newFileName), "UTF-8", writerLines, "\r\n");
                writerLines.clear();
            } else if (isWriting) {
                writerLines.add(line);
            }
        }

    }
}
