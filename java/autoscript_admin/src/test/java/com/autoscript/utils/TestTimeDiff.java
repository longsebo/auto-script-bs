/**
 * 
 */
package com.autoscript.utils;

import java.time.Duration;
import java.time.Instant;

import org.junit.Test;

import com.dfhc.util.ConvertHelper;

/**
 * @author Administrator
 * 	测试时间差
 */
public class TestTimeDiff {
	@Test
	public void testTimeDiff() throws Exception {
		Instant sendTime = Instant.parse("2020-05-28T02:00:51.536Z");
		Instant now = Instant.now();
		String strEffectiveTime = "15";
		int effectiveTime=ConvertHelper.toInt(strEffectiveTime);
//		logger.info("sendTime:"+sendTime.toString()+",strEffectiveTime:"+strEffectiveTime);
		System.out.println(Duration.between(sendTime,now).getSeconds());
		if(Duration.between(sendTime,now).getSeconds()>effectiveTime*60)
			throw new Exception("验证码已过期!");
		else {
			System.out.println(Duration.between(sendTime,now).getSeconds());
		}
	}
}
