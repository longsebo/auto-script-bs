/**
 * 
 */
package com.autoscript;

import com.autoscript.exception.ASException;
import com.autoscript.service.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * 系统初始化
 */
public class SystemInitialize implements ApplicationListener<ContextRefreshedEvent> {
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		loginSecurityDB(event);
	}
	/**
	 * 	登录securitydb
	 * @param event 
	 */
	private void loginSecurityDB(ContextRefreshedEvent event) {
		ApplicationContext applicationContext = event.getApplicationContext();
		String url= applicationContext.getEnvironment().getProperty("securitydb.url");
		String database= applicationContext.getEnvironment().getProperty("securitydb.database");
		String userName= applicationContext.getEnvironment().getProperty("securitydb.username");
		String password= applicationContext.getEnvironment().getProperty("securitydb.password");
		SecurityDBClientService.getInstance().setUrlPrefix(url);	
		DBSessionService.getSingleInstance().setSession(SecurityDBClientService.getInstance().login(userName, password, database));
		if(StringHelper.isEmpty(DBSessionService.getSingleInstance().getSession()))
			throw new ASException("登录失败!");
		PublicService publicService =(PublicService) applicationContext.getBean(PublicService.class);
		MenuService menuService = (MenuService)applicationContext.getBean(MenuService.class);
		//读取相关参数到缓存
		ParameterCacheService parameterCacheService = ParameterCacheService.getSingleInstance();
		try {
			parameterCacheService.put(ISystemConstant.CODE_DATA_SMS_SEND_INTERVAL, publicService.getParameterValue(ISystemConstant.CODE_TYPE_SMS, ISystemConstant.CODE_DATA_SMS_SEND_INTERVAL));
			parameterCacheService.put(ISystemConstant.CODE_DATA_SMS_CROPID, publicService.getParameterValue(ISystemConstant.CODE_TYPE_SMS, ISystemConstant.CODE_DATA_SMS_CROPID));
			parameterCacheService.put(ISystemConstant.CODE_DATA_SMS_PWD, publicService.getParameterValue(ISystemConstant.CODE_TYPE_SMS, ISystemConstant.CODE_DATA_SMS_PWD));
			parameterCacheService.put(ISystemConstant.CODE_DATA_SMS_GATEWAY_DOMAIN_URL, publicService.getParameterValue(ISystemConstant.CODE_TYPE_SMS, ISystemConstant.CODE_DATA_SMS_GATEWAY_DOMAIN_URL));
			parameterCacheService.put(ISystemConstant.CODE_DATA_SMS_CONTENT_TEMP, publicService.getParameterValue(ISystemConstant.CODE_TYPE_SMS, ISystemConstant.CODE_DATA_SMS_CONTENT_TEMP));
			parameterCacheService.put(ISystemConstant.CODE_DATA_SMS_EFFECTIVE_TIME, publicService.getParameterValue(ISystemConstant.CODE_TYPE_SMS, ISystemConstant.CODE_DATA_SMS_EFFECTIVE_TIME));
			parameterCacheService.put(ISystemConstant.ROOT_DIR,publicService.getParameterValue(ISystemConstant.CODE_TYPE_PATH,ISystemConstant.CODE_DATA_ROOTPATH));
			//装入全部菜单到缓存
			Map<String,Object> searchMap = new HashMap<>();
			parameterCacheService.put(ISystemConstant.ALL_MENU,menuService.search(searchMap));
		}catch(Exception e){
			e.printStackTrace();
			throw new ASException(e);
		}
		//读取预定义freemarker扩展函数到缓存
		try{
			getLoadFreeMarkerExtFun(applicationContext);
		}catch(Exception e){
			e.printStackTrace();
			throw new ASException(e);
		}
	}

	/**
	 * 读取预定义freemarker扩展函数到缓存
	 */
	private void getLoadFreeMarkerExtFun(ApplicationContext applicationContext) throws Exception {
		ConfigService configService ;
		configService = applicationContext.getBean(ConfigService.class);

		Map<String, Object> searchMap = new HashMap<>();

		List<Map> extFunctions = configService.search(searchMap);
		Map<String,Object> cacheMap = new HashMap<>();

		for(Map extFunction:extFunctions){
			Class<?> c = Class.forName((String)extFunction.get("OPERATOR_CLASS"));
			cacheMap.put((String)extFunction.get("FUNCTION_NAME"),c.newInstance());
		}
		ParameterCacheService.getSingleInstance().put(ISystemConstant.ALL_FREEMARKER_EXT_FUNCTION,cacheMap);
	}
}
