/**
 * 
 */
package com.autoscript;

/**
 * @author Administrator
   *  系统常量类
 */
public interface ISystemConstant extends com.dfhc.ISystemConstant {

	/**
	 * 字典类型:激活、锁定状态
	 */
	public static final String DICTIONARY_RM_LOCK_STATUS = "RM_LOCK_STATUS";
	/**
	 * 锁定
	 */
	public static final String DICTIONARY_RM_LOCK_STATUS_0 = "0";

	/**
	 * 激活
	 */
	public static final String DICTIONARY_RM_LOCK_STATUS_1 = "1";

	public static final String DEFAULT_PAGE_SIZE = "15";// 默认页大小
	/**
	 * 页大小key
	 */
	public static final String PAGE_SIZE_KEY="length";
	/**
	 * 开始记录
	 */
	public static final String PAGE_START_KEY="start";
	/**
	 * draw
	 */
	public static final String PAGE_DRAW="draw";
	/**
	 * 排序字段名
	 */
	public static final String SORT = "sort";
	/**
	 * 排序方向
	 */
	public static final String DIR = "dir";
	/**
	 * 总记录数
	 */
	public static final String RECORDS_TOTAL = "recordsTotal";
	/**
	 * 过滤条数
	 */
	public static final String RECORDS_FILTERED = "recordsFiltered";
	/**
	 * 记录数据
	 */
	public static final String DATA = "data";

	// AJAX处理结果
	// 处理结果标志
	/**
	 * 成功
	 */
	public static final String AJAX_RESULT_SUCCESS = "1";
	/**
	 * 失败
	 */
	public static final String AJAX_RESULT_FAIL = "0";
	/**
	 * 处理消息
	 */
	public static final String AJAX_MESSAGE = "message";
	/**
	 * 结果状态key
	 */
	public static final String AJAX_STATUS = "status";
	/**
	 * 结果列表key
	 */
	public static final String AJAX_LIST = "list";
	/**
	 * 结果bean key
	 */
	public static final String AJAX_BEAN = "bean";
	/**
	 * 公钥
	 */
	public static final String AJAX_PUBKEY="publickey";
	/**
	 * 私钥
	 */
	public static final String AJAX_PRIVATEKEY = "privatekey";
	/**
	 * 错误码
	 */
	public static final String AJAX_CODE = "code";
	/**
	 * 尚未登录
	 */
	public static final String AJAX_CODE_NOLOGIN="01";
	/**
	 * token 不正确
	 */
	public static final String AJAX_CODE_TOKEN_ERROR="02";
	/**
	 * 缺少token参数
	 */
	public static final String AJAX_CODE_NOTOKEN="03";
	/**
	 * 处理进度
	 */
	public static final String PROGRESS = "progress";
	//session 信息
	/**
	 * 当前登录用户
	 */
	public static final String CURRENT_USER="currentUser";
	/**
	 * 验证码
	 */
	public static final String CURRENT_VERIFY_CODE="currentVerifyCode";
	/**
	 * 登录TOKEN
	 */
	public static final String CURRENT_APP_TOKEN ="appToken";
	/**
	 * 用户id
	 */
	public static final String CURRENT_USER_ID="userId";
	/**
	 * 用户名
	 */
	public static final String CURRENT_USER_NAME="userName";
	/**
	 * 创建时间
	 */
	public static final String CREATE_TIME ="createTime";
	/**
	 * 需要登录url 编码类型表ID
	 */
	public static final String PARAMETER_LOGIN_URL_ID="2001202103160000001";
	/**
	 * session id
	 */
	public static final String SESSION_ID="sessionId";
	/**
	 * 跳转页面参数
	 */
	public static final String TOPAGE = "page";
	/**
	 * 页号
	 */
	public static final String PAGE_NO = "pageNo";
	/**
	 *
	 * 页大小
	 */
	public static final String PAGE_SIZE = "pageSize";
	/**
	 * 编码表路径类型参数
	 */
	public static final String CODE_TYPE_PATH = "path";
	/**
	 * 根路径参数
	 */
	public static final String CODE_DATA_ROOTPATH = "rootpath";
	/**
	 * LOGO 图标相对目录
	 */
	public static final String RELA_LOGOPATH = "logo";
	/**
	 * 编码表序列类型参数
	 */
	public static final String CODE_TYPE_SEQUENCE = "SEQUENCE";
	/**
	 * 序列的获取序列值参数名
	 */
	public static final String CODE_DATA_URL = "URL";
	/**
	 * 序列域参数值
	 */
	public static final String CODE_DATA_DOMAIN = "DOMAIN";
	/**
	  *  存放二维码图片相对目录
	 */
	public static final String RELA_QRCODE_PATH = "qrcode";
	/**
	 * 存放临时文件的相对目录
	 */
	public static final String RELA_TMP_PATH = "tmp";
	/**
	  *  存放app用户头像图片相对目录
	 */
	public static final String RELA_APPUSER_HEAD_IMG_PATH = "appuser_headimg";
	/**
	 * JPG 后缀
	 */
	public static final String JPGEXT = ".jpg";
	/**
	 * ZIP后缀
	 */
	public static final String ZIPEXT = ".zip";
	/**
	 *  密钥参数类型
	 */
	public static final String CODE_TYPE_SECRETKEY = "secretKey";
	/**
	 * RSA 公钥 参数
	 */
	public static final String CODE_DATA_RSA_PUBLIC_KEY = "RSAPublicKey";
	/**
	 * RSA 私钥 参数
	 */
	public static final String CODE_DATA_RSA_PRIVATE_KEY = "RSAPrivateKey";
	/**
	 * 	短信验证码
	 */
	public static final String SMS_VALIDATE_CODE = "smsValidateCode";
	/**
	 * 	短信验证码发送时间
	 */
	public static final String SMS_VALIDATE_CODE_SEND_TIME="smsValidateCodeSendTime";
	/**
	 * APP tokenId
	 */
	public static final String TOKEN_ID = "tokenId";
	/**
	 * 安卓
	 */
	public static final String OS_ANDROID = "Android";
	/**
	 * 苹果
	 */
	public static final String OS_IOS = "Mac OS";
	/**
	 * 联物 app User_Agent
	 */
	public static final String USER_AGENT_COUPLET_CLIENT = "coupletClient";
	/**
	 * 	编码表安装类型
	 */
	public static final String CODE_TYPE_SETUP = "setup";
	/**
	 * 	 安卓安装包全路径文件名参数
	 */
	public static final String CODE_DATA_ANDROID_SETUP_PACK = "androidSetupPack";
	/**
	 *     用户id
	 */
	public static final String USER_ID = "userId";
	/**
	 * 日志号
	 */
	public static final String LOG_NUMBER="logNumber";
	/**
	 * AES 密钥属性名
	 */
	public static final String AES_KEY = "aesKey";
	/**
	 *		用户昵称
	 */
	public static final String NICK_NAME = "nickName";
	/**
	 * 网站信息类型
	 */
	public static final String WEBSITE = "website";
	/**
	 * 服务器名称
	 */
	public static final String SERVER_NAME = "server";
	/**
	 * 服务器端口
	 */
	public static final String SERVER_PORT = "port";
	/**
	 * 	短信类型
	 */
	public static final String CODE_TYPE_SMS = "sms";
	/**
	 * 	短信账号
	 */
	public static final String CODE_DATA_SMS_CROPID = "CorpID";
	/**
	 * 	短信密码
	 */
	public static final String CODE_DATA_SMS_PWD = "Pwd";
	/**
	 * 	短信网关域名
	 */
	public static final String CODE_DATA_SMS_GATEWAY_DOMAIN_URL = "domainurl";
	/**
	 *	 短信模板
	 */
	public static final String CODE_DATA_SMS_CONTENT_TEMP = "content_temp";
	/**
	 *	 短信有效期(分钟为单位)
	 */
	public static final String CODE_DATA_SMS_EFFECTIVE_TIME = "effective_time";
	/**
	 * 短信验证码发送时间间隔:默认60s
	 */
	public static final String CODE_DATA_SMS_SEND_INTERVAL="sendInterval";
	/**
	 * 全部菜单
	 */
	public static final String ALL_MENU="allMenu";
	/**
	 * 全部FreeMarker 扩展函数
	 */
	public static final String ALL_FREEMARKER_EXT_FUNCTION="allFreeMarkerExtFunction";
	/**
	 * pdm 模型
	 */
	public static final String PDM_MODEL ="pdmModel";
	/**
	 * 模板目录 相对于工作目录
	 */
	public static final String TEMPLDATE_PATH = "template";
	/**
	 * 中间结果目录 相对于工作目录
	 */
	public static final String INTERMEDIATE_PATH = "intermediate";
	/**
	 * 结果目录 相对于工作目录
	 */
	public static final String RESULT_PATH = "result";
	/**
	 * xml源数据文件名
	 */
	public static final String SOURCE_XML_FILE = "source.xml";
	/**
	 * 模板文件名后缀
	 */
	public static final String TEMPLATE_SUFFIX = ".ftl";
	/**
	 * 中间结果文件后缀名
	 */
	public static final String INTERMEDIATE_SUFFIX = ".imd";
	/**
	 * 导出导入工程的工程文件名
	 */
	public static final String PROJECT_FILE="as_project.json";
	/**
	 * 导出导入工程的工程参数文件名
	 */
	public static final String PROJECT_PARAMETER_FILE="as_project_parameter.json";
	/**
	 * 导出导入工程的工程模版文件名
	 */
	public static final String TEMPLATE_FILE="as_template.json";

	/**
	 * 根路径
	 */
	public static final String ROOT_DIR="rootDir";
	/**
	 * 相对工作目录
	 */
	public static final String RELA_WORK_DIR="work";
	/**
	 * xml 根节点
	 */
	public static final String ROOT_NODE="rootnode";
	/**
	 * 前台穿梭组件key
	 */
	public static final String KEY="key";
	/**
	 * 前台穿梭组件label
	 */
	public static final String LABEL="label";
	//控件属性类型
	/**
	 * 单行输入框
	 */
	public static final String CTRL_PROPERTY_TYPE_INPUT="input";
	/**
	 * 多行输入框
	 */
	public static final String CTRL_PROPERTY_TYPE_TEXTAREA = "textarea";
	/**
	 * 开关
	 */
	public static final String CTRL_PROPERTY_TYPE_SWITCH = "switch";
    /**
     * 下拉框
     */
    public static final String  CTRL_PROPERTY_TYPE_SELECT = "select";
	/**
	 * 树
	 */
	public static final String CTRL_PROPERTY_TYPE_TREE="treeSelect";
}
