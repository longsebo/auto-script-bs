package com.autoscript;

import com.autoscript.utils.ContextUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.autoscript.filter.PrivilegeFilter;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * 
 * @类名称：Application
 * @类描述：Springboot启动类 @version：
 */
// 这是一个配置Spring的配置类
@Configuration
// @SpringBootApplication：Spring Boot项目的核心注解，主要目的是开启自动配置，自动扫描该类同级包以及子包。
@SpringBootApplication
@EnableAsync
@EnableScheduling  //开启定时
@EnableWebSocket
public class Application {

	public static void main(String[] args) {
		// 启动spring boot应用
		SpringApplication sa = new SpringApplication(Application.class);
		// 禁用devTools热部署 SpringApplication.setRegisterShutdownHook（false）
		System.setProperty("spring.devtools.restart.enabled", "false");
		// 禁用命令行更改application.properties属性
		sa.setAddCommandLineProperties(false);
		// 系统初始化
		sa.addListeners(new SystemInitialize());
		ContextUtils.setApplicationContext(sa.run(args));
	}
	@Bean
	public FilterRegistrationBean filterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean(new PrivilegeFilter());
		registration.addUrlPatterns("/*");
		return registration;

	}
	/**
	 * 如果直接使用springboot的内置容器，而不是使用独立的servlet容器，就要注入ServerEndpointExporter，外部容器则不需要。
	 */
	@Bean
	public ServerEndpointExporter serverEndpointExporter() {
		return new ServerEndpointExporter();
	}
	/**
	 * 与websockeet一起使用时，必须手工创建TaskScheduler
	 */
	@Bean
	public TaskScheduler taskScheduler(){
		ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();

		taskScheduler.setPoolSize(10);
		taskScheduler.initialize();

		return taskScheduler;
	}
}
