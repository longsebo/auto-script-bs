package com.autoscript.vo;

/**
 * 菜单定义Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-24
 */
public class MenuVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 菜单名称
     */
    private  String  menuName;
    /**
     * 菜单编码
     */
    private  String  menuCode;
    /**
     * 菜单链接
     */
    private  String  menuLink;
    /**
     * 顺序
     */
    private  String  menuOrder;
    /**
     * 上级编码
     */
    private  String  parentCode;
    /**
     * 是否要登录
     */
    private  String  isNeedLogin;
    /**
     * 是否管理员菜单
     */
    private  String  isAdmin;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取菜单名称 
     * @return 菜单名称
     */
    public  String  getMenuName(){
        return menuName;
    }
    /**
     * 设置菜单名称
     * @param menuName 菜单名称
     */
    public  void  setMenuName(String menuName){
        this.menuName = menuName;
    }
    /**
     * 获取菜单编码 
     * @return 菜单编码
     */
    public  String  getMenuCode(){
        return menuCode;
    }
    /**
     * 设置菜单编码
     * @param menuCode 菜单编码
     */
    public  void  setMenuCode(String menuCode){
        this.menuCode = menuCode;
    }
    /**
     * 获取菜单链接 
     * @return 菜单链接
     */
    public  String  getMenuLink(){
        return menuLink;
    }
    /**
     * 设置菜单链接
     * @param menuLink 菜单链接
     */
    public  void  setMenuLink(String menuLink){
        this.menuLink = menuLink;
    }
    /**
     * 获取顺序 
     * @return 顺序
     */
    public  String  getMenuOrder(){
        return menuOrder;
    }
    /**
     * 设置顺序
     * @param menuOrder 顺序
     */
    public  void  setMenuOrder(String menuOrder){
        this.menuOrder = menuOrder;
    }
    /**
     * 获取上级编码 
     * @return 上级编码
     */
    public  String  getParentCode(){
        return parentCode;
    }
    /**
     * 设置上级编码
     * @param parentCode 上级编码
     */
    public  void  setParentCode(String parentCode){
        this.parentCode = parentCode;
    }
    /**
     * 获取是否要登录 
     * @return 是否要登录
     */
    public  String  getIsNeedLogin(){
        return isNeedLogin;
    }
    /**
     * 设置是否要登录
     * @param isNeedLogin 是否要登录
     */
    public  void  setIsNeedLogin(String isNeedLogin){
        this.isNeedLogin = isNeedLogin;
    }
    /**
     * 获取是否管理员菜单 
     * @return 是否管理员菜单
     */
    public  String  getIsAdmin(){
        return isAdmin;
    }
    /**
     * 设置是否管理员菜单
     * @param isAdmin 是否管理员菜单
     */
    public  void  setIsAdmin(String isAdmin){
        this.isAdmin = isAdmin;
    }
}
