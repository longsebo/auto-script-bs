package com.autoscript.vo;

/**
 * 配置表Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-02
 */
public class ConfigVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 函数名称
     */
    private  String  functionName;
    /**
     * 操作类名
     */
    private  String  operatorClass;
    /**
     * 函数描述
     */
    private  String  functionDesc;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取函数名称 
     * @return 函数名称
     */
    public  String  getFunctionName(){
        return functionName;
    }
    /**
     * 设置函数名称
     * @param functionName 函数名称
     */
    public  void  setFunctionName(String functionName){
        this.functionName = functionName;
    }
    /**
     * 获取操作类名 
     * @return 操作类名
     */
    public  String  getOperatorClass(){
        return operatorClass;
    }
    /**
     * 设置操作类名
     * @param operatorClass 操作类名
     */
    public  void  setOperatorClass(String operatorClass){
        this.operatorClass = operatorClass;
    }
    /**
     * 获取函数描述 
     * @return 函数描述
     */
    public  String  getFunctionDesc(){
        return functionDesc;
    }
    /**
     * 设置函数描述
     * @param functionDesc 函数描述
     */
    public  void  setFunctionDesc(String functionDesc){
        this.functionDesc = functionDesc;
    }
}
