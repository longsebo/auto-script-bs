package com.autoscript.vo;

/**
 * 用户菜单表Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-02
 */
public class UserMenuVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 菜单ID
     */
    private  String  menuId;
    /**
     * 菜单编码
     */
    private  String  menuCode;
    /**
     * 用户ID
     */
    private  String  userId;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取菜单ID 
     * @return 菜单ID
     */
    public  String  getMenuId(){
        return menuId;
    }
    /**
     * 设置菜单ID
     * @param menuId 菜单ID
     */
    public  void  setMenuId(String menuId){
        this.menuId = menuId;
    }
    /**
     * 获取菜单编码 
     * @return 菜单编码
     */
    public  String  getMenuCode(){
        return menuCode;
    }
    /**
     * 设置菜单编码
     * @param menuCode 菜单编码
     */
    public  void  setMenuCode(String menuCode){
        this.menuCode = menuCode;
    }
    /**
     * 获取用户ID 
     * @return 用户ID
     */
    public  String  getUserId(){
        return userId;
    }
    /**
     * 设置用户ID
     * @param userId 用户ID
     */
    public  void  setUserId(String userId){
        this.userId = userId;
    }
}
