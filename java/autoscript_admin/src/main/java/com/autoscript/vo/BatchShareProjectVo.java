package com.autoscript.vo;

import java.util.List;

/**
 * 批量分享vo
 */
public class BatchShareProjectVo {
    private String projectId;
    private List<String> userIds;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }
}
