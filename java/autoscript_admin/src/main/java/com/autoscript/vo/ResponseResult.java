/**
 * 
 */
package com.autoscript.vo;

/**
 * @author Administrator
 *
 */
public class ResponseResult {

	private int code;
	private Exception ex;
	private String message;

	public ResponseResult(int code, Exception ex) {
		this.code = code;
		this.ex = ex;
	}

	public ResponseResult(int code, String message) {
		this.code = code;
		this.message = message;
	}

}
