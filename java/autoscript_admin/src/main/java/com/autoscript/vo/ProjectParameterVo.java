package com.autoscript.vo;

/**
 * 工程参数Vo
 * @author lsb
 * @version 1.0.0
 * @date 2022-03-03
 */
public class ProjectParameterVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 工程ID
     */
    private  String  projectId;
    /**
     * 参数类型
     */
    private  String  propertyType;
    /**
     * 参数标题
     */
    private  String  propertyTitle;
    /**
     * 宽度
     */
    private  String  width;
    /**
     * 验证规则
     */
    private  String  rules;
    /**
     * 选择内容
     */
    private  String  options;
    /**
     * 参数名
     */
    private  String  bindModel;
    /**
     * 占位符
     */
    private  String  placeholder;
    /**
     * 最大长度
     */
    private  String  maxLength;
    /**
     * cs风格
     */
    private  String  style;
    /**
     * 初始化值
     */
    private  String  initVal;
    /**
     * 最大值
     */
    private  String  maxVal;
    /**
     * 最小值
     */
    private  String  minVal;
    /**
     * 小数位
     */
    private  String  precision;
    /**
     * 步长
     */
    private  String  step;
    /**
     * 最大行数
     */
    private  String  maxRows;
    /**
     * 最小行数
     */
    private  String  minRows;
    /**
     * 初始化行数
     */
    private  String  initRowNum;
    /**
     * 初始化列数
     */
    private  String  initColumnNum;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取工程ID 
     * @return 工程ID
     */
    public  String  getProjectId(){
        return projectId;
    }
    /**
     * 设置工程ID
     * @param projectId 工程ID
     */
    public  void  setProjectId(String projectId){
        this.projectId = projectId;
    }
    /**
     * 获取参数类型 
     * @return 参数类型
     */
    public  String  getPropertyType(){
        return propertyType;
    }
    /**
     * 设置参数类型
     * @param propertyType 参数类型
     */
    public  void  setPropertyType(String propertyType){
        this.propertyType = propertyType;
    }
    /**
     * 获取参数标题 
     * @return 参数标题
     */
    public  String  getPropertyTitle(){
        return propertyTitle;
    }
    /**
     * 设置参数标题
     * @param propertyTitle 参数标题
     */
    public  void  setPropertyTitle(String propertyTitle){
        this.propertyTitle = propertyTitle;
    }
    /**
     * 获取宽度 
     * @return 宽度
     */
    public  String  getWidth(){
        return width;
    }
    /**
     * 设置宽度
     * @param width 宽度
     */
    public  void  setWidth(String width){
        this.width = width;
    }
    /**
     * 获取验证规则 
     * @return 验证规则
     */
    public  String  getRules(){
        return rules;
    }
    /**
     * 设置验证规则
     * @param rules 验证规则
     */
    public  void  setRules(String rules){
        this.rules = rules;
    }
    /**
     * 获取选择内容 
     * @return 选择内容
     */
    public  String  getOptions(){
        return options;
    }
    /**
     * 设置选择内容
     * @param options 选择内容
     */
    public  void  setOptions(String options){
        this.options = options;
    }
    /**
     * 获取参数名 
     * @return 参数名
     */
    public  String  getBindModel(){
        return bindModel;
    }
    /**
     * 设置参数名
     * @param bindModel 参数名
     */
    public  void  setBindModel(String bindModel){
        this.bindModel = bindModel;
    }
    /**
     * 获取占位符 
     * @return 占位符
     */
    public  String  getPlaceholder(){
        return placeholder;
    }
    /**
     * 设置占位符
     * @param placeholder 占位符
     */
    public  void  setPlaceholder(String placeholder){
        this.placeholder = placeholder;
    }
    /**
     * 获取最大长度 
     * @return 最大长度
     */
    public  String  getMaxLength(){
        return maxLength;
    }
    /**
     * 设置最大长度
     * @param maxLength 最大长度
     */
    public  void  setMaxLength(String maxLength){
        this.maxLength = maxLength;
    }
    /**
     * 获取cs风格 
     * @return cs风格
     */
    public  String  getStyle(){
        return style;
    }
    /**
     * 设置cs风格
     * @param style cs风格
     */
    public  void  setStyle(String style){
        this.style = style;
    }
    /**
     * 获取初始化值 
     * @return 初始化值
     */
    public  String  getInitVal(){
        return initVal;
    }
    /**
     * 设置初始化值
     * @param initVal 初始化值
     */
    public  void  setInitVal(String initVal){
        this.initVal = initVal;
    }
    /**
     * 获取最大值 
     * @return 最大值
     */
    public  String  getMaxVal(){
        return maxVal;
    }
    /**
     * 设置最大值
     * @param maxVal 最大值
     */
    public  void  setMaxVal(String maxVal){
        this.maxVal = maxVal;
    }
    /**
     * 获取最小值 
     * @return 最小值
     */
    public  String  getMinVal(){
        return minVal;
    }
    /**
     * 设置最小值
     * @param minVal 最小值
     */
    public  void  setMinVal(String minVal){
        this.minVal = minVal;
    }
    /**
     * 获取小数位 
     * @return 小数位
     */
    public  String  getPrecision(){
        return precision;
    }
    /**
     * 设置小数位
     * @param precision 小数位
     */
    public  void  setPrecision(String precision){
        this.precision = precision;
    }
    /**
     * 获取步长 
     * @return 步长
     */
    public  String  getStep(){
        return step;
    }
    /**
     * 设置步长
     * @param step 步长
     */
    public  void  setStep(String step){
        this.step = step;
    }
    /**
     * 获取最大行数 
     * @return 最大行数
     */
    public  String  getMaxRows(){
        return maxRows;
    }
    /**
     * 设置最大行数
     * @param maxRows 最大行数
     */
    public  void  setMaxRows(String maxRows){
        this.maxRows = maxRows;
    }
    /**
     * 获取最小行数 
     * @return 最小行数
     */
    public  String  getMinRows(){
        return minRows;
    }
    /**
     * 设置最小行数
     * @param minRows 最小行数
     */
    public  void  setMinRows(String minRows){
        this.minRows = minRows;
    }
    /**
     * 获取初始化行数 
     * @return 初始化行数
     */
    public  String  getInitRowNum(){
        return initRowNum;
    }
    /**
     * 设置初始化行数
     * @param initRowNum 初始化行数
     */
    public  void  setInitRowNum(String initRowNum){
        this.initRowNum = initRowNum;
    }
    /**
     * 获取初始化列数 
     * @return 初始化列数
     */
    public  String  getInitColumnNum(){
        return initColumnNum;
    }
    /**
     * 设置初始化列数
     * @param initColumnNum 初始化列数
     */
    public  void  setInitColumnNum(String initColumnNum){
        this.initColumnNum = initColumnNum;
    }
}
