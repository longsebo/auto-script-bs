package com.autoscript.vo;

/**
 * 运行日志表Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-06-28
 */
public class RunlogVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 工程ID
     */
    private  String  projectId;
    /**
     * 日志编号
     */
    private  String  logCode;
    /**
     * 工程参数
     */
    private  String  projectParameter;
    /**
     * 源数据
     */
    private  String  sourceData;
    /**
     * 运行开始时间
     */
    private  String  startTime;
    /**
     * 运行结束时间
     */
    private  String  endTime;
    /**
     * 结果文件
     */
    private  String  resultFile;
    /**
     * 运行用户ID
     */
    private  String  runUserId;
    /**
     * 成功失败标志
     */
    private  String  successFailFlag;
    /**
     * 错误消息
     */
    private  String  errorMsg;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取工程ID 
     * @return 工程ID
     */
    public  String  getProjectId(){
        return projectId;
    }
    /**
     * 设置工程ID
     * @param projectId 工程ID
     */
    public  void  setProjectId(String projectId){
        this.projectId = projectId;
    }
    /**
     * 获取日志编号 
     * @return 日志编号
     */
    public  String  getLogCode(){
        return logCode;
    }
    /**
     * 设置日志编号
     * @param logCode 日志编号
     */
    public  void  setLogCode(String logCode){
        this.logCode = logCode;
    }
    /**
     * 获取工程参数 
     * @return 工程参数
     */
    public  String  getProjectParameter(){
        return projectParameter;
    }
    /**
     * 设置工程参数
     * @param projectParameter 工程参数
     */
    public  void  setProjectParameter(String projectParameter){
        this.projectParameter = projectParameter;
    }
    /**
     * 获取源数据 
     * @return 源数据
     */
    public  String  getSourceData(){
        return sourceData;
    }
    /**
     * 设置源数据
     * @param sourceData 源数据
     */
    public  void  setSourceData(String sourceData){
        this.sourceData = sourceData;
    }
    /**
     * 获取运行开始时间 
     * @return 运行开始时间
     */
    public  String  getStartTime(){
        return startTime;
    }
    /**
     * 设置运行开始时间
     * @param startTime 运行开始时间
     */
    public  void  setStartTime(String startTime){
        this.startTime = startTime;
    }
    /**
     * 获取运行结束时间 
     * @return 运行结束时间
     */
    public  String  getEndTime(){
        return endTime;
    }
    /**
     * 设置运行结束时间
     * @param endTime 运行结束时间
     */
    public  void  setEndTime(String endTime){
        this.endTime = endTime;
    }
    /**
     * 获取结果文件 
     * @return 结果文件
     */
    public  String  getResultFile(){
        return resultFile;
    }
    /**
     * 设置结果文件
     * @param resultFile 结果文件
     */
    public  void  setResultFile(String resultFile){
        this.resultFile = resultFile;
    }
    /**
     * 获取运行用户ID 
     * @return 运行用户ID
     */
    public  String  getRunUserId(){
        return runUserId;
    }
    /**
     * 设置运行用户ID
     * @param runUserId 运行用户ID
     */
    public  void  setRunUserId(String runUserId){
        this.runUserId = runUserId;
    }
    /**
     * 获取成功失败标志 
     * @return 成功失败标志
     */
    public  String  getSuccessFailFlag(){
        return successFailFlag;
    }
    /**
     * 设置成功失败标志
     * @param successFailFlag 成功失败标志
     */
    public  void  setSuccessFailFlag(String successFailFlag){
        this.successFailFlag = successFailFlag;
    }
    /**
     * 获取错误消息 
     * @return 错误消息
     */
    public  String  getErrorMsg(){
        return errorMsg;
    }
    /**
     * 设置错误消息
     * @param errorMsg 错误消息
     */
    public  void  setErrorMsg(String errorMsg){
        this.errorMsg = errorMsg;
    }
}
