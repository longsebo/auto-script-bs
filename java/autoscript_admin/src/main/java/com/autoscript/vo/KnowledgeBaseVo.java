package com.autoscript.vo;

/**
 * 知识库Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-24
 */
public class KnowledgeBaseVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 类型
     */
    private  String  kbType;
    /**
     * 关键字
     */
    private  String  kbKeyword;
    /**
     * 语法
     */
    private  String  kbGrammar;
    /**
     * 描述
     */
    private  String  desc;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取类型 
     * @return 类型
     */
    public  String  getKbType(){
        return kbType;
    }
    /**
     * 设置类型
     * @param kbType 类型
     */
    public  void  setKbType(String kbType){
        this.kbType = kbType;
    }
    /**
     * 获取关键字 
     * @return 关键字
     */
    public  String  getKbKeyword(){
        return kbKeyword;
    }
    /**
     * 设置关键字
     * @param kbKeyword 关键字
     */
    public  void  setKbKeyword(String kbKeyword){
        this.kbKeyword = kbKeyword;
    }
    /**
     * 获取语法 
     * @return 语法
     */
    public  String  getKbGrammar(){
        return kbGrammar;
    }
    /**
     * 设置语法
     * @param kbGrammar 语法
     */
    public  void  setKbGrammar(String kbGrammar){
        this.kbGrammar = kbGrammar;
    }
    /**
     * 获取描述 
     * @return 描述
     */
    public  String  getDesc(){
        return desc;
    }
    /**
     * 设置描述
     * @param desc 描述
     */
    public  void  setDesc(String desc){
        this.desc = desc;
    }
}
