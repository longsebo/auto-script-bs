package com.autoscript.vo;

import com.alibaba.fastjson.JSONArray;

/**
 * 专门为vue界面定制返回值对象
 */
public class Record {
    /**
     * 控件类型
     */
    private String type;
    /**
     * 控件属性选项
     */
    private Options options;
    /**
     * 控件模型
     */
    private String model;
    /**
     * 验证规则
     */
    private JSONArray rules;
    /**
     * 标题
     */
    private String label;
    /**
     * 帮助
     */
    private String help;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Options getOptions() {
        return options;
    }

    public void setOptions(Options options) {
        this.options = options;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public JSONArray getRules() {
        return rules;
    }

    public void setRules(JSONArray rules) {
        this.rules = rules;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }
}
