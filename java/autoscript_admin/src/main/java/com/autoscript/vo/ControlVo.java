package com.autoscript.vo;

/**
 * 控件Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-05-29
 */
public class ControlVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 控件类型
     */
    private  String  controlType;
    /**
     * 备注
     */
    private  String  remark;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取控件类型 
     * @return 控件类型
     */
    public  String  getControlType(){
        return controlType;
    }
    /**
     * 设置控件类型
     * @param controlType 控件类型
     */
    public  void  setControlType(String controlType){
        this.controlType = controlType;
    }
    /**
     * 获取备注 
     * @return 备注
     */
    public  String  getRemark(){
        return remark;
    }
    /**
     * 设置备注
     * @param remark 备注
     */
    public  void  setRemark(String remark){
        this.remark = remark;
    }
}
