package com.autoscript.vo;

import com.alibaba.fastjson.JSONArray;

/**
 * 控件属性选项
 */
public class Options {
    /**
     * 宽度
     */
    private String width;
    /**
     * 禁用启用
     */
    private boolean disabled;
    /**
     * 占位符
     */
    private String placeholder;
    /**
     * 类型?
     */
    private String type;
    /**
     * 允许清除
     */
    private boolean clearable;
    /**
     * 最大长度
     */
    private int maxLength;
    /**
     * 前缀
     */
    private String prefix;
    /**
     * 后缀
     */
    private String suffix;
    /**
     * 缺省值 初始化值
     */
    private String defaultValue;
    /**
     * 最小行
     */
    private String minRows;
    /**
     * 最大行
     */
    private String maxRows;
    /**
     * 是否有范围,用于控件:日期
     */
    private boolean range;
    /**
     * 范围缺省值
     */
    private String rangeDefaultValue;
    /**
     * 最小值，用于控件数字输入框
     */
    private String min;
    /**
     * 最大值，用于控件数字输入框
     */
    private String max;
    /**
     * 步长
     */
    private String step;
    /**
     * 精度
     */
    private String precision;
    /**
     * 动态标志
     */
    private boolean dynamic;
    /**
     * 控件模型:仅用于下拉框
     */
    private String model;
    /**
     * 可选数据
     */
    private JSONArray options;
    /**
     * 动态数据key
     */
    private String dynamicKey;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isClearable() {
        return clearable;
    }

    public void setClearable(boolean clearable) {
        this.clearable = clearable;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getMinRows() {
        return minRows;
    }

    public void setMinRows(String minRows) {
        this.minRows = minRows;
    }

    public String getMaxRows() {
        return maxRows;
    }

    public void setMaxRows(String maxRows) {
        this.maxRows = maxRows;
    }

    public boolean isRange() {
        return range;
    }

    public void setRange(boolean range) {
        this.range = range;
    }

    public String getRangeDefaultValue() {
        return rangeDefaultValue;
    }

    public void setRangeDefaultValue(String rangeDefaultValue) {
        this.rangeDefaultValue = rangeDefaultValue;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public boolean isDynamic() {
        return dynamic;
    }

    public void setDynamic(boolean dynamic) {
        this.dynamic = dynamic;
    }



    public String getDynamicKey() {
        return dynamicKey;
    }

    public void setDynamicKey(String dynamicKey) {
        this.dynamicKey = dynamicKey;
    }

    public JSONArray getOptions() {
        return options;
    }

    public void setOptions(JSONArray options) {
        this.options = options;
    }
}
