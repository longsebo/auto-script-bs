package com.autoscript.vo;

/**
 * 分享工程Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-06-25
 */
public class ShareProjectVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 工程ID
     */
    private  String  projectId;
    /**
     * 用户ID
     */
    private  String  userId;
    /**
     * 分享日期时间
     */
    private  String  shareTime;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取工程ID 
     * @return 工程ID
     */
    public  String  getProjectId(){
        return projectId;
    }
    /**
     * 设置工程ID
     * @param projectId 工程ID
     */
    public  void  setProjectId(String projectId){
        this.projectId = projectId;
    }
    /**
     * 获取用户ID 
     * @return 用户ID
     */
    public  String  getUserId(){
        return userId;
    }
    /**
     * 设置用户ID
     * @param userId 用户ID
     */
    public  void  setUserId(String userId){
        this.userId = userId;
    }
    /**
     * 获取分享日期时间 
     * @return 分享日期时间
     */
    public  String  getShareTime(){
        return shareTime;
    }
    /**
     * 设置分享日期时间
     * @param shareTime 分享日期时间
     */
    public  void  setShareTime(String shareTime){
        this.shareTime = shareTime;
    }
}
