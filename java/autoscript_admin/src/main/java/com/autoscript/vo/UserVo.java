package com.autoscript.vo;

/**
 * 用户表Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-02
 */
public class UserVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 用户名
     */
    private  String  userName;
    /**
     * 密码
     */
    private  String  password;
    /**
     * 手机号
     */
    private  String  cellPhoneNumber;
    /**
     * 邮箱
     */
    private  String  email;
    /**
     * 创建日期
     */
    private  String  createDate;
    /**
     * 是否启用
     */
    private  String  isEnable;
    /**
     * 验证码 界面扩展
     */
    private String verificationCode;
    /**
     * 是否为管理员
     */
    private  String  isAdmin;
    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取用户名 
     * @return 用户名
     */
    public  String  getUserName(){
        return userName;
    }
    /**
     * 设置用户名
     * @param userName 用户名
     */
    public  void  setUserName(String userName){
        this.userName = userName;
    }
    /**
     * 获取密码 
     * @return 密码
     */
    public  String  getPassword(){
        return password;
    }
    /**
     * 设置密码
     * @param password 密码
     */
    public  void  setPassword(String password){
        this.password = password;
    }
    /**
     * 获取手机号 
     * @return 手机号
     */
    public  String  getCellPhoneNumber(){
        return cellPhoneNumber;
    }
    /**
     * 设置手机号
     * @param cellPhoneNumber 手机号
     */
    public  void  setCellPhoneNumber(String cellPhoneNumber){
        this.cellPhoneNumber = cellPhoneNumber;
    }
    /**
     * 获取邮箱 
     * @return 邮箱
     */
    public  String  getEmail(){
        return email;
    }
    /**
     * 设置邮箱
     * @param email 邮箱
     */
    public  void  setEmail(String email){
        this.email = email;
    }
    /**
     * 获取创建日期 
     * @return 创建日期
     */
    public  String  getCreateDate(){
        return createDate;
    }
    /**
     * 设置创建日期
     * @param createDate 创建日期
     */
    public  void  setCreateDate(String createDate){
        this.createDate = createDate;
    }
    /**
     * 获取是否启用 
     * @return 是否启用
     */
    public  String  getIsEnable(){
        return isEnable;
    }
    /**
     * 设置是否启用
     * @param isEnable 是否启用
     */
    public  void  setIsEnable(String isEnable){
        this.isEnable = isEnable;
    }
    /**
     * 获取是否为管理员
     * @return 是否为管理员
     */
    public  String  getIsAdmin(){
        return isAdmin;
    }
    /**
     * 设置是否为管理员
     * @param isAdmin 是否为管理员
     */
    public  void  setIsAdmin(String isAdmin){
        this.isAdmin = isAdmin;
    }

}
