package com.autoscript.vo;

/**
 * 模板表Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-02
 */
public class TemplateVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 工程ID
     */
    private  String  projectId;
    /**
     * 模板名称
     */
    private  String  templateName;
    /**
     * 备注
     */
    private  String  remark;
    /**
     * 模板内容
     */
    private  String  templateContent;
    /**
     * 是否启用
     */
    private  String  isEnable;
    /**
     * 创建日期
     */
    private  String  createDate;
    /**
     * 更新日期
     */
    private  String  updateDate;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取工程ID 
     * @return 工程ID
     */
    public  String  getProjectId(){
        return projectId;
    }
    /**
     * 设置工程ID
     * @param projectId 工程ID
     */
    public  void  setProjectId(String projectId){
        this.projectId = projectId;
    }
    /**
     * 获取模板名称 
     * @return 模板名称
     */
    public  String  getTemplateName(){
        return templateName;
    }
    /**
     * 设置模板名称
     * @param templateName 模板名称
     */
    public  void  setTemplateName(String templateName){
        this.templateName = templateName;
    }
    /**
     * 获取备注 
     * @return 备注
     */
    public  String  getRemark(){
        return remark;
    }
    /**
     * 设置备注
     * @param remark 备注
     */
    public  void  setRemark(String remark){
        this.remark = remark;
    }
    /**
     * 获取模板内容 
     * @return 模板内容
     */
    public  String  getTemplateContent(){
        return templateContent;
    }
    /**
     * 设置模板内容
     * @param templateContent 模板内容
     */
    public  void  setTemplateContent(String templateContent){
        this.templateContent = templateContent;
    }
    /**
     * 获取是否启用 
     * @return 是否启用
     */
    public  String  getIsEnable(){
        return isEnable;
    }
    /**
     * 设置是否启用
     * @param isEnable 是否启用
     */
    public  void  setIsEnable(String isEnable){
        this.isEnable = isEnable;
    }
    /**
     * 获取创建日期 
     * @return 创建日期
     */
    public  String  getCreateDate(){
        return createDate;
    }
    /**
     * 设置创建日期
     * @param createDate 创建日期
     */
    public  void  setCreateDate(String createDate){
        this.createDate = createDate;
    }
    /**
     * 获取更新日期 
     * @return 更新日期
     */
    public  String  getUpdateDate(){
        return updateDate;
    }
    /**
     * 设置更新日期
     * @param updateDate 更新日期
     */
    public  void  setUpdateDate(String updateDate){
        this.updateDate = updateDate;
    }
}
