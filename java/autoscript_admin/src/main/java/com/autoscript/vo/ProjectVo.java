package com.autoscript.vo;

/**
 * 工程表Vo
 * @author lsb
 * @version 1.0.0
 * @date 2021-06-24
 */
public class ProjectVo  {   
    /**
     * ID
     */
    private  String  id;
    /**
     * 工程名称
     */
    private  String  projectName;
    /**
     * 备注
     */
    private  String  remark;
    /**
     * 创建日期
     */
    private  String  createDate;
    /**
     * 创建用户ID
     */
    private  String  createUserId;
    /**
     * 字符集
     */
    private  String  encode;
    /**
     * 是否选数据源
     */
    private  String  isDatasource;
    /**
     * 自定义运行页面url
     */
    private  String  customRunUrl;
    /**
     * 获取ID 
     * @return ID
     */
    public  String  getId(){
        return id;
    }
    /**
     * 设置ID
     * @param id ID
     */
    public  void  setId(String id){
        this.id = id;
    }
    /**
     * 获取工程名称 
     * @return 工程名称
     */
    public  String  getProjectName(){
        return projectName;
    }
    /**
     * 设置工程名称
     * @param projectName 工程名称
     */
    public  void  setProjectName(String projectName){
        this.projectName = projectName;
    }
    /**
     * 获取备注 
     * @return 备注
     */
    public  String  getRemark(){
        return remark;
    }
    /**
     * 设置备注
     * @param remark 备注
     */
    public  void  setRemark(String remark){
        this.remark = remark;
    }
    /**
     * 获取创建日期 
     * @return 创建日期
     */
    public  String  getCreateDate(){
        return createDate;
    }
    /**
     * 设置创建日期
     * @param createDate 创建日期
     */
    public  void  setCreateDate(String createDate){
        this.createDate = createDate;
    }
    /**
     * 获取创建用户ID 
     * @return 创建用户ID
     */
    public  String  getCreateUserId(){
        return createUserId;
    }
    /**
     * 设置创建用户ID
     * @param createUserId 创建用户ID
     */
    public  void  setCreateUserId(String createUserId){
        this.createUserId = createUserId;
    }
    /**
     * 获取字符集 
     * @return 字符集
     */
    public  String  getEncode(){
        return encode;
    }
    /**
     * 设置字符集
     * @param encode 字符集
     */
    public  void  setEncode(String encode){
        this.encode = encode;
    }
    /**
     * 获取是否选数据源 
     * @return 是否选数据源
     */
    public  String  getIsDatasource(){
        return isDatasource;
    }
    /**
     * 设置是否选数据源
     * @param isDatasource 是否选数据源
     */
    public  void  setIsDatasource(String isDatasource){
        this.isDatasource = isDatasource;
    }
    /**
     * 获取自定义运行页面url 
     * @return 自定义运行页面url
     */
    public  String  getCustomRunUrl(){
        return customRunUrl;
    }
    /**
     * 设置自定义运行页面url
     * @param customRunUrl 自定义运行页面url
     */
    public  void  setCustomRunUrl(String customRunUrl){
        this.customRunUrl = customRunUrl;
    }
}
