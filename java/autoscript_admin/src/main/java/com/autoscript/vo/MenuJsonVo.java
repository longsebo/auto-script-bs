package com.autoscript.vo;

import java.util.List;

/**
 * 为了前台vue 展示方便，专门做一个vo
 */
public class MenuJsonVo {
    /**
     * 菜单名称
     */
    private  String  menuName;
    /**
     * 菜单编码
     */
    private  String  menuCode;
    /**
     * 菜单链接
     */
    private  String  menuLink;
    /**
     * 顺序
     */
    private  String  menuOrder;
    /**
     * 上级编码
     */
    private  String  parentCode;
    /**
     * 下级菜单列表
     */
    private List<MenuJsonVo> children;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public String getMenuLink() {
        return menuLink;
    }

    public void setMenuLink(String menuLink) {
        this.menuLink = menuLink;
    }

    public String getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(String menuOrder) {
        this.menuOrder = menuOrder;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public List<MenuJsonVo> getChildren() {
        return children;
    }

    public void setChildren(List<MenuJsonVo> children) {
        this.children = children;
    }
}
