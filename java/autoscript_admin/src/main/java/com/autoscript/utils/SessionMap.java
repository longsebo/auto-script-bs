package com.autoscript.utils;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SessionMap {
    private Map<String,Map<String,Object>> sessionMap;
    public SessionMap() {
        sessionMap = new HashMap<>();
    }

    /**
     * 根据sessionId 放入map
     * @param sessionId
     * @param map
     */
    public void put(String sessionId,Map<String,Object> map){
        synchronized (sessionMap){
            sessionMap.put(sessionId,map);
        }
    }

    /**
     * 根据sessionId获取
     * @param sessionId
     * @return
     */
    public Map<String,Object> get(String sessionId){
        synchronized (sessionMap){
            return sessionMap.get(sessionId);
        }
    }

    /**
     * 获取key列表
     * @return
     */
    public Set<String> getKeys(){
        synchronized (sessionMap){
            return sessionMap.keySet();
        }
    }

    /**
     * 根据key删除
     * @param key
     */
    public void remove(String key) {
        synchronized (sessionMap){
            sessionMap.remove(key);
        }
    }
}
