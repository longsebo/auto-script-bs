/**
 * 
 */
package com.autoscript.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lsb
 * 	短信工具类
 */
public class SMSUtils {
	private static Logger logger=LoggerFactory.getLogger(SMSUtils.class);
	/*
	 * 发送方法  
	 */
	public static int sendSMS(String cropId,String pwd,String smsgatewayDomainUrl,String Mobile,String Content,String send_time) throws Exception {
		URL url = null;
//		String CorpID="LKSDK0007205";//账户名
//		String Pwd="zh9527@";//密码
		String send_content=URLEncoder.encode(Content.replaceAll("<br/>", " "), "GBK");//发送内容
		url = new URL("https://"+smsgatewayDomainUrl+"/BatchSend2.aspx?CorpID="+cropId+"&Pwd="+pwd+"&Mobile="+Mobile+"&Content="+send_content+"&Cell=&SendTime="+send_time);
		BufferedReader in = null;
		int inputLine = 0;
		try {
			System.out.println("开始发送短信手机号码为 ："+Mobile);
			logger.info("开始发送短信手机号码为 ："+Mobile);
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			inputLine = new Integer(in.readLine()).intValue();
		} catch (Exception e) {
			System.out.println("网络异常,发送短信失败！");
			logger.error("网络异常,发送短信失败！", e);
			inputLine=-2;
		}
		System.out.println("结束发送短信返回值：  "+inputLine);
		logger.info("结束发送短信返回值：  "+inputLine);
		if(inputLine<=0)
			throw new Exception("发送短信失败!");
		return inputLine;
	}

}
