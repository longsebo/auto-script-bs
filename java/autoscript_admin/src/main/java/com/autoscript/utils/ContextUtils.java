/**
 * 
 */
package com.autoscript.utils;

import org.springframework.context.ApplicationContext;
/**
 * 上下文工具类
 * @author Administrator
 *
 */
public class ContextUtils {

    public static ApplicationContext context;

    private ContextUtils() {
    }

    public static void setApplicationContext(ApplicationContext applicationContext) {
        context = applicationContext;
    }

    public static Object getBean(String beanName) {
        return context.getBean(beanName);
    }

    public static <T> T getBean(Class<T> t) {
        return context.getBean(t);
    }
    /**
     * 根据参数名获取配置参数值
     * @param configName
     * @return
     */
    public static String getConfigValue(String configName) {
    	return context.getEnvironment().getProperty(configName);
    }
}
