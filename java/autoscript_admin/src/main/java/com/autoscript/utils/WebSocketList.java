package com.autoscript.utils;

import com.autoscript.endpoint.WebSocket;
import com.autoscript.endpoint.WebSocket;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class WebSocketList {
    private Map<String,WebSocket> webSocketMap;

    public WebSocketList(){
        webSocketMap = new HashMap<>();
    }

    /**
     * 放入队列
     * @param logNumber
     * @param webSocket
     */
    public void put(String logNumber,WebSocket webSocket){
        synchronized (webSocketMap){
            webSocketMap.put(logNumber,webSocket);
        }
    }

    /**
     * 根据日志编号获取WebSocket
     * @param logNumber
     * @return
     */
    public WebSocket get(String logNumber){
        synchronized (webSocketMap){
            return webSocketMap.get(logNumber);
        }
    }
    /**
     * 获取key列表
     * @return
     */
    public Set<String> getKeys(){
        synchronized (webSocketMap){
            return webSocketMap.keySet();
        }
    }

    /**
     * 根据日志号删除websocket
     * @param logNumber
     */
    public void remove(String logNumber) {
        synchronized (webSocketMap){
            webSocketMap.remove(logNumber);
        }
    }
}
