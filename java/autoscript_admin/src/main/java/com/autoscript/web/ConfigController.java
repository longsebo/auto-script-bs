package com.autoscript.web;

import com.autoscript.ISystemConstant;
import com.dfhc.util.StringHelper;
import com.dfhc.util.ConvertHelper;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import com.autoscript.vo.ConfigVo;
import com.autoscript.service.ConfigService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 配置表Controller
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-02
 */
@RestController
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RequestMapping(value ="/api/Config")
public class ConfigController {
    private static Logger logger=LoggerFactory.getLogger(ConfigController.class);
    /**
     * 配置表服务
     */
    @Autowired
    private ConfigService configService;
    /**
     * 新增配置表
     * ConfigVo  配置表VO
     * @return Map
     */
    @RequestMapping(value = "insert", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> insert(HttpServletRequest request,@RequestBody  ConfigVo configVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //插入配置表(含校验)
            configService.insert(configVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "配置表成功!");
        } catch (Exception e) {
            logger.error("配置表失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 更新配置表
     * ConfigVo  配置表VO
     * @return Map
     */
    @RequestMapping(value = "update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> update(HttpServletRequest request,@RequestBody  ConfigVo configVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新配置表(含校验)
            configService.update(configVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "配置表成功!");
        } catch (Exception e) {
            logger.error("配置表失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 删除配置表
     * ConfigVo  配置表VO
     * @return Map
     */
    @RequestMapping(value = "delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> delete(HttpServletRequest request,@RequestBody  ConfigVo configVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新配置表(含校验)
            configService.delete(configVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "配置表成功!");
        } catch (Exception e) {
            logger.error("配置表失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 根据id查询配置表
     * @return Map<String, Object>  应答Map
     */
    @RequestMapping(value ="getById" ,method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getById(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            String id = request.getParameter("id");
            Map m = configService.getById(id);
		
            //返回
            if(m==null) {
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "配置表数据没找到!");
            }else {
            	result.put(ISystemConstant.AJAX_BEAN,m);
            	result.put(ISystemConstant.AJAX_MESSAGE, "操作成功!");
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            }
        } catch (Exception e) {
            	logger.error("getById失败", e);
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "查询配置表数据失败!");
        }
        return result;

    }
    /**
     * 翻页查询配置表列表
     * @param HttpServletRequest
     * @return Map<String, Object> 查询列表应答Map
      */
    @RequestMapping(value = "listPage", method = RequestMethod.POST, produces =MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> listPage(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //TODO:获取查询条件
            String userId = request.getParameter("userId");
            if(StringHelper.isEmpty(userId))
            	throw new Exception("缺少参数:userId");
            //页号
            String strPageNo = request.getParameter(ISystemConstant.PAGE_NO);
            if(StringHelper.isEmpty(strPageNo))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_NO);
            //页大小
            String strPageSize = request.getParameter(ISystemConstant.PAGE_SIZE);
            if(StringHelper.isEmpty(strPageSize))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_SIZE);
		
            int pageNo = ConvertHelper.toInt(strPageNo);
            int pageSize = ConvertHelper.toInt(strPageSize);
            List<Map> list;
            logger.info("pageNo:"+pageNo);
            logger.info("pageSize:"+pageSize);
            Map<String, Object> searchMap;
            searchMap = new HashMap<String,Object>();
            //TODO 拼装条件
            searchMap.put("OPER_USER_ID",userId);
            list = configService.listPage(searchMap, pageNo, pageSize);
            logger.info("list count:"+list.size());
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_LIST,list);
            result.put("count",configService.count(searchMap));
            result.put(ISystemConstant.AJAX_MESSAGE, "查询成功!");
        } catch (Exception e) {
            logger.error("配置表查询失败,参数为:"+request.getParameter("userId"), e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
}
