package com.autoscript.web;

import com.autoscript.ISystemConstant;
import com.autoscript.service.ProjectService;
import com.autoscript.vo.Record;
import com.dfhc.util.StringHelper;
import com.dfhc.util.ConvertHelper;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import com.autoscript.vo.ProjectParameterVo;
import com.autoscript.service.ProjectParameterService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 工程参数Controller
 * @author lsb
 * @version 1.0.0
 * @date 2021-06-25
 */
@RestController
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RequestMapping(value ="/api/ProjectParameter")
public class ProjectParameterController {
    private static Logger logger=LoggerFactory.getLogger(ProjectParameterController.class);
    /**
     * 工程参数服务
     */
    @Autowired
    private ProjectParameterService projectParameterService;
    /**
     * 工程服务
     */
    @Autowired
    private ProjectService projectService;
    /**
     * 新增工程参数
     * ProjectParameterVo  工程参数VO
     * @return Map
     */
    @RequestMapping(value = "insert", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> insert(HttpServletRequest request,@RequestBody  ProjectParameterVo projectParameterVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //插入工程参数(含校验)
            projectParameterService.insert(projectParameterVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "工程参数成功!");
        } catch (Exception e) {
            logger.error("工程参数失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 更新工程参数
     * ProjectParameterVo  工程参数VO
     * @return Map
     */
    @RequestMapping(value = "update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> update(HttpServletRequest request,@RequestBody  ProjectParameterVo projectParameterVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新工程参数(含校验)
            projectParameterService.update(projectParameterVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "工程参数成功!");
        } catch (Exception e) {
            logger.error("工程参数失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 删除工程参数
     * ProjectParameterVo  工程参数VO
     * @return Map
     */
    @RequestMapping(value = "delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> delete(HttpServletRequest request,@RequestBody  ProjectParameterVo projectParameterVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新工程参数(含校验)
            projectParameterService.delete(projectParameterVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "工程参数成功!");
        } catch (Exception e) {
            logger.error("工程参数失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 根据id查询工程参数
     * @return Map<String, Object>  应答Map
     */
    @RequestMapping(value ="getById" ,method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getById(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            String id = request.getParameter("id");
            Map m = projectParameterService.getById(id);
		
            //返回
            if(m==null) {
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "工程参数数据没找到!");
            }else {
            	result.put(ISystemConstant.AJAX_BEAN,m);
            	result.put(ISystemConstant.AJAX_MESSAGE, "操作成功!");
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            }
        } catch (Exception e) {
            	logger.error("getById失败", e);
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "查询工程参数数据失败!");
        }
        return result;

    }
    /**
     * 翻页查询工程参数列表
     * @param request
     * @return Map<String, Object> 查询列表应答Map
      */
    @RequestMapping(value = "listPage", method = RequestMethod.POST, produces =MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> listPage(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //获取查询条件
            String projectId = request.getParameter("projectId");
            if(StringHelper.isEmpty(projectId))
            	throw new Exception("缺少参数:projectId");
            //页号
            String strPageNo = request.getParameter(ISystemConstant.PAGE_NO);
            if(StringHelper.isEmpty(strPageNo))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_NO);
            //页大小
            String strPageSize = request.getParameter(ISystemConstant.PAGE_SIZE);
            if(StringHelper.isEmpty(strPageSize))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_SIZE);
		
            int pageNo = ConvertHelper.toInt(strPageNo);
            int pageSize = ConvertHelper.toInt(strPageSize);
            List<Map> list;
            logger.info("pageNo:"+pageNo);
            logger.info("pageSize:"+pageSize);
            Map<String, Object> searchMap;
            searchMap = new HashMap<String,Object>();
            //拼装条件
            searchMap.put("PROJECT_ID",projectId);
            list = projectParameterService.listPage(searchMap, pageNo, pageSize);
            //查询工程名称
            Map<String,Object> projectMap;
            projectMap = projectService.getById(projectId);

            logger.info("list count:"+list.size());
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_LIST,list);
            result.put("count", projectParameterService.count(searchMap));
            if(projectMap!=null){
                result.put("projectName",projectMap.get("PROJECT_NAME"));
            }
            result.put(ISystemConstant.AJAX_MESSAGE, "查询成功!");
        } catch (Exception e) {
            logger.error("工程参数查询失败,参数为:"+request.getParameter("projectId"), e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
    /**
     * 根据工程id查询控件属性列表
     * @param request
     * @return Map<String, Object> 查询列表应答Map
     */
    @RequestMapping(value = "listControlProperitesByProjectId", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> listControlPropertiesByType(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //获取查询条件
            String projectId = request.getParameter("projectId");
            if(StringHelper.isEmpty(projectId))
                throw new Exception("缺少参数:projectId");

            List<Map> list;
            Map<String, Object> searchMap;
            searchMap = new HashMap<String,Object>();

            // 拼装条件
            searchMap.put("PROJECT_ID",projectId);
            list = projectParameterService.search(searchMap);
            List<Record> records;
            //转换为vue 控件属性格式
            records = projectParameterService.convertToVueRecords(list);

            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_LIST,records);
            result.put("count",projectParameterService.count(searchMap));
            result.put(ISystemConstant.AJAX_MESSAGE, "查询成功!");
        } catch (Exception e) {
            logger.error("工程属性查询失败,参数为:"+request.getParameter("projectId"), e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
    /**
     * 根据工程id和运行用户id查询工程最后参数列表
     * @param request
     * @return Map<String, Object> 查询列表应答Map
     */
    @RequestMapping(value = "getLastProjectParameterByProjectId", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getLastProjectParameterByProjectId(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //获取查询条件
            String projectId = request.getParameter("projectId");
            if(StringHelper.isEmpty(projectId))
                throw new Exception("缺少参数:projectId");
            //获取用户ID
            String runUserId = request.getParameter("runUserId");
            if(StringHelper.isEmpty(runUserId))
                throw new Exception("缺少参数:runUserId");
            List<Map> list;
            Map<String, Object> searchMap;
            searchMap = new HashMap<String,Object>();

            // 拼装条件
            searchMap.put("PROJECT_ID",projectId);
//            searchMap
            list = projectParameterService.search(searchMap);
            List<Record> records;
            //转换为vue 控件属性格式
            records = projectParameterService.convertToVueRecords(list);

            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_LIST,records);
            result.put("count",projectParameterService.count(searchMap));
            result.put(ISystemConstant.AJAX_MESSAGE, "查询成功!");
        } catch (Exception e) {
            logger.error("工程属性查询失败,参数为:"+request.getParameter("projectId"), e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
}
