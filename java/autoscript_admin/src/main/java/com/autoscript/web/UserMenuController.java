package com.autoscript.web;

import com.autoscript.ISystemConstant;
import com.autoscript.service.ParameterCacheService;
import com.autoscript.utils.SessionMap;
import com.autoscript.vo.MenuJsonVo;
import com.dfhc.util.StringHelper;
import com.dfhc.util.ConvertHelper;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.autoscript.vo.UserMenuVo;
import com.autoscript.service.UserMenuService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 用户菜单表Controller
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-02
 */
@RestController
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RequestMapping(value ="/api/UserMenu")
public class UserMenuController {
    private static Logger logger=LoggerFactory.getLogger(UserMenuController.class);
    /**
     * 用户菜单表服务
     */
    @Autowired
    private UserMenuService userMenuService;
    @Autowired
    private SessionMap sessionMap;
    /**
     * 新增用户菜单表
     * UserMenuVo  用户菜单表VO
     * @return Map
     */
    @RequestMapping(value = "insert", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> insert(HttpServletRequest request,@RequestBody  UserMenuVo userMenuVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //插入用户菜单表(含校验)
            userMenuService.insert(userMenuVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "用户菜单表成功!");
        } catch (Exception e) {
            logger.error("用户菜单表失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 更新用户菜单表
     * UserMenuVo  用户菜单表VO
     * @return Map
     */
    @RequestMapping(value = "update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> update(HttpServletRequest request,@RequestBody  UserMenuVo userMenuVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新用户菜单表(含校验)
            userMenuService.update(userMenuVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "用户菜单表成功!");
        } catch (Exception e) {
            logger.error("用户菜单表失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 删除用户菜单表
     * UserMenuVo  用户菜单表VO
     * @return Map
     */
    @RequestMapping(value = "delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> delete(HttpServletRequest request,@RequestBody  UserMenuVo userMenuVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新用户菜单表(含校验)
            userMenuService.delete(userMenuVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "用户菜单表成功!");
        } catch (Exception e) {
            logger.error("用户菜单表失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 根据id查询用户菜单表
     * @return Map<String, Object>  应答Map
     */
    @RequestMapping(value ="getById" ,method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getById(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            String id = request.getParameter("id");
            Map m = userMenuService.getById(id);
		
            //返回
            if(m==null) {
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "用户菜单表数据没找到!");
            }else {
            	result.put(ISystemConstant.AJAX_BEAN,m);
            	result.put(ISystemConstant.AJAX_MESSAGE, "操作成功!");
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            }
        } catch (Exception e) {
            	logger.error("getById失败", e);
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "查询用户菜单表数据失败!");
        }
        return result;

    }
    /**
     * 翻页查询用户菜单表列表
     * @param HttpServletRequest
     * @return Map<String, Object> 查询列表应答Map
      */
    @RequestMapping(value = "listPage", method = RequestMethod.POST, produces =MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> listPage(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //TODO:获取查询条件
            String userId = request.getParameter("userId");
            if(StringHelper.isEmpty(userId))
            	throw new Exception("缺少参数:userId");
            //页号
            String strPageNo = request.getParameter(ISystemConstant.PAGE_NO);
            if(StringHelper.isEmpty(strPageNo))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_NO);
            //页大小
            String strPageSize = request.getParameter(ISystemConstant.PAGE_SIZE);
            if(StringHelper.isEmpty(strPageSize))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_SIZE);
		
            int pageNo = ConvertHelper.toInt(strPageNo);
            int pageSize = ConvertHelper.toInt(strPageSize);
            List<Map> list;
            logger.info("pageNo:"+pageNo);
            logger.info("pageSize:"+pageSize);
            Map<String, Object> searchMap;
            searchMap = new HashMap<String,Object>();
            //TODO 拼装条件
            searchMap.put("OPER_USER_ID",userId);
            list = userMenuService.listPage(searchMap, pageNo, pageSize);
            logger.info("list count:"+list.size());
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_LIST,list);
            result.put("count",userMenuService.count(searchMap));
            result.put(ISystemConstant.AJAX_MESSAGE, "查询成功!");
        } catch (Exception e) {
            logger.error("用户菜单表查询失败,参数为:"+request.getParameter("userId"), e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
    /**
     * 查询当前登录用户拥有菜单列表
     * @return Map<String, Object>  应答Map
     */
    @RequestMapping(value ="getCurrentUserMenus" ,method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getCurrentUserMenus(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            Map<String,Object> hSessionMap = sessionMap.get(request.getHeader(ISystemConstant.SESSION_ID));
            if(hSessionMap==null){
                result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
                result.put(ISystemConstant.AJAX_MESSAGE, "无效session!");
                return result;
            }

            Map<String, Object> userMap =( Map<String, Object>) hSessionMap.get(ISystemConstant.CURRENT_USER);
            if(userMap==null){
                result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
                result.put(ISystemConstant.AJAX_MESSAGE, "尚未登录!");
                return result;
            }
           //查询当前用户下拥有菜单
            Map<String,Object> searchMap = new HashMap<>();
            searchMap.put("USER_ID",userMap.get("ID"));
            List<Map> userMenus = userMenuService.search(searchMap);
            //转换为MenuJson 菜单列表
            //获取全部菜单
            List<Map> allMenus =(List<Map>) ParameterCacheService.getSingleInstance().get(ISystemConstant.ALL_MENU);

            List<MenuJsonVo> returnMenuJsonVos = userMenuService.convertToMenuJsonVos(allMenus, userMenus);
            //返回
            result.put(ISystemConstant.AJAX_LIST,returnMenuJsonVos);
            result.put(ISystemConstant.AJAX_MESSAGE, "操作成功!");
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
        } catch (Exception e) {
            logger.error("getCurrentUserMenus失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, "查询用户菜单表数据失败!");
        }
        return result;

    }

}
