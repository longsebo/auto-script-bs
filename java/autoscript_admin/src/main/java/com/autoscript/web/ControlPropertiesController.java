package com.autoscript.web;

import com.autoscript.ISystemConstant;
import com.autoscript.service.ControlService;
import com.autoscript.vo.Record;
import com.dfhc.util.StringHelper;
import com.dfhc.util.ConvertHelper;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import com.autoscript.vo.ControlPropertiesVo;
import com.autoscript.service.ControlPropertiesService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 控件属性Controller
 * @author lsb
 * @version 1.0.0
 * @date 2021-05-29
 */
@RestController
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RequestMapping(value ="/api/ControlProperties")
public class ControlPropertiesController {
    private static Logger logger=LoggerFactory.getLogger(ControlPropertiesController.class);
    /**
     * 控件属性服务
     */
    @Autowired
    private ControlPropertiesService controlPropertiesService;
    /**
     * 控件服务
     */
    @Autowired
    private ControlService controlService;
    /**
     * 新增控件属性
     * ControlPropertiesVo  控件属性VO
     * @return Map
     */
    @RequestMapping(value = "insert", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> insert(HttpServletRequest request,@RequestBody  ControlPropertiesVo controlPropertiesVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //插入控件属性(含校验)
            controlPropertiesService.insert(controlPropertiesVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "控件属性成功!");
        } catch (Exception e) {
            logger.error("控件属性失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 更新控件属性
     * ControlPropertiesVo  控件属性VO
     * @return Map
     */
    @RequestMapping(value = "update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> update(HttpServletRequest request,@RequestBody  ControlPropertiesVo controlPropertiesVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新控件属性(含校验)
            controlPropertiesService.update(controlPropertiesVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "控件属性成功!");
        } catch (Exception e) {
            logger.error("控件属性失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 删除控件属性
     * ControlPropertiesVo  控件属性VO
     * @return Map
     */
    @RequestMapping(value = "delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> delete(HttpServletRequest request,@RequestBody  ControlPropertiesVo controlPropertiesVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新控件属性(含校验)
            controlPropertiesService.delete(controlPropertiesVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "控件属性成功!");
        } catch (Exception e) {
            logger.error("控件属性失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 根据id查询控件属性
     * @return Map<String, Object>  应答Map
     */
    @RequestMapping(value ="getById" ,method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getById(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            String id = request.getParameter("id");
            Map m = controlPropertiesService.getById(id);
		
            //返回
            if(m==null) {
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "控件属性数据没找到!");
            }else {
            	result.put(ISystemConstant.AJAX_BEAN,m);
            	result.put(ISystemConstant.AJAX_MESSAGE, "操作成功!");
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            }
        } catch (Exception e) {
            	logger.error("getById失败", e);
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "查询控件属性数据失败!");
        }
        return result;

    }
    /**
     * 翻页查询控件属性列表
     * @param request
     * @return Map<String, Object> 查询列表应答Map
      */
    @RequestMapping(value = "listPage", method = RequestMethod.POST, produces =MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> listPage(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //TODO:获取查询条件
            String controlId = request.getParameter("controlId");
            if(StringHelper.isEmpty(controlId))
                throw new Exception("缺少参数:controlId");
            //页号
            String strPageNo = request.getParameter(ISystemConstant.PAGE_NO);
            if(StringHelper.isEmpty(strPageNo))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_NO);
            //页大小
            String strPageSize = request.getParameter(ISystemConstant.PAGE_SIZE);
            if(StringHelper.isEmpty(strPageSize))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_SIZE);
		
            int pageNo = ConvertHelper.toInt(strPageNo);
            int pageSize = ConvertHelper.toInt(strPageSize);
            List<Map> list;
            logger.info("pageNo:"+pageNo);
            logger.info("pageSize:"+pageSize);
            Map<String, Object> searchMap;
            searchMap = new HashMap<String,Object>();
            //TODO 拼装条件
            searchMap.put("CONTROL_ID",controlId);
            list = controlPropertiesService.listPage(searchMap, pageNo, pageSize);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_LIST,list);
            //查询控件id对应名称
            Map controlMap = controlService.getById(controlId);
            result.put("controlRemark",controlMap.get("REMARK"));
            result.put("count",controlPropertiesService.count(searchMap));
            result.put(ISystemConstant.AJAX_MESSAGE, "查询成功!");
        } catch (Exception e) {
            logger.error("控件属性查询失败,参数为:"+request.getParameter("userId"), e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
    /**
     * 根据控件类型查询控件属性列表
     * @param request
     * @return Map<String, Object> 查询列表应答Map
     */
    @RequestMapping(value = "listControlProperitesByType", method = RequestMethod.GET, produces =MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> listControlPropertiesByType(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //TODO:获取查询条件
            String controlType = request.getParameter("controlType");
            if(StringHelper.isEmpty(controlType))
                throw new Exception("缺少参数:controlType");

            List<Map> list;
            Map<String, Object> searchMap;
            searchMap = new HashMap<String,Object>();
            searchMap.put("CONTROL_TYPE",controlType);

            List<Map> controlVos = controlService.search(searchMap);
            if(controlVos==null||controlVos.isEmpty())
                throw new Exception("无效的控件类型:"+controlType);
            //TODO 拼装条件
            searchMap.clear();
            searchMap.put("CONTROL_ID",controlVos.get(0).get("ID"));
            list = controlPropertiesService.search(searchMap);
            logger.info("list count:"+list.size());
            List<Record> records;
            //转换为vue 控件属性格式
            records = controlPropertiesService.convertToVueRecords(list);

            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_LIST,records);
            result.put("count",controlPropertiesService.count(searchMap));
            result.put(ISystemConstant.AJAX_MESSAGE, "查询成功!");
        } catch (Exception e) {
            logger.error("控件属性查询失败,参数为:"+request.getParameter("userId"), e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
}
