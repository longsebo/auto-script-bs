package com.autoscript.web;

import com.autoscript.ISystemConstant;
import com.autoscript.service.ProjectService;
import com.autoscript.vo.BatchShareProjectVo;
import com.dfhc.util.StringHelper;
import com.dfhc.util.ConvertHelper;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import com.autoscript.vo.ShareProjectVo;
import com.autoscript.service.ShareProjectService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 分享工程Controller
 * @author lsb
 * @version 1.0.0
 * @date 2021-06-25
 */
@RestController
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RequestMapping(value ="/api/ShareProject")
public class ShareProjectController {
    private static Logger logger=LoggerFactory.getLogger(ShareProjectController.class);
    /**
     * 分享工程服务
     */
    @Autowired
    private ShareProjectService shareProjectService;
    /**
     * 工程服务
     */
    @Autowired
    private ProjectService projectService;
    /**
     * 新增分享工程
     * ShareProjectVo  分享工程VO
     * @return Map
     */
    @RequestMapping(value = "insert", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> insert(HttpServletRequest request,@RequestBody  ShareProjectVo shareProjectVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //插入分享工程(含校验)
            shareProjectService.insert(shareProjectVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "分享工程成功!");
        } catch (Exception e) {
            logger.error("分享工程失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 更新分享工程
     * ShareProjectVo  分享工程VO
     * @return Map
     */
    @RequestMapping(value = "update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> update(HttpServletRequest request,@RequestBody  ShareProjectVo shareProjectVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新分享工程(含校验)
            shareProjectService.update(shareProjectVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "分享工程成功!");
        } catch (Exception e) {
            logger.error("分享工程失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 删除分享工程
     * ShareProjectVo  分享工程VO
     * @return Map
     */
    @RequestMapping(value = "delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> delete(HttpServletRequest request,@RequestBody  ShareProjectVo shareProjectVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新分享工程(含校验)
            shareProjectService.delete(shareProjectVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "分享工程成功!");
        } catch (Exception e) {
            logger.error("分享工程失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 根据id查询分享工程
     * @return Map<String, Object>  应答Map
     */
    @RequestMapping(value ="getById" ,method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getById(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            String id = request.getParameter("id");
            Map m = shareProjectService.getById(id);
		
            //返回
            if(m==null) {
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "分享工程数据没找到!");
            }else {
            	result.put(ISystemConstant.AJAX_BEAN,m);
            	result.put(ISystemConstant.AJAX_MESSAGE, "操作成功!");
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            }
        } catch (Exception e) {
            	logger.error("getById失败", e);
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "查询分享工程数据失败!");
        }
        return result;

    }
    /**
     * 翻页查询分享工程列表
     * @param request
     * @return Map<String, Object> 查询列表应答Map
      */
    @RequestMapping(value = "listPage", method = RequestMethod.POST, produces =MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> listPage(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //获取查询条件
            String projectId = request.getParameter("projectId");
            if(StringHelper.isEmpty(projectId))
            	throw new Exception("缺少参数:projectId");
            //页号
            String strPageNo = request.getParameter(ISystemConstant.PAGE_NO);
            if(StringHelper.isEmpty(strPageNo))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_NO);
            //页大小
            String strPageSize = request.getParameter(ISystemConstant.PAGE_SIZE);
            if(StringHelper.isEmpty(strPageSize))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_SIZE);
		
            int pageNo = ConvertHelper.toInt(strPageNo);
            int pageSize = ConvertHelper.toInt(strPageSize);
            List<Map> list;
            logger.info("pageNo:"+pageNo);
            logger.info("pageSize:"+pageSize);
            Map<String, Object> searchMap;
            searchMap = new HashMap<String,Object>();
            //拼装条件
            searchMap.put("PROJECT_ID",projectId);
            list = shareProjectService.listPage(searchMap, pageNo, pageSize);
            //查询工程名称
            Map<String,Object> projectMap;
            projectMap = projectService.getById(projectId);
            logger.info("list count:"+list.size());
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_LIST,list);
            result.put("count", shareProjectService.count(searchMap));
            result.put(ISystemConstant.AJAX_MESSAGE, "查询成功!");
            if(projectMap!=null){
                result.put("projectName",projectMap.get("PROJECT_NAME"));
            }
        } catch (Exception e) {
            logger.error("分享工程查询失败,参数为:"+request.getParameter("projectId"), e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
    /**
     * 批量新增分享工程
     * ShareProjectVo  批量分享工程VO
     * @return Map
     */
    @RequestMapping(value = "batchinsert", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> batchinsert(HttpServletRequest request, @RequestBody BatchShareProjectVo batchShareProjectVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //插入分享工程(含校验)
            shareProjectService.batchInsert(batchShareProjectVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "分享工程成功!");
        } catch (Exception e) {
            logger.error("分享工程失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
}
