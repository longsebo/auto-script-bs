package com.autoscript.web;

import com.autoscript.ISystemConstant;
import com.autoscript.service.FileDownloadService;
import com.dfhc.util.StringHelper;
import com.dfhc.util.ConvertHelper;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import com.autoscript.vo.RunlogVo;
import com.autoscript.service.RunlogService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 运行日志表Controller
 * @author lsb
 * @version 1.0.0
 */
@RestController
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RequestMapping(value ="/api/Runlog")
public class RunlogController {
    private static Logger logger=LoggerFactory.getLogger(RunlogController.class);
    /**
     * 运行日志表服务
     */
    @Autowired
    private RunlogService runlogService;
    @Autowired
    private FileDownloadService fileDownloadService;
    /**
     * 新增运行日志表
     * RunlogVo  运行日志表VO
     * @return Map
     */
    @RequestMapping(value = "insert", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> insert(HttpServletRequest request,@RequestBody  RunlogVo runlogVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //插入运行日志表(含校验)
            runlogService.insert(runlogVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "运行日志表成功!");
        } catch (Exception e) {
            logger.error("运行日志表失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 更新运行日志表
     * RunlogVo  运行日志表VO
     * @return Map
     */
    @RequestMapping(value = "update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> update(HttpServletRequest request,@RequestBody  RunlogVo runlogVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新运行日志表(含校验)
            runlogService.update(runlogVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "运行日志表成功!");
        } catch (Exception e) {
            logger.error("运行日志表失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 删除运行日志表
     * RunlogVo  运行日志表VO
     * @return Map
     */
    @RequestMapping(value = "delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> delete(HttpServletRequest request,@RequestBody  RunlogVo runlogVo){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //更新运行日志表(含校验)
            runlogService.delete(runlogVo);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_MESSAGE, "运行日志表成功!");
        } catch (Exception e) {
            logger.error("运行日志表失败", e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;   
    }
    /**
     * 根据id查询运行日志表
     * @return Map<String, Object>  应答Map
     */
    @RequestMapping(value ="getById" ,method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getById(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            String id = request.getParameter("id");
            Map m = runlogService.getById(id);
		
            //返回
            if(m==null) {
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "运行日志表数据没找到!");
            }else {
            	result.put(ISystemConstant.AJAX_BEAN,m);
            	result.put(ISystemConstant.AJAX_MESSAGE, "操作成功!");
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            }
        } catch (Exception e) {
            	logger.error("getById失败", e);
            	result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            	result.put(ISystemConstant.AJAX_MESSAGE, "查询运行日志表数据失败!");
        }
        return result;

    }
    /**
     * 翻页查询运行日志表列表
     * @param request
     * @return Map<String, Object> 查询列表应答Map
      */
    @RequestMapping(value = "listPage", method = RequestMethod.POST, produces =MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> listPage(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            //TODO:获取查询条件
            String userId = request.getParameter("userId");
            if(StringHelper.isEmpty(userId))
            	throw new Exception("缺少参数:userId");
            //页号
            String strPageNo = request.getParameter(ISystemConstant.PAGE_NO);
            if(StringHelper.isEmpty(strPageNo))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_NO);
            //页大小
            String strPageSize = request.getParameter(ISystemConstant.PAGE_SIZE);
            if(StringHelper.isEmpty(strPageSize))
            	throw new Exception("缺少参数:"+ISystemConstant.PAGE_SIZE);
		
            int pageNo = ConvertHelper.toInt(strPageNo);
            int pageSize = ConvertHelper.toInt(strPageSize);
            List<Map> list;
            logger.info("pageNo:"+pageNo);
            logger.info("pageSize:"+pageSize);
            Map<String, Object> searchMap;
            searchMap = new HashMap<String,Object>();
            //TODO 拼装条件
            searchMap.put("OPER_USER_ID",userId);
            list = runlogService.listPage(searchMap, pageNo, pageSize);
            logger.info("list count:"+list.size());
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_SUCCESS);
            result.put(ISystemConstant.AJAX_LIST,list);
            result.put("count",runlogService.count(searchMap));
            result.put(ISystemConstant.AJAX_MESSAGE, "查询成功!");
        } catch (Exception e) {
            logger.error("运行日志表查询失败,参数为:"+request.getParameter("userId"), e);
            result.put(ISystemConstant.AJAX_STATUS, ISystemConstant.AJAX_RESULT_FAIL);
            result.put(ISystemConstant.AJAX_MESSAGE, e.getMessage());
        }
        return result;
    }
    /**
     * 根据id 下载结果文件
     */
    @RequestMapping(value ="downloadById" )
    public void downloadById(HttpServletRequest request, HttpServletResponse response){
        try {
            //TODO:登录检查
            String id = request.getParameter("id");
            if (StringHelper.isEmpty(id)) {
                response.getWriter().println("缺少id参数!");
                return;
            }
            Map m = runlogService.getById(id);

            //返回
            if(m==null) {
                response.getWriter().println("运行日志表数据没找到!");
            }else {
                fileDownloadService.download((String)m.get("RESULT_FILE"),response);
            }
        } catch (Exception e) {
            logger.error("downloadById失败", e);
            try {
                response.getWriter().println("下载结果文件失败!");
            }catch(Exception e1){
                e1.printStackTrace();
            }
        }
    }
    /**
     * 根据日志号 下载结果文件
     */
    @RequestMapping(value ="downloadByLogNum" )
    public void downloadByLogNum(HttpServletRequest request, HttpServletResponse response){
        try {
            //TODO:登录检查
            String logNum = request.getParameter("logNum");
            if (StringHelper.isEmpty(logNum)) {
                response.getWriter().println("缺少日志号参数!");
                return;
            }
            Map<String, Object> searchMap = new HashMap<>();
            searchMap.put("LOG_CODE",logNum);
            List<Map> vos = runlogService.search(searchMap);

            //返回
            if(CollectionUtils.isEmpty(vos)) {
                response.getWriter().println("运行日志表数据没找到!");
            }else {
                Map m = vos.get(0);
                fileDownloadService.download((String)m.get("RESULT_FILE"),response);
            }
        } catch (Exception e) {
            logger.error("downloadByLogNum失败", e);
            try {
                response.getWriter().println("下载结果文件失败!");
            }catch(Exception e1){
                e1.printStackTrace();
            }
        }
    }
}
