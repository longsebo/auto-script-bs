/**
 * 
 */
package com.autoscript.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.dfhc.util.FileOperateHelper;

/**
 * 文件下载服务
 * @author 龙色波
 */
@Service
public class FileDownloadService {
	private static Logger logger=LoggerFactory.getLogger(FileDownloadService.class);
	/**
	 * 下载文件
	 * @param fileName 文件名
	 * @param response http应答对象
	 * @throws Exception 
	 */
	public void download(String fileName, HttpServletResponse response) throws Exception{
		//判断文件是否存在
		if(!FileOperateHelper.isExists(fileName)){
			response.setContentType("text/html;charset=utf-8");   
			response.getWriter().print("<script>alert('没有找到文件!');</script>");
			return;
		}
		// 写流文件到前端浏览器   
		ServletOutputStream out = response.getOutputStream();   
		response.addHeader("Content-Disposition", "attachment;filename="+ new String((FileOperateHelper.getFileName(fileName)).getBytes("GBK"), "ISO8859-1"));
		response.setContentType("application/octet-stream;charset=UTF-8");   
		response.addHeader("Content-Length", ""+FileOperateHelper.getFileLength(fileName));
		BufferedInputStream bis = null;   
		BufferedOutputStream bos = null;   
		try {   
		     bis = new BufferedInputStream(new FileInputStream(fileName));   
		     bos = new BufferedOutputStream(out);   
		      byte[] buff = new byte[1024];   
		      int bytesRead;   
		      while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {   
		        bos.write(buff, 0, bytesRead);   
		      }   
		      out.flush();
	    } catch (IOException e) {   
	    	logger.error("",e);
	    	throw e;
	    } finally {   
	      if (bis != null){   
	         bis.close();
	      }	         
	      if (bos != null){   
	         bos.close();  
	      }	         
	      if(out!=null){
	    	  out.close();
	      }	    	  
		}   
	}
	
	
	/**
	 * 查看报表文件
	 * @param fileName 文件名
	 * @param byteArray 字节数组
	 * @param response http应答对象
	 * @throws Exception 
	 */
	public void downloadStream(String fileName,byte[] byteArray, HttpServletResponse response) throws Exception{
		
		
		// 写流文件到前端浏览器   
		ServletOutputStream out = response.getOutputStream();   
		response.addHeader("Content-Disposition", "attachment;filename="+ new String((FileOperateHelper.getFileName(fileName)).getBytes("GBK"), "ISO8859-1"));
		response.setContentType("application/octet-stream;charset=UTF-8");   
		response.addHeader("Content-Length", ""+byteArray.length);
		BufferedInputStream bis = null;   
		BufferedOutputStream bos = null; 
		ByteArrayInputStream in=null;
		try {   
			 in = new ByteArrayInputStream(byteArray);
			 
		     bis = new BufferedInputStream(in);   
		     bos = new BufferedOutputStream(out);   
		     byte[] buff = new byte[1024];   
		     int bytesRead;   
		     while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {   
		        bos.write(buff, 0, bytesRead);   
		     }   
		     out.flush();
		     
	    } catch (IOException e) {   
	    	logger.error("", e);
	    	throw e;
	    } finally {   
	      if(in!=null){
	    	  in.close();
	      }	
	      if (bis != null){   
	         bis.close(); 
	         }  
	      if (bos != null){   
	         bos.close(); 
	      }
	      if(out!=null){
	    	  out.close();
	      }
		}   
	}
	
}
