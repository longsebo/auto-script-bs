/**
 * 
 */
package com.autoscript.service;

/**
 * @author Administrator
 * 数据库会话
 */
public class DBSessionService {
	private static DBSessionService dbSessionService;
	private String session;

	public static synchronized DBSessionService getSingleInstance() {
		if(dbSessionService==null) {
			dbSessionService = new DBSessionService();
		}
		return dbSessionService;
	}
	
	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}
	
	
}
