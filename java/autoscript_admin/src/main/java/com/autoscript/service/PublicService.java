/**
 * 
 */
package com.autoscript.service;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.autoscript.ISystemConstant;
import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.sequence.service.SequenceClientService;
import com.dfhc.util.StringHelper;
import com.dfhc.util.aes.GibberishAES;

/**
 * @author Administrator
  *  公共服务
 */
@Service
public class PublicService {
	private static Logger logger=LoggerFactory.getLogger(PublicService.class);
	/**
	 *    获取logo图片路径
	 * @return
	 * @throws Exception 
	 */
	public String getRootPath() throws Exception {
		//获取编码表类型信息
    	Map<String, Object> searchVo = new HashMap<String,Object>();
		searchVo.put("TYPE_KEYWORD",ISystemConstant.CODE_TYPE_PATH);
		List<Map> maps = SecurityDBClientService.getInstance().search(DBSessionService.getSingleInstance().getSession(), "rm_code_type", searchVo);
        if(maps == null||maps.size()==0)
        	throw new Exception("编码表类型表，缺少:"+ISystemConstant.CODE_TYPE_PATH);
        Map m = maps.get(0);
        String typeId = (String) m.get("ID");
        if(StringHelper.isEmpty(typeId))
        	throw new Exception("编码表类型:"+ISystemConstant.CODE_TYPE_PATH+"对应记录id为空!");
        //获取编码表参数值
        searchVo.clear();
        searchVo.put("CODE_TYPE_ID",typeId);
        searchVo.put("DATA_KEY",ISystemConstant.CODE_DATA_ROOTPATH);
		maps = SecurityDBClientService.getInstance().search(DBSessionService.getSingleInstance().getSession(), "rm_code_data", searchVo);
        if(maps == null||maps.size()==0)
        	throw new Exception("编码表数据表，缺少:"+ISystemConstant.CODE_DATA_ROOTPATH);
        m = maps.get(0);
        String rootPath = (String) m.get("DATA_VALUE");
        if(StringHelper.isEmpty(rootPath))
        	throw new Exception("编码表数据表，参数:"+ISystemConstant.CODE_DATA_ROOTPATH+"值为空!");
        //自动末尾追加目录分隔符
        String pathSeparator = File.separator;
		if(!rootPath.endsWith(pathSeparator)) {
			rootPath = rootPath +pathSeparator;
		}
        return rootPath;
	}
	/**
	 * 	获取完整的logo图标文件名
	 * @param logoImg 
	 * @return
	 * @throws Exception
	 */
	public String getLogoFileName(String logoImg) throws Exception{
		String rootPath = getRootPath();
		return rootPath+logoImg;
	}
	/**
	 * 根据编码表类型，参数名获取参数值
	 * @param codeType
	 * @param paramName
	 * @return
	 * @throws Exception
	 */
	public String getParameterValue(String codeType,String paramName) throws Exception{
		//获取编码表类型信息
    	Map<String, Object> searchVo = new HashMap<String,Object>();
		searchVo.put("TYPE_KEYWORD",codeType);
		List<Map> maps = SecurityDBClientService.getInstance().search(DBSessionService.getSingleInstance().getSession(), "rm_code_type", searchVo);
        if(maps == null||maps.size()==0)
        	throw new Exception("编码表类型表，缺少:"+codeType);
        if(maps.size()>1)
        	throw new Exception("编码表类型表，参数类型:"+codeType+"配置重复!");
        Map m = maps.get(0);
        String typeId = (String) m.get("ID");
        if(StringHelper.isEmpty(typeId))
        	throw new Exception("编码表类型:"+codeType+"对应记录id为空!");
        //获取编码表参数值
        searchVo.clear();
        searchVo.put("CODE_TYPE_ID",typeId);
        searchVo.put("DATA_KEY",paramName);
		maps = SecurityDBClientService.getInstance().search(DBSessionService.getSingleInstance().getSession(), "rm_code_data", searchVo);
        if(maps == null||maps.size()==0)
        	throw new Exception("编码表数据表，缺少:"+paramName);
        if(maps.size()>1)
        	throw new Exception("编码表数据表，参数类型:"+codeType+",参数:"+paramName+"出现多条!");
        m = maps.get(0);
        String paramValue = (String) m.get("DATA_VALUE");
        if(StringHelper.isEmpty(paramValue))
        	throw new Exception("编码表数据表，参数:"+paramName+"值为空!");
		return paramValue;
	}
	/**
	 * 	获取二维码图片存放位置
	 *  @return
	 * @throws Exception 
	 */
	public String getQrCodePath() throws Exception {
		String rootPath = getRootPath();
		String pathSeparator = File.separator;
		return rootPath+ISystemConstant.RELA_QRCODE_PATH+pathSeparator;
	}
	/**
	 * 产生指定表的id 数组
	 * @param tableName
	 * @param idNum
	 * @return
	 * @throws Exception 
	 */
	public String[] getIds(String tableName, int idNum) throws Exception {
		String sequenceUrl = getParameterValue(ISystemConstant.CODE_TYPE_SEQUENCE, ISystemConstant.CODE_DATA_URL);
		String sequenceDomain = getParameterValue(ISystemConstant.CODE_TYPE_SEQUENCE, ISystemConstant.CODE_DATA_DOMAIN);
		Date d = new Date();
		return SequenceClientService.getInstance().getNextSequenceValue(sequenceUrl, idNum,sequenceDomain, tableName, d,d,d);
	}
	/**
	 * AES 解密
	 * @param aesKey
	 * @param value
	 * @return 
	 * @throws Exception 
	 */
	public String aesDec(String aesKey, String value) throws Exception {
		GibberishAES aes = new GibberishAES();
		try {
			aes.setSize(256);
			return aes.dec(value,aesKey,  false);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("aes解密失败,aesKey:"+aesKey+",value:"+value, e);
			throw e;
		}
	}
	/**
	 * 	获取app用户头像图片存放位置
	 *  @return
	 * @throws Exception 
	 */
	public String getAppUserHeadImgPath() throws Exception {
		String rootPath = getRootPath();
		String pathSeparator = File.separator;
		return rootPath+ISystemConstant.RELA_APPUSER_HEAD_IMG_PATH+pathSeparator;
	}	
}
