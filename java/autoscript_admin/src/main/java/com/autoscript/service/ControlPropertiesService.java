package com.autoscript.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.autoscript.ISystemConstant;
import com.autoscript.vo.Options;
import com.autoscript.vo.Record;
import com.dfhc.util.ConvertHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;
import com.autoscript.vo.ControlPropertiesVo;
/**
 * 控件属性服务
 * @author lsb
 * @version 1.0.0
 * @date 2021-05-29
 */
@Service
public class ControlPropertiesService {
	private static Logger logger=LoggerFactory.getLogger(ControlPropertiesService.class);
	@Autowired
	private PublicService publicService;
	/**
	 * 表名
	 */
	private String TABLE_NAME="AS_CONTROL_PROPERTIES";

	/**
	 * 统计记录数
	 * @param searchMap
	 * @return
	 */
	public long count(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().count(getDBSesion(), TABLE_NAME, searchMap);
	}

	/**
	 *  插入
	 * @param controlPropertiesVo
	 * @throws Exception 
	 */
	public void insert(ControlPropertiesVo controlPropertiesVo) throws Exception {
		Map<String, Object> data;
		data = new HashMap<String,Object>();
		//获取序列id
		if(StringHelper.isEmpty(controlPropertiesVo.getId())){
			data.put("ID",publicService.getIds(TABLE_NAME, 1)[0]);
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getId())){
			data.put("ID",controlPropertiesVo.getId());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getControlId())){
			data.put("CONTROL_ID",controlPropertiesVo.getControlId());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPropertyType())){
			data.put("PROPERTY_TYPE",controlPropertiesVo.getPropertyType());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPropertyTitle())){
			data.put("PROPERTY_TITLE",controlPropertiesVo.getPropertyTitle());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getWidth())){
			data.put("WIDTH",controlPropertiesVo.getWidth());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getRules())){
			data.put("RULES",controlPropertiesVo.getRules());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getOptions())){
			data.put("OPTIONS",controlPropertiesVo.getOptions());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getBindModel())){
			data.put("BIND_MODEL",controlPropertiesVo.getBindModel());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPlaceholder())){
			data.put("PLACEHOLDER",controlPropertiesVo.getPlaceholder());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMaxLength())){
			data.put("MAX_LENGTH",controlPropertiesVo.getMaxLength());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getStyle())){
			data.put("STYLE",controlPropertiesVo.getStyle());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getInitVal())){
			data.put("INIT_VAL",controlPropertiesVo.getInitVal());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMaxVal())){
			data.put("MAX_VAL",controlPropertiesVo.getMaxVal());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMinVal())){
			data.put("MIN_VAL",controlPropertiesVo.getMinVal());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPrecision())){
			data.put("PRECISION",controlPropertiesVo.getPrecision());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getStep())){
			data.put("STEP",controlPropertiesVo.getStep());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMaxRows())){
			data.put("MAX_ROWS",controlPropertiesVo.getMaxRows());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMinRows())){
			data.put("MIN_ROWS",controlPropertiesVo.getMinRows());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getInitRowNum())){
			data.put("INIT_ROW_NUM",controlPropertiesVo.getInitRowNum());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getInitColumnNum())){
			data.put("INIT_COLUMN_NUM",controlPropertiesVo.getInitColumnNum());
		}
		int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
		if(rowNum!=1) {
			logger.error("插入控件属性失败！数据为:"+data+",rowNum:"+rowNum);
			throw new Exception("插入控件属性失败！");
		}
	}
	
	/**
	 * 获取db会话
	 * @return
	 */
	private String getDBSesion() {
		return DBSessionService.getSingleInstance().getSession();
	}
	/**
	 * 更新控件属性
	 * @param controlPropertiesVo
	 * @return
	 * @throws Exception
	 */
	public int update(ControlPropertiesVo controlPropertiesVo) throws Exception{
		Map<String, Object> searchMap;
		Map<String, Object> data;

		if(StringHelper.isEmpty(controlPropertiesVo.getId()))
			throw new Exception("控件属性id为空!");
		searchMap = new HashMap<String,Object>();

		searchMap.put("ID",controlPropertiesVo.getId());
		data = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(controlPropertiesVo.getId())){
			data.put("ID",controlPropertiesVo.getId());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getControlId())){
			data.put("CONTROL_ID",controlPropertiesVo.getControlId());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPropertyType())){
			data.put("PROPERTY_TYPE",controlPropertiesVo.getPropertyType());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPropertyTitle())){
			data.put("PROPERTY_TITLE",controlPropertiesVo.getPropertyTitle());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getWidth())){
			data.put("WIDTH",controlPropertiesVo.getWidth());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getRules())){
			data.put("RULES",controlPropertiesVo.getRules());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getOptions())){
			data.put("OPTIONS",controlPropertiesVo.getOptions());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getBindModel())){
			data.put("BIND_MODEL",controlPropertiesVo.getBindModel());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPlaceholder())){
			data.put("PLACEHOLDER",controlPropertiesVo.getPlaceholder());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMaxLength())){
			data.put("MAX_LENGTH",controlPropertiesVo.getMaxLength());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getStyle())){
			data.put("STYLE",controlPropertiesVo.getStyle());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getInitVal())){
			data.put("INIT_VAL",controlPropertiesVo.getInitVal());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMaxVal())){
			data.put("MAX_VAL",controlPropertiesVo.getMaxVal());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMinVal())){
			data.put("MIN_VAL",controlPropertiesVo.getMinVal());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPrecision())){
			data.put("PRECISION",controlPropertiesVo.getPrecision());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getStep())){
			data.put("STEP",controlPropertiesVo.getStep());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMaxRows())){
			data.put("MAX_ROWS",controlPropertiesVo.getMaxRows());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMinRows())){
			data.put("MIN_ROWS",controlPropertiesVo.getMinRows());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getInitRowNum())){
			data.put("INIT_ROW_NUM",controlPropertiesVo.getInitRowNum());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getInitColumnNum())){
			data.put("INIT_COLUMN_NUM",controlPropertiesVo.getInitColumnNum());
		}
		int rowNum = SecurityDBClientService.getInstance().update(getDBSesion(), TABLE_NAME, searchMap, data);
		if(rowNum<1) {
			logger.error("更新控件属性失败!数据为:searchMap:"+searchMap+",data:"+data+",rowNum:"+rowNum);
			throw new Exception("更新控件属性失败!");
		}
		return rowNum;
	}
	/**
	 * 删除控件属性
	 * @param controlPropertiesVo
	 * @return
	 * @throws Exception
	 */
	public int delete(ControlPropertiesVo controlPropertiesVo) throws Exception{
		Map<String,Object> searchMap = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(controlPropertiesVo.getId())){
			searchMap.put("ID",controlPropertiesVo.getId());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getControlId())){
			searchMap.put("CONTROL_ID",controlPropertiesVo.getControlId());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPropertyType())){
			searchMap.put("PROPERTY_TYPE",controlPropertiesVo.getPropertyType());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPropertyTitle())){
			searchMap.put("PROPERTY_TITLE",controlPropertiesVo.getPropertyTitle());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getRules())){
			searchMap.put("RULES",controlPropertiesVo.getRules());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getOptions())){
			searchMap.put("OPTIONS",controlPropertiesVo.getOptions());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getBindModel())){
			searchMap.put("BIND_MODEL",controlPropertiesVo.getBindModel());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPlaceholder())){
			searchMap.put("PLACEHOLDER",controlPropertiesVo.getPlaceholder());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMaxLength())){
			searchMap.put("MAX_LENGTH",controlPropertiesVo.getMaxLength());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getStyle())){
			searchMap.put("STYLE",controlPropertiesVo.getStyle());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getInitVal())){
			searchMap.put("INIT_VAL",controlPropertiesVo.getInitVal());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMaxVal())){
			searchMap.put("MAX_VAL",controlPropertiesVo.getMaxVal());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getMinVal())){
			searchMap.put("MIN_VAL",controlPropertiesVo.getMinVal());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getPrecision())){
			searchMap.put("PRECISION",controlPropertiesVo.getPrecision());
		}
		if(!StringHelper.isEmpty(controlPropertiesVo.getStep())){
			searchMap.put("STEP",controlPropertiesVo.getStep());
		}
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
		if(rowNum<1) {
			logger.error("删除控件属性失败,参数为:"+searchMap+",rowNum:"+rowNum);
			throw new Exception("删除控件属性失败!");
		}
		return rowNum;		
	}

	/**
	 * 查询控件属性
	 * @param searchMap 查询条件
	 * @param pageNo 页号
	 * @param pageSize 页大小
	 * @return
	 */
	public List<Map> listPage(Map<String, Object> searchMap, int pageNo, int pageSize) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap, pageNo, pageSize);
	}
	/**
	 * 查询控件属性
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<Map> search(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
	}
	/**
	 * 根据id查询控件属性
	 * @param id id标识
	 * @return
	 */
	public Map getById(String id) throws Exception {
		if(StringHelper.isEmpty(id))
			throw new Exception("控件属性id为空!");
		Map<String,Object> searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",id);
		List<Map> resultList =  SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
 		if(resultList==null||resultList.size()==0){
 		  return null;
 		}else{
 		  return resultList.get(0);
 		}
	}

	/**
	 * 转换为vue控件属性格式
	 * @param list
	 * @return
	 */
    public List<Record> convertToVueRecords(List<Map> list) throws Exception {
    	List<Record> records = new ArrayList<>();
    	for(Map m:list){
    		Record record= convertToVueRecord(m);
    		records.add(record);
		}
    	return records;
    }

	/**
	 * 转换map为Record
	 * @param m
	 * @return
	 */
	private Record convertToVueRecord(Map m) throws Exception {
		String type = (String) m.get("PROPERTY_TYPE");
		Record record;
		Options options;
		record = new Record();

		JSONArray jsonArray =null;
		if(!StringHelper.isEmpty((String)m.get("RULES"))){
			jsonArray  = JSON.parseArray((String)m.get("RULES"));
		}
		switch (type){
			case ISystemConstant.CTRL_PROPERTY_TYPE_INPUT:
				record.setType(type);
				record.setLabel((String)m.get("PROPERTY_TITLE"));
				record.setModel((String) m.get("BIND_MODEL"));
				record.setRules(jsonArray);
				options = new Options();
				record.setOptions(options);

				options.setWidth((String) m.get("WIDTH"));
				options.setPlaceholder((String) m.get("PLACEHOLDER"));
				options.setType(type);
				if(StringHelper.isNumber((String)m.get("MAX_LENGTH"))) {
					options.setMaxLength(ConvertHelper.toInt((String) m.get("MAX_LENGTH")));
				}else{
					options.setMaxLength(0);
				}
				options.setDefaultValue((String) m.get("INIT_VAL"));
				break;
			case ISystemConstant.CTRL_PROPERTY_TYPE_TEXTAREA:
				record.setType(type);
				record.setLabel((String)m.get("PROPERTY_TITLE"));
				record.setModel((String) m.get("BIND_MODEL"));
				record.setRules(jsonArray);
				options = new Options();
				record.setOptions(options);
				options.setWidth((String) m.get("WIDTH"));
				options.setPlaceholder((String) m.get("PLACEHOLDER"));
				options.setMaxRows((String) m.get("MAX_ROWS"));
				options.setMinRows((String) m.get("MIN_ROWS"));
				if(StringHelper.isNumber((String)m.get("MAX_LENGTH"))) {
					options.setMaxLength(ConvertHelper.toInt((String) m.get("MAX_LENGTH")));
				}else{
					options.setMaxLength(0);
				}
				options.setDefaultValue((String) m.get("INIT_VAL"));
				break;
			case ISystemConstant.CTRL_PROPERTY_TYPE_SWITCH:
				record.setType(type);
				record.setLabel((String)m.get("PROPERTY_TITLE"));
				record.setModel((String) m.get("BIND_MODEL"));
				record.setRules(jsonArray);
				options = new Options();
				record.setOptions(options);
				options.setDefaultValue((String) m.get("INIT_VAL"));
				break;
			case ISystemConstant.CTRL_PROPERTY_TYPE_SELECT:
				record.setType(type);
				record.setLabel((String)m.get("PROPERTY_TITLE"));
				//record.setModel((String) m.get("BIND_MODEL"));
				record.setRules(jsonArray);
				options = new Options();
				record.setOptions(options);
				options.setModel((String) m.get("BIND_MODEL"));
				options.setDynamic(false);
				options.setOptions(JSON.parseArray((String) m.get("OPTIONS")));
				break;
			case ISystemConstant.CTRL_PROPERTY_TYPE_TREE:
				record.setType(type);
				record.setLabel((String)m.get("PROPERTY_TITLE"));
				record.setModel((String) m.get("BIND_MODEL"));
				record.setRules(jsonArray);
				options = new Options();
				record.setOptions(options);
				options.setDynamic(false);
				options.setDynamicKey("");
				options.setOptions(JSON.parseArray((String) m.get("OPTIONS")));
				break;
		}
		return record;
	}
	
	
}
