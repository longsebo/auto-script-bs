package com.autoscript.service;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;
import com.autoscript.vo.KnowledgeBaseVo;
/**
 * 知识库服务
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-24
 */
@Service
public class KnowledgeBaseService {
	private static Logger logger=LoggerFactory.getLogger(KnowledgeBaseService.class);
	@Autowired
	private PublicService publicService;
	/**
	 * 表名
	 */
	private String TABLE_NAME="AS_KNOWLEDGE_BASE";

	/**
	 * 统计记录数
	 * @param searchMap
	 * @return
	 */
	public long count(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().count(getDBSesion(), TABLE_NAME, searchMap);
	}

	/**
	 *  插入
	 * @param qrCodeType
	 * @throws Exception 
	 */
	public void insert(KnowledgeBaseVo knowledgeBaseVo) throws Exception {
		Map<String, Object> data;
		data = new HashMap<String,Object>();
		//获取序列id
		if(StringHelper.isEmpty(knowledgeBaseVo.getId())){
			data.put("ID",publicService.getIds(TABLE_NAME, 1)[0]);
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getId())){
			data.put("ID",knowledgeBaseVo.getId());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getKbType())){
			data.put("KB_TYPE",knowledgeBaseVo.getKbType());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getKbKeyword())){
			data.put("KB_KEYWORD",knowledgeBaseVo.getKbKeyword());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getKbGrammar())){
			data.put("KB_GRAMMAR",knowledgeBaseVo.getKbGrammar());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getDesc())){
			data.put("DESC",knowledgeBaseVo.getDesc());
		}
		int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
		if(rowNum!=1) {
			logger.error("插入知识库失败！数据为:"+data+",rowNum:"+rowNum);
			throw new Exception("插入知识库失败！");
		}
	}
	
	/**
	 * 获取db会话
	 * @return
	 */
	private String getDBSesion() {
		return DBSessionService.getSingleInstance().getSession();
	}
	/**
	 * 更新知识库
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int update(KnowledgeBaseVo knowledgeBaseVo) throws Exception{
		Map<String, Object> searchMap;
		Map<String, Object> data;

		if(StringHelper.isEmpty(knowledgeBaseVo.getId()))
			throw new Exception("知识库id为空!");
		searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",knowledgeBaseVo.getId());
		data = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(knowledgeBaseVo.getId())){
			data.put("ID",knowledgeBaseVo.getId());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getKbType())){
			data.put("KB_TYPE",knowledgeBaseVo.getKbType());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getKbKeyword())){
			data.put("KB_KEYWORD",knowledgeBaseVo.getKbKeyword());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getKbGrammar())){
			data.put("KB_GRAMMAR",knowledgeBaseVo.getKbGrammar());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getDesc())){
			data.put("DESC",knowledgeBaseVo.getDesc());
		}
		int rowNum = SecurityDBClientService.getInstance().update(getDBSesion(), TABLE_NAME, searchMap, data);
		if(rowNum<1) {
			logger.error("更新知识库失败!数据为:searchMap:"+searchMap+",data:"+data+",rowNum:"+rowNum);
			throw new Exception("更新知识库失败!");
		}
		return rowNum;
	}
	/**
	 * 删除知识库
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int delete(KnowledgeBaseVo knowledgeBaseVo) throws Exception{
		Map<String,Object> searchMap = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(knowledgeBaseVo.getId())){
			searchMap.put("ID",knowledgeBaseVo.getId());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getKbType())){
			searchMap.put("KB_TYPE",knowledgeBaseVo.getKbType());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getKbKeyword())){
			searchMap.put("KB_KEYWORD",knowledgeBaseVo.getKbKeyword());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getKbGrammar())){
			searchMap.put("KB_GRAMMAR",knowledgeBaseVo.getKbGrammar());
		}
		if(!StringHelper.isEmpty(knowledgeBaseVo.getDesc())){
			searchMap.put("DESC",knowledgeBaseVo.getDesc());
		}
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
		if(rowNum<1) {
			logger.error("删除知识库失败,参数为:"+searchMap+",rowNum:"+rowNum);
			throw new Exception("删除知识库失败!");
		}
		return rowNum;		
	}

	/**
	 * 查询知识库
	 * @param searchMap 查询条件
	 * @param pageNo 页号
	 * @param pageSize 页大小
	 * @return
	 */
	public List<Map> listPage(Map<String, Object> searchMap, int pageNo, int pageSize) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap, pageNo, pageSize);
	}
	/**
	 * 查询知识库
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<Map> search(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
	}
	/**
	 * 根据id查询知识库
	 * @param id id标识
	 * @return
	 */
	public Map getById(String id) throws Exception {
		if(StringHelper.isEmpty(id))
			throw new Exception("知识库id为空!");
		Map<String,Object> searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",id);
		List<Map> resultList =  SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
 		if(resultList==null||resultList.size()==0){
 		  return null;
 		}else{
 		  return resultList.get(0);
 		}
	}
	
}
