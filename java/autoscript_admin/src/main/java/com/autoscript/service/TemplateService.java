package com.autoscript.service;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;
import com.autoscript.vo.TemplateVo;
import org.springframework.util.CollectionUtils;

/**
 * 模板表服务
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-02
 */
@Service
public class TemplateService {
	private static final String PROJECT_TABLE_NAME = "AS_PROJECT";
	private static Logger logger=LoggerFactory.getLogger(TemplateService.class);
	@Autowired
	private PublicService publicService;
	/**
	 * 表名
	 */
	public static final String TABLE_NAME="AS_TEMPLATE";

	/**
	 * 统计记录数
	 * @param searchMap
	 * @return
	 */
	public long count(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().count(getDBSesion(), TABLE_NAME, searchMap);
	}

	/**
	 *  插入
	 * @param templateVo
	 * @throws Exception
	 */
	public void insert(TemplateVo templateVo) throws Exception {
		Map<String, Object> data;
		data = new HashMap<String,Object>();
		//获取序列id
		if(StringHelper.isEmpty(templateVo.getId())){
			data.put("ID",publicService.getIds(TABLE_NAME, 1)[0]);
		}
		if(!StringHelper.isEmpty(templateVo.getId())){
			data.put("ID",templateVo.getId());
		}
		if(!StringHelper.isEmpty(templateVo.getProjectId())){
			data.put("PROJECT_ID",templateVo.getProjectId());
		}
		if(!StringHelper.isEmpty(templateVo.getTemplateName())){
			data.put("TEMPLATE_NAME",templateVo.getTemplateName());
		}
		if(!StringHelper.isEmpty(templateVo.getRemark())){
			data.put("REMARK",templateVo.getRemark());
		}
		if(!StringHelper.isEmpty(templateVo.getTemplateContent())){
			data.put("TEMPLATE_CONTENT",templateVo.getTemplateContent());
		}
		if(!StringHelper.isEmpty(templateVo.getIsEnable())){
			data.put("IS_ENABLE",templateVo.getIsEnable());
		}
		if(!StringHelper.isEmpty(templateVo.getCreateDate())){
			data.put("CREATE_DATE",templateVo.getCreateDate());
		}
		if(!StringHelper.isEmpty(templateVo.getUpdateDate())){
			data.put("UPDATE_DATE",templateVo.getUpdateDate());
		}
		int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
		if(rowNum!=1) {
			logger.error("插入模板表失败！数据为:"+data+",rowNum:"+rowNum);
			throw new Exception("插入模板表失败！");
		}
	}

	/**
	 * 获取db会话
	 * @return
	 */
	private String getDBSesion() {
		return DBSessionService.getSingleInstance().getSession();
	}
	/**
	 * 更新模板表
	 * @param templateVo
	 * @return
	 * @throws Exception
	 */
	public int update(TemplateVo templateVo) throws Exception{
		Map<String, Object> searchMap;
		Map<String, Object> data;

		if(StringHelper.isEmpty(templateVo.getId()))
			throw new Exception("模板表id为空!");
		searchMap = new HashMap<String,Object>();

		searchMap.put("ID",templateVo.getId());
		data = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(templateVo.getId())){
			data.put("ID",templateVo.getId());
		}
		if(!StringHelper.isEmpty(templateVo.getProjectId())){
			data.put("PROJECT_ID",templateVo.getProjectId());
		}
		if(!StringHelper.isEmpty(templateVo.getTemplateName())){
			data.put("TEMPLATE_NAME",templateVo.getTemplateName());
		}
		if(!StringHelper.isEmpty(templateVo.getRemark())){
			data.put("REMARK",templateVo.getRemark());
		}
		if(!StringHelper.isEmpty(templateVo.getTemplateContent())){
			data.put("TEMPLATE_CONTENT",templateVo.getTemplateContent());
		}
		if(!StringHelper.isEmpty(templateVo.getIsEnable())){
			data.put("IS_ENABLE",templateVo.getIsEnable());
		}
		if(!StringHelper.isEmpty(templateVo.getCreateDate())){
			data.put("CREATE_DATE",templateVo.getCreateDate());
		}
		if(!StringHelper.isEmpty(templateVo.getUpdateDate())){
			data.put("UPDATE_DATE",templateVo.getUpdateDate());
		}
		int rowNum = SecurityDBClientService.getInstance().update(getDBSesion(), TABLE_NAME, searchMap, data);
		if(rowNum<1) {
			logger.error("更新模板表失败!数据为:searchMap:"+searchMap+",data:"+data+",rowNum:"+rowNum);
			throw new Exception("更新模板表失败!");
		}
		return rowNum;
	}
	/**
	 * 删除模板表
	 * @param templateVo
	 * @return
	 * @throws Exception
	 */
	public int delete(TemplateVo templateVo) throws Exception{
		Map<String,Object> searchMap = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(templateVo.getId())){
			searchMap.put("ID",templateVo.getId());
		}
		if(!StringHelper.isEmpty(templateVo.getProjectId())){
			searchMap.put("PROJECT_ID",templateVo.getProjectId());
		}
		if(!StringHelper.isEmpty(templateVo.getTemplateName())){
			searchMap.put("TEMPLATE_NAME",templateVo.getTemplateName());
		}
		if(!StringHelper.isEmpty(templateVo.getRemark())){
			searchMap.put("REMARK",templateVo.getRemark());
		}
		if(!StringHelper.isEmpty(templateVo.getTemplateContent())){
			searchMap.put("TEMPLATE_CONTENT",templateVo.getTemplateContent());
		}
		if(!StringHelper.isEmpty(templateVo.getIsEnable())){
			searchMap.put("IS_ENABLE",templateVo.getIsEnable());
		}
		if(!StringHelper.isEmpty(templateVo.getCreateDate())){
			searchMap.put("CREATE_DATE",templateVo.getCreateDate());
		}
		if(!StringHelper.isEmpty(templateVo.getUpdateDate())){
			searchMap.put("UPDATE_DATE",templateVo.getUpdateDate());
		}
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
		if(rowNum<1) {
			logger.error("删除模板表失败,参数为:"+searchMap+",rowNum:"+rowNum);
			throw new Exception("删除模板表失败!");
		}
		return rowNum;
	}

	/**
	 * 查询模板表
	 * @param searchMap 查询条件
	 * @param pageNo 页号
	 * @param pageSize 页大小
	 * @return
	 */
	public List<Map> listPage(Map<String, Object> searchMap, int pageNo, int pageSize) {
		return SecurityDBClientService.getInstance().search(getDBSesion(),
				TABLE_NAME, searchMap, pageNo, pageSize);
	}
	/**
	 * 查询模板表
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<Map> search(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().search(getDBSesion(),
				TABLE_NAME, searchMap);
	}
	/**
	 * 根据id查询模板表
	 * @param id id标识
	 * @return
	 */
	public Map getById(String id) throws Exception {
		if(StringHelper.isEmpty(id))
			throw new Exception("模板表id为空!");
		Map<String,Object> searchMap = new HashMap<String,Object>();

		searchMap.put("ID",id);
		List<Map> resultList =  SecurityDBClientService.getInstance().search(getDBSesion(),
				TABLE_NAME, searchMap);
 		if(resultList==null||resultList.size()==0){
 		  return null;
 		}else{
 		  //查询项目名称返回前端
			searchMap.clear();
			Map map = resultList.get(0);
			searchMap.put("ID", map.get("PROJECT_ID"));
			List<Map> projectList = SecurityDBClientService.getInstance().search(getDBSesion(),
					PROJECT_TABLE_NAME, searchMap);
			if(!CollectionUtils.isEmpty(projectList)){
				map.put("PROJECT_NAME",projectList.get(0).get("PROJECT_NAME"));
			}else{
				map.put("PROJECT_NAME","");
			}
 		    return map;
 		}
	}

	/**
	 *  根据工程id删除工程模版
	 * @param projectId
	 */
	public void deleteByProjectId(String projectId) throws Exception {
		if(StringHelper.isEmpty(projectId))
			throw new Exception("缺少工程id");
		Map<String, Object> searchMap = new HashMap<>();
		searchMap.put("PROJECT_ID",projectId);
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
//		if(rowNum<1) {
//			logger.error("删除工程模版失败,参数为:"+searchMap+",rowNum:"+rowNum);
//			throw new Exception("删除工程模版失败!");
//		}
	}

	/**
	 * 批量插入
	 * @param templateVos
	 */
	public void insertBatch(List<HashMap> templateVos) throws Exception {
		for(Map data:templateVos) {
			int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
			if (rowNum != 1) {
				logger.error("插入工程模版失败！数据为:" + data + ",rowNum:" + rowNum);
				throw new Exception("插入工程模版失败！");
			}
		}
	}
}
