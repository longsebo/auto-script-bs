package com.autoscript.service;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;
import com.autoscript.vo.MenuVo;
/**
 * 菜单定义服务
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-22
 */
@Service
public class MenuService {
	private static Logger logger=LoggerFactory.getLogger(MenuService.class);
	@Autowired
	private PublicService publicService;
	/**
	 * 表名
	 */
	private String TABLE_NAME="AS_MENU";

	/**
	 * 统计记录数
	 * @param searchMap
	 * @return
	 */
	public long count(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().count(getDBSesion(), TABLE_NAME, searchMap);
	}

	/**
	 *  插入
	 * @param qrCodeType
	 * @throws Exception 
	 */
	public void insert(MenuVo menuVo) throws Exception {
		Map<String, Object> data;
		data = new HashMap<String,Object>();
		//获取序列id
		if(StringHelper.isEmpty(menuVo.getId())){
			data.put("ID",publicService.getIds(TABLE_NAME, 1)[0]);
		}
		if(!StringHelper.isEmpty(menuVo.getId())){
			data.put("ID",menuVo.getId());
		}
		if(!StringHelper.isEmpty(menuVo.getMenuName())){
			data.put("MENU_NAME",menuVo.getMenuName());
		}
		if(!StringHelper.isEmpty(menuVo.getMenuCode())){
			data.put("MENU_CODE",menuVo.getMenuCode());
		}
		if(!StringHelper.isEmpty(menuVo.getMenuLink())){
			data.put("MENU_LINK",menuVo.getMenuLink());
		}
		int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
		if(rowNum!=1) {
			logger.error("插入菜单定义失败！数据为:"+data+",rowNum:"+rowNum);
			throw new Exception("插入菜单定义失败！");
		}
	}
	
	/**
	 * 获取db会话
	 * @return
	 */
	private String getDBSesion() {
		return DBSessionService.getSingleInstance().getSession();
	}
	/**
	 * 更新菜单定义
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int update(MenuVo menuVo) throws Exception{
		Map<String, Object> searchMap;
		Map<String, Object> data;

		if(StringHelper.isEmpty(menuVo.getId()))
			throw new Exception("菜单定义id为空!");
		searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",menuVo.getId());
		data = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(menuVo.getId())){
			data.put("ID",menuVo.getId());
		}
		if(!StringHelper.isEmpty(menuVo.getMenuName())){
			data.put("MENU_NAME",menuVo.getMenuName());
		}
		if(!StringHelper.isEmpty(menuVo.getMenuCode())){
			data.put("MENU_CODE",menuVo.getMenuCode());
		}
		if(!StringHelper.isEmpty(menuVo.getMenuLink())){
			data.put("MENU_LINK",menuVo.getMenuLink());
		}
		int rowNum = SecurityDBClientService.getInstance().update(getDBSesion(), TABLE_NAME, searchMap, data);
		if(rowNum<1) {
			logger.error("更新菜单定义失败!数据为:searchMap:"+searchMap+",data:"+data+",rowNum:"+rowNum);
			throw new Exception("更新菜单定义失败!");
		}
		return rowNum;
	}
	/**
	 * 删除菜单定义
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int delete(MenuVo menuVo) throws Exception{
		Map<String,Object> searchMap = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(menuVo.getId())){
			searchMap.put("ID",menuVo.getId());
		}
		if(!StringHelper.isEmpty(menuVo.getMenuName())){
			searchMap.put("MENU_NAME",menuVo.getMenuName());
		}
		if(!StringHelper.isEmpty(menuVo.getMenuCode())){
			searchMap.put("MENU_CODE",menuVo.getMenuCode());
		}
		if(!StringHelper.isEmpty(menuVo.getMenuLink())){
			searchMap.put("MENU_LINK",menuVo.getMenuLink());
		}
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
		if(rowNum<1) {
			logger.error("删除菜单定义失败,参数为:"+searchMap+",rowNum:"+rowNum);
			throw new Exception("删除菜单定义失败!");
		}
		return rowNum;		
	}

	/**
	 * 查询菜单定义
	 * @param searchMap 查询条件
	 * @param pageNo 页号
	 * @param pageSize 页大小
	 * @return
	 */
	public List<Map> listPage(Map<String, Object> searchMap, int pageNo, int pageSize) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap, pageNo, pageSize);
	}
	/**
	 * 查询菜单定义
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<Map> search(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
	}
	/**
	 * 根据id查询菜单定义
	 * @param id id标识
	 * @return
	 */
	public Map getById(String id) throws Exception {
		if(StringHelper.isEmpty(id))
			throw new Exception("菜单定义id为空!");
		Map<String,Object> searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",id);
		List<Map> resultList =  SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
 		if(resultList==null||resultList.size()==0){
 		  return null;
 		}else{
 		  return resultList.get(0);
 		}
	}
	
}
