package com.autoscript.service;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;
import com.autoscript.vo.ProjectParameterVo;
/**
 * 工程参数服务
 * @author lsb
 * @version 1.0.0
 * @date 2021-06-25
 */
@Service
public class ProjectParameterService extends ControlPropertiesService {
	private static Logger logger=LoggerFactory.getLogger(ProjectParameterService.class);
	@Autowired
	private PublicService publicService;
	/**
	 * 表名
	 */
	public static final String TABLE_NAME="AS_PROJECT_PARAMETER";

	/**
	 * 统计记录数
	 * @param searchMap
	 * @return
	 */
	public long count(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().count(getDBSesion(), TABLE_NAME, searchMap);
	}

	/**
	 *  插入
	 * @param qrCodeType
	 * @throws Exception 
	 */
	public void insert(ProjectParameterVo projectParameterVo) throws Exception {
		Map<String, Object> data;
		data = new HashMap<String,Object>();
		//获取序列id
		if(StringHelper.isEmpty(projectParameterVo.getId())){
			data.put("ID",publicService.getIds(TABLE_NAME, 1)[0]);
		}
		if(!StringHelper.isEmpty(projectParameterVo.getId())){
			data.put("ID",projectParameterVo.getId());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getProjectId())){
			data.put("PROJECT_ID",projectParameterVo.getProjectId());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPropertyType())){
			data.put("PROPERTY_TYPE",projectParameterVo.getPropertyType());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPropertyTitle())){
			data.put("PROPERTY_TITLE",projectParameterVo.getPropertyTitle());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getWidth())){
			data.put("WIDTH",projectParameterVo.getWidth());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getRules())){
			data.put("RULES",projectParameterVo.getRules());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getOptions())){
			data.put("OPTIONS",projectParameterVo.getOptions());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getBindModel())){
			data.put("BIND_MODEL",projectParameterVo.getBindModel());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPlaceholder())){
			data.put("PLACEHOLDER",projectParameterVo.getPlaceholder());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMaxLength())){
			data.put("MAX_LENGTH",projectParameterVo.getMaxLength());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getStyle())){
			data.put("STYLE",projectParameterVo.getStyle());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getInitVal())){
			data.put("INIT_VAL",projectParameterVo.getInitVal());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMaxVal())){
			data.put("MAX_VAL",projectParameterVo.getMaxVal());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMinVal())){
			data.put("MIN_VAL",projectParameterVo.getMinVal());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPrecision())){
			data.put("PRECISION",projectParameterVo.getPrecision());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getStep())){
			data.put("STEP",projectParameterVo.getStep());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMaxRows())){
			data.put("MAX_ROWS",projectParameterVo.getMaxRows());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMinRows())){
			data.put("MIN_ROWS",projectParameterVo.getMinRows());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getInitRowNum())){
			data.put("INIT_ROW_NUM",projectParameterVo.getInitRowNum());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getInitColumnNum())){
			data.put("INIT_COLUMN_NUM",projectParameterVo.getInitColumnNum());
		}
		int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
		if(rowNum!=1) {
			logger.error("插入工程参数失败！数据为:"+data+",rowNum:"+rowNum);
			throw new Exception("插入工程参数失败！");
		}
	}
	
	/**
	 * 获取db会话
	 * @return
	 */
	private String getDBSesion() {
		return DBSessionService.getSingleInstance().getSession();
	}
	/**
	 * 更新工程参数
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int update(ProjectParameterVo projectParameterVo) throws Exception{
		Map<String, Object> searchMap;
		Map<String, Object> data;

		if(StringHelper.isEmpty(projectParameterVo.getId()))
			throw new Exception("工程参数id为空!");
		searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",projectParameterVo.getId());
		data = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(projectParameterVo.getId())){
			data.put("ID",projectParameterVo.getId());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getProjectId())){
			data.put("PROJECT_ID",projectParameterVo.getProjectId());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPropertyType())){
			data.put("PROPERTY_TYPE",projectParameterVo.getPropertyType());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPropertyTitle())){
			data.put("PROPERTY_TITLE",projectParameterVo.getPropertyTitle());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getWidth())){
			data.put("WIDTH",projectParameterVo.getWidth());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getRules())){
			data.put("RULES",projectParameterVo.getRules());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getOptions())){
			data.put("OPTIONS",projectParameterVo.getOptions());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getBindModel())){
			data.put("BIND_MODEL",projectParameterVo.getBindModel());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPlaceholder())){
			data.put("PLACEHOLDER",projectParameterVo.getPlaceholder());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMaxLength())){
			data.put("MAX_LENGTH",projectParameterVo.getMaxLength());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getStyle())){
			data.put("STYLE",projectParameterVo.getStyle());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getInitVal())){
			data.put("INIT_VAL",projectParameterVo.getInitVal());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMaxVal())){
			data.put("MAX_VAL",projectParameterVo.getMaxVal());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMinVal())){
			data.put("MIN_VAL",projectParameterVo.getMinVal());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPrecision())){
			data.put("PRECISION",projectParameterVo.getPrecision());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getStep())){
			data.put("STEP",projectParameterVo.getStep());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMaxRows())){
			data.put("MAX_ROWS",projectParameterVo.getMaxRows());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMinRows())){
			data.put("MIN_ROWS",projectParameterVo.getMinRows());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getInitRowNum())){
			data.put("INIT_ROW_NUM",projectParameterVo.getInitRowNum());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getInitColumnNum())){
			data.put("INIT_COLUMN_NUM",projectParameterVo.getInitColumnNum());
		}
		int rowNum = SecurityDBClientService.getInstance().update(getDBSesion(), TABLE_NAME, searchMap, data);
		if(rowNum<1) {
			logger.error("更新工程参数失败!数据为:searchMap:"+searchMap+",data:"+data+",rowNum:"+rowNum);
			throw new Exception("更新工程参数失败!");
		}
		return rowNum;
	}
	/**
	 * 删除工程参数
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int delete(ProjectParameterVo projectParameterVo) throws Exception{
		Map<String,Object> searchMap = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(projectParameterVo.getId())){
			searchMap.put("ID",projectParameterVo.getId());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getProjectId())){
			searchMap.put("PROJECT_ID",projectParameterVo.getProjectId());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPropertyType())){
			searchMap.put("PROPERTY_TYPE",projectParameterVo.getPropertyType());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPropertyTitle())){
			searchMap.put("PROPERTY_TITLE",projectParameterVo.getPropertyTitle());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getWidth())){
			searchMap.put("WIDTH",projectParameterVo.getWidth());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getRules())){
			searchMap.put("RULES",projectParameterVo.getRules());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getOptions())){
			searchMap.put("OPTIONS",projectParameterVo.getOptions());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getBindModel())){
			searchMap.put("BIND_MODEL",projectParameterVo.getBindModel());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPlaceholder())){
			searchMap.put("PLACEHOLDER",projectParameterVo.getPlaceholder());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMaxLength())){
			searchMap.put("MAX_LENGTH",projectParameterVo.getMaxLength());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getStyle())){
			searchMap.put("STYLE",projectParameterVo.getStyle());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getInitVal())){
			searchMap.put("INIT_VAL",projectParameterVo.getInitVal());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMaxVal())){
			searchMap.put("MAX_VAL",projectParameterVo.getMaxVal());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMinVal())){
			searchMap.put("MIN_VAL",projectParameterVo.getMinVal());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getPrecision())){
			searchMap.put("PRECISION",projectParameterVo.getPrecision());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getStep())){
			searchMap.put("STEP",projectParameterVo.getStep());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMaxRows())){
			searchMap.put("MAX_ROWS",projectParameterVo.getMaxRows());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getMinRows())){
			searchMap.put("MIN_ROWS",projectParameterVo.getMinRows());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getInitRowNum())){
			searchMap.put("INIT_ROW_NUM",projectParameterVo.getInitRowNum());
		}
		if(!StringHelper.isEmpty(projectParameterVo.getInitColumnNum())){
			searchMap.put("INIT_COLUMN_NUM",projectParameterVo.getInitColumnNum());
		}
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
		if(rowNum<1) {
			logger.error("删除工程参数失败,参数为:"+searchMap+",rowNum:"+rowNum);
			throw new Exception("删除工程参数失败!");
		}
		return rowNum;		
	}

	/**
	 * 查询工程参数
	 * @param searchMap 查询条件
	 * @param pageNo 页号
	 * @param pageSize 页大小
	 * @return
	 */
	public List<Map> listPage(Map<String, Object> searchMap, int pageNo, int pageSize) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap, pageNo, pageSize);
	}
	/**
	 * 查询工程参数
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<Map> search(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
	}
	/**
	 * 根据id查询工程参数
	 * @param id id标识
	 * @return
	 */
	public Map getById(String id) throws Exception {
		if(StringHelper.isEmpty(id))
			throw new Exception("工程参数id为空!");
		Map<String,Object> searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",id);
		List<Map> resultList =  SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
 		if(resultList==null||resultList.size()==0){
 		  return null;
 		}else{
 		  return resultList.get(0);
 		}
	}

	/**
	 *  根据工程id删除工程参数
	 * @param projectId
	 */
    public void deleteByProjectId(String projectId) throws Exception {
		if(StringHelper.isEmpty(projectId))
			throw new Exception("缺少工程id");
		Map<String, Object> searchMap = new HashMap<>();
		searchMap.put("PROJECT_ID",projectId);
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
//		if(rowNum<1) {
//			logger.error("删除工程参数失败,参数为:"+searchMap+",rowNum:"+rowNum);
//			throw new Exception("删除工程参数失败!");
//		}
    }

	/**
	 * 批量插入
	 * @param projectParameterVos
	 */
	public void insertBatch(List<HashMap> projectParameterVos) throws Exception {
		for(Map data:projectParameterVos) {
			int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
			if (rowNum != 1) {
				logger.error("插入工程参数失败！数据为:" + data + ",rowNum:" + rowNum);
				throw new Exception("插入工程参数失败！");
			}
		}
	}
}
