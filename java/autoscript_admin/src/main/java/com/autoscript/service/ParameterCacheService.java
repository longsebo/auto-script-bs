/**
 * 
 */
package com.autoscript.service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * 参数缓存服务
 */
public class ParameterCacheService {
	private static ParameterCacheService parameterCacheService;
    private Map<String,Object> cacheMap ;

	public static synchronized ParameterCacheService getSingleInstance() {
		if(parameterCacheService ==null) {
			parameterCacheService = new ParameterCacheService();
		}
		return parameterCacheService;
	}
	private ParameterCacheService(){
		cacheMap = new HashMap<>();
	}

	/**
	 * 放入缓存
	 * @param key
	 * @param value
	 */
	public void put(String key,Object value){
		cacheMap.put(key,value);
	}

	/**
	 * 从缓存获取
	 * @param key
	 * @return
	 */
	public Object get(String key){
		return cacheMap.get(key);
	}
	/**
	 * 从缓存获取字符串value
	 * @param key
	 * @return
	 */
	public String getString(String key){
		return (String)cacheMap.get(key);
	}

}
