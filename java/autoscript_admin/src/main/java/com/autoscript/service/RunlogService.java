package com.autoscript.service;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;
import com.autoscript.vo.RunlogVo;
/**
 * 运行日志表服务
 * @author lsb
 * @version 1.0.0
 * @date 2021-06-28
 */
@Service
public class RunlogService {
	private static Logger logger=LoggerFactory.getLogger(RunlogService.class);
	@Autowired
	private PublicService publicService;
	/**
	 * 表名
	 */
	public static final String TABLE_NAME="AS_RUNLOG";

	/**
	 * 统计记录数
	 * @param searchMap
	 * @return
	 */
	public long count(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().count(getDBSesion(), TABLE_NAME, searchMap);
	}

	/**
	 *  插入
	 * @param qrCodeType
	 * @throws Exception
	 */
	public Map<String, Object> insert(RunlogVo runlogVo) throws Exception {
		Map<String, Object> data;
		data = new HashMap<String,Object>();
		//获取序列id
		if(StringHelper.isEmpty(runlogVo.getId())){
			data.put("ID",publicService.getIds(TABLE_NAME, 1)[0]);
		}
		if(!StringHelper.isEmpty(runlogVo.getId())){
			data.put("ID",runlogVo.getId());
		}
		if(!StringHelper.isEmpty(runlogVo.getProjectId())){
			data.put("PROJECT_ID",runlogVo.getProjectId());
		}
		if(!StringHelper.isEmpty(runlogVo.getLogCode())){
			data.put("LOG_CODE",runlogVo.getLogCode());
		}
		if(!StringHelper.isEmpty(runlogVo.getProjectParameter())){
			data.put("PROJECT_PARAMETER",runlogVo.getProjectParameter());
		}
		if(!StringHelper.isEmpty(runlogVo.getSourceData())){
			data.put("SOURCE_DATA",runlogVo.getSourceData());
		}
		if(!StringHelper.isEmpty(runlogVo.getStartTime())){
			data.put("START_TIME",runlogVo.getStartTime());
		}
		if(!StringHelper.isEmpty(runlogVo.getEndTime())){
			data.put("END_TIME",runlogVo.getEndTime());
		}
		if(!StringHelper.isEmpty(runlogVo.getResultFile())){
			data.put("RESULT_FILE",runlogVo.getResultFile());
		}
		if(!StringHelper.isEmpty(runlogVo.getRunUserId())){
			data.put("RUN_USER_ID",runlogVo.getRunUserId());
		}
		if(!StringHelper.isEmpty(runlogVo.getSuccessFailFlag())){
			data.put("SUCCESS_FAIL_FLAG",runlogVo.getSuccessFailFlag());
		}
		if(!StringHelper.isEmpty(runlogVo.getErrorMsg())){
			data.put("ERROR_MSG",runlogVo.getErrorMsg());
		}
		int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
		if(rowNum!=1) {
			logger.error("插入运行日志表失败！数据为:"+data+",rowNum:"+rowNum);
			throw new Exception("插入运行日志表失败！");
		}
		return data;
	}

	/**
	 * 获取db会话
	 * @return
	 */
	private String getDBSesion() {
		return DBSessionService.getSingleInstance().getSession();
	}
	/**
	 * 更新运行日志表
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int update(RunlogVo runlogVo) throws Exception{
		Map<String, Object> searchMap;
		Map<String, Object> data;

		if(StringHelper.isEmpty(runlogVo.getId()))
			throw new Exception("运行日志表id为空!");
		searchMap = new HashMap<String,Object>();

		searchMap.put("ID",runlogVo.getId());
		data = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(runlogVo.getId())){
			data.put("ID",runlogVo.getId());
		}
		if(!StringHelper.isEmpty(runlogVo.getProjectId())){
			data.put("PROJECT_ID",runlogVo.getProjectId());
		}
		if(!StringHelper.isEmpty(runlogVo.getLogCode())){
			data.put("LOG_CODE",runlogVo.getLogCode());
		}
		if(!StringHelper.isEmpty(runlogVo.getProjectParameter())){
			data.put("PROJECT_PARAMETER",runlogVo.getProjectParameter());
		}
		if(!StringHelper.isEmpty(runlogVo.getSourceData())){
			data.put("SOURCE_DATA",runlogVo.getSourceData());
		}
		if(!StringHelper.isEmpty(runlogVo.getStartTime())){
			data.put("START_TIME",runlogVo.getStartTime());
		}
		if(!StringHelper.isEmpty(runlogVo.getEndTime())){
			data.put("END_TIME",runlogVo.getEndTime());
		}
		if(!StringHelper.isEmpty(runlogVo.getResultFile())){
			data.put("RESULT_FILE",runlogVo.getResultFile());
		}
		if(!StringHelper.isEmpty(runlogVo.getRunUserId())){
			data.put("RUN_USER_ID",runlogVo.getRunUserId());
		}
		if(!StringHelper.isEmpty(runlogVo.getSuccessFailFlag())){
			data.put("SUCCESS_FAIL_FLAG",runlogVo.getSuccessFailFlag());
		}
		if(!StringHelper.isEmpty(runlogVo.getErrorMsg())){
			data.put("ERROR_MSG",runlogVo.getErrorMsg());
		}
		int rowNum = SecurityDBClientService.getInstance().update(getDBSesion(), TABLE_NAME, searchMap, data);
		if(rowNum<1) {
			logger.error("更新运行日志表失败!数据为:searchMap:"+searchMap+",data:"+data+",rowNum:"+rowNum);
			throw new Exception("更新运行日志表失败!");
		}
		return rowNum;
	}
	/**
	 * 删除运行日志表
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int delete(RunlogVo runlogVo) throws Exception{
		Map<String,Object> searchMap = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(runlogVo.getId())){
			searchMap.put("ID",runlogVo.getId());
		}
		if(!StringHelper.isEmpty(runlogVo.getProjectId())){
			searchMap.put("PROJECT_ID",runlogVo.getProjectId());
		}
		if(!StringHelper.isEmpty(runlogVo.getLogCode())){
			searchMap.put("LOG_CODE",runlogVo.getLogCode());
		}
		if(!StringHelper.isEmpty(runlogVo.getProjectParameter())){
			searchMap.put("PROJECT_PARAMETER",runlogVo.getProjectParameter());
		}
		if(!StringHelper.isEmpty(runlogVo.getSourceData())){
			searchMap.put("SOURCE_DATA",runlogVo.getSourceData());
		}
		if(!StringHelper.isEmpty(runlogVo.getStartTime())){
			searchMap.put("START_TIME",runlogVo.getStartTime());
		}
		if(!StringHelper.isEmpty(runlogVo.getEndTime())){
			searchMap.put("END_TIME",runlogVo.getEndTime());
		}
		if(!StringHelper.isEmpty(runlogVo.getResultFile())){
			searchMap.put("RESULT_FILE",runlogVo.getResultFile());
		}
		if(!StringHelper.isEmpty(runlogVo.getRunUserId())){
			searchMap.put("RUN_USER_ID",runlogVo.getRunUserId());
		}
		if(!StringHelper.isEmpty(runlogVo.getSuccessFailFlag())){
			searchMap.put("SUCCESS_FAIL_FLAG",runlogVo.getSuccessFailFlag());
		}
		if(!StringHelper.isEmpty(runlogVo.getErrorMsg())){
			searchMap.put("ERROR_MSG",runlogVo.getErrorMsg());
		}
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
		if(rowNum<1) {
			logger.error("删除运行日志表失败,参数为:"+searchMap+",rowNum:"+rowNum);
			throw new Exception("删除运行日志表失败!");
		}
		return rowNum;
	}

	/**
	 * 查询运行日志表
	 * @param searchMap 查询条件
	 * @param pageNo 页号
	 * @param pageSize 页大小
	 * @return
	 */
	public List<Map> listPage(Map<String, Object> searchMap, int pageNo, int pageSize) {
		List<Map> retList;
		retList =  SecurityDBClientService.getInstance().search(getDBSesion(),
				TABLE_NAME, searchMap, pageNo, pageSize);
		//循环把源数据设置为空串
//		for(Map m:retList){
//			m.put("SOURCE_DATA","");
//		}
		return retList;
	}
	/**
	 * 查询运行日志表
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<Map> search(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().search(getDBSesion(),
				TABLE_NAME, searchMap);
	}
	/**
	 * 根据id查询运行日志表
	 * @param id id标识
	 * @return
	 */
	public Map getById(String id) throws Exception {
		if(StringHelper.isEmpty(id))
			throw new Exception("运行日志表id为空!");
		Map<String,Object> searchMap = new HashMap<String,Object>();

		searchMap.put("ID",id);
		List<Map> resultList =  SecurityDBClientService.getInstance().search(getDBSesion(),
				TABLE_NAME, searchMap);
		if(resultList==null||resultList.size()==0){
			return null;
		}else{
			return resultList.get(0);
		}
	}

}
