package com.autoscript.service;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;
import com.autoscript.vo.ConfigVo;
/**
 * 配置表服务
 * @author lsb
 * @version 1.0.0
 * @date 2021-02-02
 */
@Service
public class ConfigService {
	private static Logger logger=LoggerFactory.getLogger(ConfigService.class);
	@Autowired
	private PublicService publicService;
	/**
	 * 表名
	 */
	private String TABLE_NAME="AS_CONFIG";

	/**
	 * 统计记录数
	 * @param searchMap
	 * @return
	 */
	public long count(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().count(getDBSesion(), TABLE_NAME, searchMap);
	}

	/**
	 *  插入
	 * @param qrCodeType
	 * @throws Exception 
	 */
	public void insert(ConfigVo configVo) throws Exception {
		Map<String, Object> data;
		data = new HashMap<String,Object>();
		//获取序列id
		if(StringHelper.isEmpty(configVo.getId())){
			data.put("ID",publicService.getIds(TABLE_NAME, 1)[0]);
		}
		if(!StringHelper.isEmpty(configVo.getId())){
			data.put("ID",configVo.getId());
		}
		if(!StringHelper.isEmpty(configVo.getFunctionName())){
			data.put("FUNCTION_NAME",configVo.getFunctionName());
		}
		if(!StringHelper.isEmpty(configVo.getOperatorClass())){
			data.put("OPERATOR_CLASS",configVo.getOperatorClass());
		}
		if(!StringHelper.isEmpty(configVo.getFunctionDesc())){
			data.put("FUNCTION_DESC",configVo.getFunctionDesc());
		}
		int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
		if(rowNum!=1) {
			logger.error("插入配置表失败！数据为:"+data+",rowNum:"+rowNum);
			throw new Exception("插入配置表失败！");
		}
	}
	
	/**
	 * 获取db会话
	 * @return
	 */
	private String getDBSesion() {
		return DBSessionService.getSingleInstance().getSession();
	}
	/**
	 * 更新配置表
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int update(ConfigVo configVo) throws Exception{
		Map<String, Object> searchMap;
		Map<String, Object> data;

		if(StringHelper.isEmpty(configVo.getId()))
			throw new Exception("配置表id为空!");
		searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",configVo.getId());
		data = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(configVo.getId())){
			data.put("ID",configVo.getId());
		}
		if(!StringHelper.isEmpty(configVo.getFunctionName())){
			data.put("FUNCTION_NAME",configVo.getFunctionName());
		}
		if(!StringHelper.isEmpty(configVo.getOperatorClass())){
			data.put("OPERATOR_CLASS",configVo.getOperatorClass());
		}
		if(!StringHelper.isEmpty(configVo.getFunctionDesc())){
			data.put("FUNCTION_DESC",configVo.getFunctionDesc());
		}
		int rowNum = SecurityDBClientService.getInstance().update(getDBSesion(), TABLE_NAME, searchMap, data);
		if(rowNum<1) {
			logger.error("更新配置表失败!数据为:searchMap:"+searchMap+",data:"+data+",rowNum:"+rowNum);
			throw new Exception("更新配置表失败!");
		}
		return rowNum;
	}
	/**
	 * 删除配置表
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int delete(ConfigVo configVo) throws Exception{
		Map<String,Object> searchMap = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(configVo.getId())){
			searchMap.put("ID",configVo.getId());
		}
		if(!StringHelper.isEmpty(configVo.getFunctionName())){
			searchMap.put("FUNCTION_NAME",configVo.getFunctionName());
		}
		if(!StringHelper.isEmpty(configVo.getOperatorClass())){
			searchMap.put("OPERATOR_CLASS",configVo.getOperatorClass());
		}
		if(!StringHelper.isEmpty(configVo.getFunctionDesc())){
			searchMap.put("FUNCTION_DESC",configVo.getFunctionDesc());
		}
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
		if(rowNum<1) {
			logger.error("删除配置表失败,参数为:"+searchMap+",rowNum:"+rowNum);
			throw new Exception("删除配置表失败!");
		}
		return rowNum;		
	}

	/**
	 * 查询配置表
	 * @param searchMap 查询条件
	 * @param pageNo 页号
	 * @param pageSize 页大小
	 * @return
	 */
	public List<Map> listPage(Map<String, Object> searchMap, int pageNo, int pageSize) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap, pageNo, pageSize);
	}
	/**
	 * 查询配置表
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<Map> search(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
	}
	/**
	 * 根据id查询配置表
	 * @param id id标识
	 * @return
	 */
	public Map getById(String id) throws Exception {
		if(StringHelper.isEmpty(id))
			throw new Exception("配置表id为空!");
		Map<String,Object> searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",id);
		List<Map> resultList =  SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
 		if(resultList==null||resultList.size()==0){
 		  return null;
 		}else{
 		  return resultList.get(0);
 		}
	}
	
}
