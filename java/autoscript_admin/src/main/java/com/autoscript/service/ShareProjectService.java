package com.autoscript.service;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.autoscript.vo.BatchShareProjectVo;
import com.dfhc.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dfhc.securitydb.service.SecurityDBClientService;
import com.dfhc.util.StringHelper;
import com.autoscript.vo.ShareProjectVo;
/**
 * 分享工程服务
 * @author lsb
 * @version 1.0.0
 * @date 2021-06-25
 */
@Service
public class ShareProjectService {
	private static Logger logger=LoggerFactory.getLogger(ShareProjectService.class);
	@Autowired
	private PublicService publicService;
	@Autowired
	private UserService userService;
	/**
	 * 表名
	 */
	private String TABLE_NAME="AS_SHARE_PROJECT";

	/**
	 * 统计记录数
	 * @param searchMap
	 * @return
	 */
	public long count(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().count(getDBSesion(), TABLE_NAME, searchMap);
	}

	/**
	 *  插入
	 * @param shareProjectVo
	 * @throws Exception 
	 */
	public void insert(ShareProjectVo shareProjectVo) throws Exception {
		Map<String, Object> data;
		data = new HashMap<String,Object>();
		//获取序列id
		if(StringHelper.isEmpty(shareProjectVo.getId())){
			data.put("ID",publicService.getIds(TABLE_NAME, 1)[0]);
		}
		if(!StringHelper.isEmpty(shareProjectVo.getId())){
			data.put("ID",shareProjectVo.getId());
		}
		if(!StringHelper.isEmpty(shareProjectVo.getProjectId())){
			data.put("PROJECT_ID",shareProjectVo.getProjectId());
		}
		if(!StringHelper.isEmpty(shareProjectVo.getUserId())){
			data.put("USER_ID",shareProjectVo.getUserId());
		}
		if(!StringHelper.isEmpty(shareProjectVo.getShareTime())){
			data.put("SHARE_TIME",shareProjectVo.getShareTime());
		}else {
			data.put("SHARE_TIME", DateUtil.getNowDate("yyyy-MM-dd HH:mm:ss"));
		}
		int rowNum = SecurityDBClientService.getInstance().insert(getDBSesion(), TABLE_NAME, data);
		if(rowNum!=1) {
			logger.error("插入分享工程失败！数据为:"+data+",rowNum:"+rowNum);
			throw new Exception("插入分享工程失败！");
		}
	}
	
	/**
	 * 获取db会话
	 * @return
	 */
	private String getDBSesion() {
		return DBSessionService.getSingleInstance().getSession();
	}
	/**
	 * 更新分享工程
	 * @param shareProjectVo
	 * @return
	 * @throws Exception
	 */
	public int update(ShareProjectVo shareProjectVo) throws Exception{
		Map<String, Object> searchMap;
		Map<String, Object> data;

		if(StringHelper.isEmpty(shareProjectVo.getId()))
			throw new Exception("分享工程id为空!");
		searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",shareProjectVo.getId());
		data = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(shareProjectVo.getId())){
			data.put("ID",shareProjectVo.getId());
		}
		if(!StringHelper.isEmpty(shareProjectVo.getProjectId())){
			data.put("PROJECT_ID",shareProjectVo.getProjectId());
		}
		if(!StringHelper.isEmpty(shareProjectVo.getUserId())){
			data.put("USER_ID",shareProjectVo.getUserId());
		}
		if(!StringHelper.isEmpty(shareProjectVo.getShareTime())){
			data.put("SHARE_TIME",shareProjectVo.getShareTime());
		}
		int rowNum = SecurityDBClientService.getInstance().update(getDBSesion(), TABLE_NAME, searchMap, data);
		if(rowNum<1) {
			logger.error("更新分享工程失败!数据为:searchMap:"+searchMap+",data:"+data+",rowNum:"+rowNum);
			throw new Exception("更新分享工程失败!");
		}
		return rowNum;
	}
	/**
	 * 删除分享工程
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int delete(ShareProjectVo shareProjectVo) throws Exception{
		Map<String,Object> searchMap = new HashMap<String,Object>();
		if(!StringHelper.isEmpty(shareProjectVo.getId())){
			searchMap.put("ID",shareProjectVo.getId());
		}
		if(!StringHelper.isEmpty(shareProjectVo.getProjectId())){
			searchMap.put("PROJECT_ID",shareProjectVo.getProjectId());
		}
		if(!StringHelper.isEmpty(shareProjectVo.getUserId())){
			searchMap.put("USER_ID",shareProjectVo.getUserId());
		}
		if(!StringHelper.isEmpty(shareProjectVo.getShareTime())){
			searchMap.put("SHARE_TIME",shareProjectVo.getShareTime());
		}
		int rowNum = SecurityDBClientService.getInstance().delete(getDBSesion(), TABLE_NAME, searchMap);
		if(rowNum<1) {
			logger.error("删除分享工程失败,参数为:"+searchMap+",rowNum:"+rowNum);
			throw new Exception("删除分享工程失败!");
		}
		return rowNum;		
	}

	/**
	 * 查询分享工程
	 * @param searchMap 查询条件
	 * @param pageNo 页号
	 * @param pageSize 页大小
	 * @return
	 */
	public List<Map> listPage(Map<String, Object> searchMap, int pageNo, int pageSize) throws Exception {
		List<Map> retList;
		retList =  SecurityDBClientService.getInstance().search(getDBSesion(),
				TABLE_NAME, searchMap, pageNo, pageSize);
		//循环填充用户名
		Map<String,Object> userMap;
		int size = retList.size();
		for(int i=0;i<size;i++){
			Map<String,Object> m ;
			m = retList.get(i);
			userMap = userService.getById((String)m.get("USER_ID"));
			m.put("USER_NAME",userMap.get("USER_NAME"));
		}
		return retList;
	}
	/**
	 * 查询分享工程
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<Map> search(Map<String, Object> searchMap) {
		return SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
	}
	/**
	 * 根据id查询分享工程
	 * @param id id标识
	 * @return
	 */
	public Map getById(String id) throws Exception {
		if(StringHelper.isEmpty(id))
			throw new Exception("分享工程id为空!");
		Map<String,Object> searchMap = new HashMap<String,Object>();
		
		searchMap.put("ID",id);
		List<Map> resultList =  SecurityDBClientService.getInstance().search(getDBSesion(), 
				TABLE_NAME, searchMap);
 		if(resultList==null||resultList.size()==0){
 		  return null;
 		}else{
 		  return resultList.get(0);
 		}
	}

	/**
	 * 批量新增分享工程
	 * @param batchShareProjectVo
	 */
    public void batchInsert(BatchShareProjectVo batchShareProjectVo) throws Exception {
		String[] ids = publicService.getIds(TABLE_NAME, batchShareProjectVo.getUserIds().size());
		Map<String,Object> searchMap =new HashMap<>();
		for(int i=0;i<ids.length;i++){
			ShareProjectVo vo = new ShareProjectVo();
			vo.setId(ids[i]);
			vo.setProjectId(batchShareProjectVo.getProjectId());
			vo.setUserId(batchShareProjectVo.getUserIds().get(i));
			//判断是否存在
			searchMap.put("PROJECT_ID",batchShareProjectVo.getProjectId());
			searchMap.put("USER_ID",vo.getUserId());
			if(count(searchMap)==0) {
				insert(vo);
			}
		}
    }
}
