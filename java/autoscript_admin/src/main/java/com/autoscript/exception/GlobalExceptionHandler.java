/**
 * 
 */
package com.autoscript.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.autoscript.vo.ResponseResult;
/**
 * @author Administrator 全局异常铺捉类
 */
@CrossOrigin

@RestControllerAdvice
public class GlobalExceptionHandler {
	private static Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler

	public ResponseResult processException(Exception ex, HttpServletRequest request, HttpServletResponse response) {

		ex.printStackTrace();

		if (ex instanceof MissingServletRequestParameterException) {

			return new ResponseResult(400, ex);

		}

		
		return new ResponseResult(500, ex.getMessage());

	}

}
