/**
 * 
 */
package com.autoscript.exception;

/**
 * 部署运行期异常
 * @author 龙色波
 */
public class ASException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5911940609720822853L;
	public ASException(){
		super();
	}
	public ASException(String msg){
		super(msg);
	}
	public ASException(String msg, Throwable e){
		super(msg,e);
	}
	public ASException(Throwable e){
		super(e);
	}
}
