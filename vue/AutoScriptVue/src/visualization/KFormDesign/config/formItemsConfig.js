/*
 * author kcz
 * date 2019-11-20
 * description 表单控件项
 */
// 基础控件
export const basicsList = [
  {
    type: "project", // 表单类型
    label: "工程", // 标题文字
    //icon: "icon-write",
	icon: "icon-ucodergongcheng",
	img:require('@/img/project.png'),//在画布显示图片
    options: {
      type: "text",
      width: "100%", // 宽度
      defaultValue: "", // 默认值
      placeholder: "请输入", // 没有输入时，提示文字
      clearable: false,
      maxLength: null,
      hidden: false, // 是否隐藏，false显示，true隐藏
      disabled: false // 是否禁用，false不禁用，true禁用
    },
    model: "", // 数据字段
    key: "",
    help: "",
    prefix: "",
    suffix: "",
    rules: [
      //验证规则
      {
        required: false, // 必须填写
        message: "必填项"
      }
    ]
  },
  {
    type: "template", // 表单类型
    label: "模板", // 标题文字
    icon: "icon-edit",
	img:require('@/img/template.png'),//在画布显示图片
    options: {
      width: "100%", // 宽度
      minRows: 4,
      maxRows: 6,
      maxLength: null,
      defaultValue: "",
      clearable: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      disabled: false,
      placeholder: "请输入"
    },
    model: "", // 数据字段
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "line", // 表单类型
    label: "连接线", // 标题文字
    icon: "icon-edit",
	img:require('@/img/line.png'),//在画布显示图片
    options: {
      width: "100%", // 宽度
      minRows: 4,
      maxRows: 6,
      maxLength: null,
      defaultValue: "",
      clearable: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      disabled: false,
      placeholder: "请输入"
    },
    model: "", // 数据字段
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "tableloop", // 表单类型
    label: "表循环", // 标题文字
    icon: "icon-number",
	img:require('@/img/tabloop.png'),//在画布显示图片
    options: {
      width: "100%", // 宽度
      defaultValue: 0, // 默认值
      min: null, // 可输入最小值
      max: null, // 可输入最大值
      precision: null,
      step: 1, // 步长，点击加减按钮时候，加减多少
      hidden: false, // 是否隐藏，false显示，true隐藏
      disabled: false, //是否禁用
      placeholder: "请输入"
    },
    model: "", // 数据字段
    key: "",
    help: "",
    prefix: "",
    suffix: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "fieldloop", // 表单类型
    label: "字段循环", // 标题文字
    icon: "icon-xiala",
	img:require('@/img/fieldloop.png'),//在画布显示图片
    options: {
      width: "100%", // 宽度
      defaultValue: undefined, // 下拉选框请使用undefined为默认值
      multiple: false, // 是否允许多选
      disabled: false, // 是否禁用
      clearable: false, // 是否显示清除按钮
      hidden: false, // 是否隐藏，false显示，true隐藏
      placeholder: "请选择", // 默认提示文字
      dynamicKey: "",
      dynamic: false,
      options: [
        // 下拉选择项配置
        {
          value: "1",
          label: "下拉框1"
        },
        {
          value: "2",
          label: "下拉框2"
        }
      ],
      showSearch: false // 是否显示搜索框，搜索选择的项的值，而不是文字
    },
    model: "",
    key: "",
    help: "",
    prefix: "",
    suffix: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "decide",
    label: "判断",
    icon: "icon-duoxuan1",
	img:require('@/img/decide.png'),//在画布显示图片
    options: {
      disabled: false, //是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      defaultValue: [],
      dynamicKey: "",
      dynamic: false,
      options: [
        {
          value: "1",
          label: "选项1"
        },
        {
          value: "2",
          label: "选项2"
        },
        {
          value: "3",
          label: "选项3"
        }
      ]
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "varinit", // 表单类型
    label: "变量初始化", // 标题文字
    icon: "icon-danxuan-cuxiantiao",
	img:require('@/img/var.png'),//在画布显示图片
    options: {
      disabled: false, //是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      defaultValue: "", // 默认值
      dynamicKey: "",
      dynamic: false,
      options: [
        {
          value: "1",
          label: "选项1"
        },
        {
          value: "2",
          label: "选项2"
        },
        {
          value: "3",
          label: "选项3"
        }
      ]
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "tablecn", // 表单类型
    label: "表中文名", // 标题文字
    icon: "icon-calendar",
	img:require('@/img/tabcn.png'),//在画布显示图片
    options: {
      width: "100%", // 宽度
      defaultValue: "", // 默认值，字符串 12:00:00
      rangeDefaultValue: [], // 默认值，字符串 12:00:00
      range: false, // 范围日期选择，为true则会显示两个时间选择框（同时defaultValue和placeholder要改成数组），
      showTime: false, // 是否显示时间选择器
      disabled: false, // 是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      clearable: false, // 是否显示清除按钮
      placeholder: "请选择",
      rangePlaceholder: ["开始时间", "结束时间"],
      format: "YYYY-MM-DD" // 展示格式  （请按照这个规则写 YYYY-MM-DD HH:mm:ss，区分大小写）
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "tableen", // 表单类型
    label: "表英文名", // 标题文字
    icon: "icon-time",
	img:require('@/img/taben.png'),//在画布显示图片
    options: {
      width: "100%", // 宽度
      defaultValue: "", // 默认值，字符串 12:00:00
      disabled: false, // 是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      clearable: false, // 是否显示清除按钮
      placeholder: "请选择",
      format: "HH:mm:ss" // 展示格式
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "fieldcn", // 表单类型
    label: "字段中文", // 标题文字
    icon: "icon-pingfen_moren",
	img:require('@/img/fieldcn.png'),//在画布显示图片
    options: {
      defaultValue: 0,
      max: 5, // 最大值
      disabled: false, // 是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      allowHalf: false // 是否允许半选
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "fielden", // 表单类型
    label: "字段英文名", // 标题文字
    icon: "icon-menu",
	img:require('@/img/fielden.png'),//在画布显示图片
    options: {
      width: "100%", // 宽度
      defaultValue: 0, // 默认值， 如果range为true的时候，则需要改成数组,如：[12,15]
      disabled: false, // 是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      min: 0, // 最小值
      max: 100, // 最大值
      step: 1, // 步长，取值必须大于 0，并且可被 (max - min) 整除
      showInput: false // 是否显示输入框，range为true时，请勿开启
      // range: false // 双滑块模式
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  }
];

// 高级控件
// export const highList = [];

// import { Alert } from "ant-design-vue";

// 宏
export const customComponents = [
   {
    type: "table2class", // 表单类型
    label: "表名转类名", // 标题文字
    icon: "icon-upload",
	img:require('@/img/T2C.png'),//在画布显示图片
    options: {
      defaultValue: "[]",
      multiple: false,
      disabled: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      drag: false,
      downloadWay: "a",
      dynamicFun: "",
      width: "100%",
      limit: 3,
      data: "{}",
      fileName: "file",
      headers: {},
      action: "http://cdn.kcz66.com/uploadFile.txt",
      placeholder: "上传"
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "table2file",
    label: "表转文件名",
    icon: "icon-image",
	img:require('@/img/T2F.png'),//在画布显示图片
    options: {
      defaultValue: "[]",
      multiple: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      disabled: false,
      width: "100%",
      data: "{}",
      limit: 3,
      placeholder: "上传",
      fileName: "image",
      headers: {},
      action: "http://cdn.kcz66.com/upload-img.txt",
      listType: "picture-card"
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "table2method", // 表单类型
    label: "表转方法名", // 标题文字
    icon: "icon-tree",
	img:require('@/img/T2M.png'),//在画布显示图片
    options: {
      disabled: false, //是否禁用
      defaultValue: undefined, // 默认值
      multiple: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      clearable: false, // 是否显示清除按钮
      showSearch: false, // 是否显示搜索框，搜索选择的项的值，而不是文字
      treeCheckable: false,
      placeholder: "请选择",
      dynamicKey: "",
      dynamic: true,
      options: [
        {
          value: "1",
          label: "选项1",
          children: [
            {
              value: "11",
              label: "选项11"
            }
          ]
        },
        {
          value: "2",
          label: "选项2",
          children: [
            {
              value: "22",
              label: "选项22"
            }
          ]
        }
      ]
    },
    model: "",
    key: "",
    help: "",
    prefix: "",
    suffix: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "field2prop", // 表单类型
    label: "字段转属性", // 标题文字
    icon: "icon-guanlian",
	img:require('@/img/F2P.png'),//在画布显示图片
    options: {
      disabled: false, //是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      defaultValue: undefined, // 默认值
      showSearch: false, // 是否显示搜索框，搜索选择的项的值，而不是文字
      placeholder: "请选择",
      clearable: false, // 是否显示清除按钮
      dynamicKey: "",
      dynamic: true,
      options: [
        {
          value: "1",
          label: "选项1",
          children: [
            {
              value: "11",
              label: "选项11"
            }
          ]
        },
        {
          value: "2",
          label: "选项2",
          children: [
            {
              value: "22",
              label: "选项22"
            }
          ]
        }
      ]
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "field2method",
    label: "字段转方法",
    icon: "icon-biaoge",
	img:require('@/img/F2M.png'),//在画布显示图片
    list: [],
    options: {
      scrollY: 0,
      disabled: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      showLabel: false,
      hideSequence: false,
      width: "100%"
    },
    model: "",
    key: "",
    help: ""
  }
];
// window.$customComponentList = customComponents.list;

// 函数控件
export const layoutList = [
{
    type: "writefile",
    label: "写文件函数",
    icon: "icon-LC_icon_edit_line_1",
	img:require('@/img/writefile.png'),//在画布显示图片
    list: [],
    options: {
      height: 300,
      placeholder: "请输入",
      defaultValue: "",
      chinesization: true,
      hidden: false, // 是否隐藏，false显示，true隐藏
      disabled: false,
      showLabel: false,
      width: "100%"
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "date", // 表单类型
    label: "日期函数", // 标题文字
    icon: "icon-kaiguan3",
	img:require('@/img/datefn.png'),//在画布显示图片
    options: {
      defaultValue: false, // 默认值 Boolean 类型
      hidden: false, // 是否隐藏，false显示，true隐藏
      disabled: false // 是否禁用
    },
    model: "",
    key: "",
    help: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  }
];
