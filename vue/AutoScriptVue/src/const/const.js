export const projectId = "project";
export const templateId = "template";
export const lineId = "line";
//属性类型
export const propertyType = [
  {
    value: "input",
    label: "单行输入框"
  },
  {
    value: "textarea",
    label: "多行输入框"
  },
  {
    value: "date",
    label: "日期"
  },
  {
    value: "time",
    label: "时间"
  },
  {
    value: "number",
    label: "数字输入框"
  },
  {
    value: "radio",
    label: "单选框"
  },
  {
    value: "checkbox",
    label: "多选框"
  },
  {
    value: "select",
    label: "选择器"
  },
  {
    value: "switch",
    label: "开关"
  },
  {
    value: "treeSelect",
    label: "树选择器"
  },
];
