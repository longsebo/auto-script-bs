import Vue from "vue";
import Router from "vue-router";
import Login from "@/components/Login";
import Main from "@/components/Main";
import UserRegister from "../components/UserRegister";
import ForgetPassword from "../components/ForgetPassword";
Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: "/",
      name: "index",
      redirect: "/login"
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/forgetPassword",
      name: "ForgetPassword",
      component: ForgetPassword,
      meta: {
        requiredLogin: false
      }
    },
    {
      path: "/userRegister",
      name: "UserRegister",
      component: UserRegister,
      meta: {
        requiredLogin: false
      }
    },
    {
      path: "/main",
      name: "main",
      component: Main,
      show: false,
      meta: {
        title: "Main",
        icon: "el-icon-eleme",
        requireAuth: true
      },
      children: [
        {
          path: "/templatefunction/edit/:id",
          component: () => import("@/templatefunction/edit.vue"),
          show: true,
          meta: {
            title: "TemplateFunction",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/templatefunction/add",
          component: () => import("@/templatefunction/add.vue"),
          show: true,
          meta: {
            title: "TemplateFunction",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/templatefunction/list",
          component: () => import("@/templatefunction/list.vue"),
          show: true,
          meta: {
            title: "TemplateFunction",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/knowledgebase/edit/:id",
          component: () => import("@/knowledgebase/edit.vue"),
          show: true,
          meta: {
            title: "Knowledgebase",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/knowledgebase/add",
          component: () => import("@/knowledgebase/add.vue"),
          show: true,
          meta: {
            title: "Knowledgebase",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/knowledgebase/list",
          component: () => import("@/knowledgebase/list.vue"),
          show: true,
          meta: {
            title: "Knowledgebase",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/project/edit/:id",
          component: () => import("@/project/edit.vue"),
          show: true,
          meta: {
            title: "Project",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/project/run/:id/:isDataSource",
          // component: () => import("@/project/run.vue"),
          component: () => import("@/project/newrun.vue"),
          show: true,
          meta: {
            title: "Project",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/project/add",
          component: () => import("@/project/add.vue"),
          show: true,
          meta: {
            title: "Project",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/project/list",
          component: () => import("@/project/list.vue"),
          show: true,
          meta: {
            title: "Project",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/runlog/list",
          component: () => import("@/runlog/list.vue"),
          show: true,
          meta: {
            title: "Runlog",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/template/edit/:id",
          component: () => import("@/template/edit.vue"),
          show: true,
          meta: {
            title: "Template",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/template/add/:projectId/:projectName",
          component: () => import("@/template/add.vue"),
          show: true,
          meta: {
            title: "Template",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/template/list/:projectId/:projectName",
          component: () => import("@/template/list.vue"),
          show: true,
          meta: {
            title: "Template",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/control/edit/:id",
          component: () => import("@/control/edit.vue"),
          show: true,
          meta: {
            title: "Control",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/control/add",
          component: () => import("@/control/add.vue"),
          show: true,
          meta: {
            title: "Control",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/control/list",
          component: () => import("@/control/list.vue"),
          show: true,
          meta: {
            title: "Control",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/controlproperties/edit/:id",
          component: () => import("@/controlproperties/edit.vue"),
          show: true,
          meta: {
            title: "Controlproperties",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/controlproperties/add/:controlId",
          component: () => import("@/controlproperties/add.vue"),
          show: true,
          meta: {
            title: "Controlproperties",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/controlproperties/list/:controlId",
          component: () => import("@/controlproperties/list.vue"),
          show: true,
          meta: {
            title: "Controlproperties",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/kformdesign",
          name: "kformdesign",
          component: () => import("@/visualization/KFormDesign/index.vue"),
          show: true,
          meta: {
            title: "Controlproperties",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/projectparameter/edit/:id",
          component: () => import("@/projectparameter/edit.vue"),
          show: true,
          meta: {
            title: "Projectparameter",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/projectparameter/add/:projectId",
          component: () => import("@/projectparameter/add.vue"),
          show: true,
          meta: {
            title: "Projectparameter",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/projectparameter/list/:projectId",
          component: () => import("@/projectparameter/list.vue"),
          show: true,
          meta: {
            title: "Projectparameter",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/shareproject/add/:projectId",
          component: () => import("@/shareproject/add.vue"),
          show: true,
          meta: {
            title: "Shareproject",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/shareproject/list/:projectId",
          component: () => import("@/shareproject/list.vue"),
          show: true,
          meta: {
            title: "Shareproject",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/testant",
          component: () => import("@/project/testant.vue"),
          show: true,
          meta: {
            title: "Shareproject",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
        {
          path: "/project/lamprun/:id/:isDataSource",
          // component: () => import("@/project/run.vue"),
          component: () => import("@/custompage/lamprun.vue"),
          show: true,
          meta: {
            title: "Project",
            icon: "el-icon-s-marketing",
            requireAuth: true
          }
        },
      ]
    }
  ]
});

export default router;
router.beforeEach((to, from, next) => {
  let islogin = localStorage.getItem("islogin");
  console.log(islogin);
  console.log(to.path);
  islogin = Boolean(Number(islogin));

  if (to.path == "/login") {
    if (islogin) {
      next("/project/list");
    } else {
      next();
    }
  } else if (to.path == "/userRegister") {
    next();
  } else if (to.path == "/forgetPassword") {
    next();
  } else {
    // requireAuth:可以在路由元信息指定哪些页面需要登录权限
    if (to.meta.requireAuth && islogin) {
      next();
    } else {
      next("/login");
    }
  }
});
