/*
 * 配置编译环境和线上环境之间的切换
 * baseUrl: 域名地址
 * routerMode: 路由模式
 * DEBUG: debug状态
 * cancleHTTP: 取消请求头设置
 */
var baseUrl = "";
var wsBaseUrl = "";
const routerMode = "history";
var DEBUG = false;
const cancleHTTP = [];
if (process.env.NODE_ENV == "dev") {
  baseUrl = "http://127.0.0.1:3338";
  wsBaseUrl ="ws://127.0.0.1:3338/websocket/";
  DEBUG = true;
} else if (process.env.NODE_ENV == "production") {
  baseUrl = "http://192.168.43.19:3334";
  wsBaseUrl ="ws://192.168.43.19:3334/websocket/";
  DEBUG = false;
}
export { baseUrl, routerMode, DEBUG, cancleHTTP,wsBaseUrl };
