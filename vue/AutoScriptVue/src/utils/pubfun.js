//判断手机号是否正确
export function  validatePhone(value){
  //判断输入的手机号是否合法
  if(!value) {
    return "手机号码不能为空"

    // return false
  } else if(!/^1[345678]\d{9}$/.test(value)) {
     return "请输入正确是手机号"
    // return false
  } else {
    return ""
  }
}
//判断邮箱是否正确
export function validateEmail(value){
  //不能为空
  let verify = /^\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/;

  if(!value){
    return "邮箱不能空"
  }else if (!verify.test(value)) {
    return "邮箱格式错误"
  }else {
    return ""
  }
}
//

