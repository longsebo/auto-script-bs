// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "xe-utils";
import VXETable from "vxe-table";
import "vxe-table/lib/style.css";
import KFormDesign from "k-form-design";
import "k-form-design/lib/k-form-design.css";
import {TreeSelect} from "ant-design-vue";
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(VXETable);
Vue.use(KFormDesign);
Vue.use(TreeSelect);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  components: { App },
  template: "<App/>"
});
